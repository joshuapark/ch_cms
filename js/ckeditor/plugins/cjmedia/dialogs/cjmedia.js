﻿CKEDITOR.dialog.add('cjmediaDialog', function (editor) {
        return {
            title: 'Media Upload',
            minWidth: 400,
            minHeight: 200,
            contents: [
                {
                        id: 'Upload',
                        hidden: true,
                        filebrowser: 'uploadButton',
                        label: editor.lang.image.upload,
                        elements:
                        [
                            {
                                type: 'file',
                                id: 'upload',
                                label: editor.lang.image.btnUpload,
                                style: 'height:40px',
                                size: 38
                            },
                            {
                                type: 'fileButton',
                                id: 'uploadButton',
                                filebrowser: 'Upload:txtUrl',
                                label: editor.lang.image.btnUpload,
    
                                'for': ['Upload', 'upload']
                            },
                            {
                                type: 'text',
                                id: 'txtUrl',
                                label: '미디어 링크'
                            }
                        ]
                    }
                ],
                onOk: function () {
                    var dialog = this;
                                
                    var fileUrl = dialog.getValueOf('Upload', 'txtUrl');
                    var splitedFilUrl = fileUrl.split('/');
                    var fileName = splitedFilUrl[splitedFilUrl.length - 1];

                    editor.insertText( fileName );
                    
              }
    };
});