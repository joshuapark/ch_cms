﻿﻿CKEDITOR.plugins.add('cjmedia', {

    icons: 'cjmedia',
    init: function (editor) {
            editor.addCommand('cjmedia', new CKEDITOR.dialogCommand('cjmediaDialog'));
            editor.ui.addButton('CJMedia', {
                    label: 'Insert Media',
                    command: 'cjmedia',
                    toolbar: 'insert'
                });

        CKEDITOR.dialog.add('cjmediaDialog', this.path + 'dialogs/cjmedia.js');
    }
});