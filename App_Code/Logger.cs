﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

/// <summary>
/// Logger의 요약 설명입니다.
/// </summary>
public class Logger
{
    public static void Log(string logMessage)
    {
        
        using (StreamWriter w = File.AppendText("D:\\Logs\\log.txt"))
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("  :");
            w.WriteLine("  :{0}", logMessage);
            w.WriteLine("-------------------------------");
            // Update the underlying file.
            w.Flush();

            // Close the writer and underlying file.
            w.Close();
        }
    }
}