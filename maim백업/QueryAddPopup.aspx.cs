﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class main_QueryAddPopup : System.Web.UI.Page
{

    private string connectionString = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

        string query = Request["querytext"];

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(query, Con);
        Con.Open();

        SqlDataReader dataReader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        DataTable dataTable = new DataTable();
        dataTable.Load(dataReader);

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        lblResultCount.Text = dataTable.Rows.Count.ToString();
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        
    }

}