﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="BinderTree.aspx.cs" Inherits="notice_List" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<link href="../css/editor.css" rel="stylesheet">
	<link href="../css/jquery-ui.css" rel="stylesheet">
	<link href="../css/jquery.simplecolorpicker.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />

    <style type="text/css">
		ol {
			max-width: 600px;
			padding-left: 25px;
		}
		
		ol.sortable,ol.sortable ol {
			list-style-type: none;
		}
		
		.sortable li div {
			border: 1px solid #d4d4d4;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
			cursor: move;
			border-color: #D4D4D4 #D4D4D4 #BCBCBC;
			margin: 0;
			padding: 3px;
		}
		
		li.mjs-nestedSortable-collapsed.mjs-nestedSortable-hovering div {
			border-color: #999;
		}
		
		.disclose, .expandEditor {
			cursor: pointer;
			width: 20px;
			display: none;
		}
		
		.sortable li.mjs-nestedSortable-collapsed > ol {
			display: none;
		}
		
		.sortable li.mjs-nestedSortable-branch > div > .disclose {
			display: inline-block;
		}
		
		.sortable span.ui-icon {
			display: inline-block;
			margin: 0;
			padding: 0;
		}
    </style>


	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/ckeditor/ckeditor.js"></script>
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/jquery.ui/jquery.ui.js"></script>
<script src="../js/jquery.nestedSortable.js"></script>
<script src="../js/jquery.simplecolorpicker.js"></script>
<script>
    $(document).ready(function () {
       /*
        var ns = $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div',
            helper: 'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 4,
            isTree: true,
            expandOnHover: 700,
            startCollapsed: false
        });
        */

        $('.disclose').on('click', function () {
            $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            $(this).toggleClass('ui-icon-plusthick').toggleClass('ui-icon-minusthick');
        });
        
    });

    function showDirectoryAdd( parentIdx )
    {

    }

    function showDirectoryEdit(binderIdx) {

    }

    function doTheSubmit() {
        $("#searchForm").submit();
    }
</script>
<body>

<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">

	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2 active"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li class="active"><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->
    <form id="searchForm" runat="server">


	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">바인더관리</h2>
			<div class="action">
				<div class="tab-b"><!-- tab -->
					<ul>
						<li><a href="BinderList.aspx"><span>목록보기</span></a></li>
						<li class="active"><a href="BinderTree.aspx"><span>트리보기</span></a></li>
					</ul>
				</div><!-- // tab -->
			</div>
		</div><!-- // title -->	

		<div><!-- binder menu -->
		   <ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">

               <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
		           <div>
			           <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			           <span></span>
			           </span>
                       <strong>Binder-Root</strong>
                       <button type="button" class="btn-setup" onclick="javascript:window.open('BinderDirectoryAdd.aspx?ParentIdx=0&ParentLevel=0', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
		           </div>
                   <ol>
                   <% 
                       if (binderTable.Rows.Count > 0) { 
                       DataRow[] firstLevelRowList = binderTable.Select("ParentIdx=0");
                       foreach (DataRow firstRow in firstLevelRowList) { 
                       %>
               
                   <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
		           <div>
			           <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			           <span></span>
			           </span>
                       <strong><a href='BinderEdit.aspx?idx=<%=firstRow["BinderIdx"] %>'><%=firstRow["BinderTitle"] %>(1,100)</a></strong>
                       <button type="button" class="btn-setup" onclick="javascript:window.open('BinderDirectoryAdd.aspx?ParentIdx=<%=firstRow["BinderIdx"] %>&ParentLevel=<%=firstRow["BinderLevel"]%>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                       <button type="button" class="btn-modify" onclick="javascript:window.open('BinderDirectoryEdit.aspx?BinderIdx=<%=firstRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                       <button type="button" class="btn-delete" onclick="javascript:window.open('BinderDirectoryDelete.aspx?BinderIdx=<%=firstRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
		           </div>
                       <% 
                           DataRow[] secondRowList = binderTable.Select("ParentIdx=" + firstRow["BinderIdx"]);
                           if( secondRowList.Length > 0 ){
                           %>
                       <ol>
                           <%
                               foreach (DataRow secondRow in secondRowList) { 
                                %>
                           <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
		                       <div>
			                       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			                       <span></span>
			                       </span>
                                   <strong><%=secondRow["BinderTitle"] %>(1,100)</strong>
                                    <button type="button" class="btn-setup" onclick="javascript:window.open('BinderDirectoryAdd.aspx?ParentIdx=<%=secondRow["BinderIdx"] %>&ParentLevel=<%=secondRow["BinderLevel"]%>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                    <button type="button" class="btn-modify" onclick="javascript:window.open('BinderDirectoryEdit.aspx?BinderIdx=<%=secondRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                    <button type="button" class="btn-delete" onclick="javascript:window.open('BinderDirectoryDelete.aspx?BinderIdx=<%=secondRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                
		                       </div>
                               <%
                                   DataRow[] thirdRowList = binderTable.Select("ParentIdx=" + secondRow["BinderIdx"]);
                                   if (thirdRowList.Length > 0) { 
                                    %>
                               <ol>
                                   <%
                                       foreach (DataRow thirdRow in thirdRowList) { 
                                        %>
                                   <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
		                               <div>
			                               <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			                               <span></span>
			                               </span>
                                           <strong><%=thirdRow["BinderTitle"] %>(1,100)</strong>
                                            <button type="button" class="btn-setup" onclick="javascript:window.open('BinderDirectoryAdd.aspx?ParentIdx=<%=thirdRow["BinderIdx"] %>&ParentLevel=<%=thirdRow["BinderLevel"]%>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                            <button type="button" class="btn-modify" onclick="javascript:window.open('BinderDirectoryEdit.aspx?BinderIdx=<%=thirdRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                            <button type="button" class="btn-delete" onclick="javascript:window.open('BinderDirectoryDelete.aspx?BinderIdx=<%=thirdRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
		                               </div>
                                       <%
                                           DataRow[] fourthRowList = binderTable.Select("ParentIdx=" + thirdRow["BinderIdx"]);
                                           if (fourthRowList.Length > 0) {
                                            %>
                                       <ol>
                                           <% 
                                               foreach(DataRow fourthRow in fourthRowList){
                                                %>
                                       
                                           <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
		                                       <div>
			                                       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			                                       <span></span>
			                                       </span>
                                                   <strong><%=fourthRow["BinderTitle"] %>(1,100)</strong>
                                                    <button type="button" class="btn-setup" onclick="javascript:window.open('BinderDirectoryAdd.aspx?ParentIdx=<%=fourthRow["BinderIdx"] %>&ParentLevel=<%=fourthRow["BinderLevel"]%>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                                    <button type="button" class="btn-modify" onclick="javascript:window.open('BinderDirectoryEdit.aspx?BinderIdx=<%=fourthRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                                    <button type="button" class="btn-delete" onclick="javascript:window.open('BinderDirectoryDelete.aspx?BinderIdx=<%=fourthRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
		                                       </div>     
                                               <%
                                                   DataRow[] fifthRowList = binderTable.Select("ParentIdx=" + fourthRow["BinderIdx"]);
                                                   if(fifthRowList.Length > 0){
                                                    %>
                                               <ol>
                                                   <%
                                                       foreach(DataRow fifthRow in fifthRowList){
                                                        %>
                                                       <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
		                                                   <div>
			                                                   <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			                                                   <span></span>
			                                                   </span>
                                                               <strong><%=fifthRow["BinderTitle"] %>(1,100)</strong>
                                                                <button type="button" class="btn-setup" onclick="javascript:window.open('BinderDirectoryAdd.aspx?ParentIdx=<%=fifthRow["BinderIdx"] %>&ParentLevel=<%=fifthRow["BinderLevel"]%>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                                                <button type="button" class="btn-modify" onclick="javascript:window.open('BinderDirectoryEdit.aspx?BinderIdx=<%=fifthRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                                                <button type="button" class="btn-delete" onclick="javascript:window.open('BinderDirectoryDelete.aspx?BinderIdx=<%=fifthRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
		                                                   </div>
                                                           <% 
                                                               DataRow[] sixthRowList = binderTable.Select("ParentIdx=" + fifthRow["BinderIdx"]);
                                                               if(sixthRowList.Length > 0){
                                                                %>
                                                           <ol>
                                                               <% 
                                                                   foreach (DataRow sixthRow in sixthRowList) { 
                                                                   %>
                                                               <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
		                                                   <div>
			                                                   <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			                                                   <span></span>
			                                                   </span>
                                                               <strong><%=sixthRow["BinderTitle"] %>(1,100)</strong>
                                                                <button type="button" class="btn-setup" onclick="javascript:window.open('BinderDirectoryAdd.aspx?ParentIdx=<%=sixthRow["BinderIdx"] %>&ParentLevel=<%=sixthRow["BinderLevel"]%>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                                                <button type="button" class="btn-modify" onclick="javascript:window.open('BinderDirectoryEdit.aspx?BinderIdx=<%=sixthRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
                                                                <button type="button" class="btn-delete" onclick="javascript:window.open('BinderDirectoryDelete.aspx?BinderIdx=<%=sixthRow["BinderIdx"] %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=300,left=0, top=0, scrollbars=yes');return false" />
		                                                   </div>



                                                               <% }//sixth for %>
                                                           </ol>
                                                           <%}//sixth if 
                                                             %>
                                                   <% }//fifth for %>
                                               </ol>

                                               <%} //fifth if %>

                                           <% } //fourth for %>
                                       </ol>
                                       <%
                                       }//fourth if
                                            %>
                                   <%
                                        }//third for
                                        %>
                               </ol>
                               <% } //third if
                                   %>
                           <% } //second for %>
                       </ol> 
                       <% } //second  if%>
                   </li>    
               
                   <%
                       } //firstLevel
                       } //if
                       %>
                    </ol>
               </ol>
		</div><!-- // binder menu -->

	</div><!-- // contents -->
    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->

</body>
</html>


<%--<ul>
				<li class="first open last">
					<strong>국어(1,100)</strong>
					<a href="#!" class="btn-modify"><!-- modify --></a>
					<a href="#!" class="btn-setup"><!-- setup --></a>
					<a href="#!" class="btn-delete"><!-- delete --></a>
					<ul>
						<li class="open last">
							<strong>시(1,100)</strong>
							<a href="#!" class="btn-modify"><!-- modify --></a>
							<a href="#!" class="btn-setup"><!-- setup --></a>
							<a href="#!" class="btn-delete"><!-- delete --></a>
							<ul>
								<li class="open"><strong>시즌별 (8)</strong>
									<div class="action">
										<button type="button" class="btn btn-sm btn-default">바인더 등록</button>
									</div>

									<table border="0" cellpadding="0" cellspacing="0" class="table table-list"><!-- table-a -->
									<thead>
										<tr>
											<th>No</th>
											<th>바인더명</th>
											<th>엔트리수</th>
											<th>바인더명</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>설</td>
											<td>300</td>
											<td>2015-03-01</td>
										</tr>
										<tr>
											<td>1</td>
											<td>설</td>
											<td>300</td>
											<td>2015-03-01</td>
										</tr>
										<tr>
											<td>1</td>
											<td>설</td>
											<td>300</td>
											<td>2015-03-01</td>
										</tr>
										<tr>
											<td>1</td>
											<td>설</td>
											<td>300</td>
											<td>2015-03-01</td>
										</tr>
										<tr>
											<td>1</td>
											<td>설</td>
											<td>300</td>
											<td>2015-03-01</td>
										</tr>
									</tbody>
									</table><!-- // table-a -->
								</li>
								<li><strong>작가별 (100)</strong>
									<button type="button" class="btn btn-sm btn-primary">디렉토리 등록</button>
									<button type="button" class="btn btn-sm btn-primary">디렉토리 수정</button>
									<button type="button" class="btn btn-sm btn-primary">디렉토리 삭제</button>
								</li>
								<li class="last"><strong>학교별 (100)</strong>
									<button type="button" class="btn btn-sm btn-primary">디렉토리 등록</button>
									<button type="button" class="btn btn-sm btn-primary">디렉토리 수정</button>
									<button type="button" class="btn btn-sm btn-primary">디렉토리 삭제</button>
								</li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>--%>






<%--<ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">
		       <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_2">
		       <div class="menuDiv">
			       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			       <span></span>
			       </span>
			       <span>
			       <span data-id="2" class="itemTitle">a</span>
			       </span>
			       <div id="menuEdit2" class="menuEdit hidden">
				       <p>
					       Content or form, or nothing here. Whatever you want.
				       </p>
			       </div>
		       </div>

		       <ol>
			       <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_4">
			       <div class="menuDiv">
				       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
				       <span></span>
				       </span>
				       <span>
				       <span data-id="4" class="itemTitle">c</span>
				       </span>
				       <div id="menuEdit4" class="menuEdit hidden">
					       <p>
						       Content or form, or nothing here. Whatever you want.
					       </p>
				       </div>
			       </div>
			       <ol>
				       <li class="mjs-nestedSortable-leaf" id="menuItem_6">
				       <div class="menuDiv">
					       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
					       <span></span>
					       </span>
					       <span>
					       <span data-id="6" class="itemTitle">e</span>
					       </span>
					       <div id="menuEdit6" class="menuEdit hidden">
						       <p>
							       Content or form, or nothing here. Whatever you want.
						       </p>
					       </div>
				       </div>
				       </li>
			       </ol>
			       </li>
			       <li style="display: list-item;" class="mjs-nestedSortable-leaf" id="menuItem_5">
			       <div class="menuDiv">
				       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
				       <span></span>
				       </span>
				       <span>
				       <span data-id="5" class="itemTitle">d</span>
				       </span>
				       <div id="menuEdit5" class="menuEdit hidden">
					       <p>
						       Content or form, or nothing here. Whatever you want.
					       </p>
				       </div>
			       </div>
			       </li>
		       </ol>
		       </li>
		       <li style="display: list-item;" class="mjs-nestedSortable-leaf" id="menuItem_7">
		       <div class="menuDiv">
			       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			       <span></span>
			       </span>
			       <span>
			       <span data-id="7" class="itemTitle">f</span>
			       </span>
			       <div id="menuEdit7" class="menuEdit hidden">
				       <p>
					      Content or form, or nothing here. Whatever you want.
				       </p>
			       </div>
		       </div>
		       </li>
		       <li class="mjs-nestedSortable-leaf" id="menuItem_3">
		       <div class="menuDiv">
			       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			       <span></span>
			       </span>
			       <span title="Click to show/hide item editor" data-id="3" class="expandEditor ui-icon ui-icon-triangle-1-n">
			       <span></span>
			       </span>
			       <span>
			       <span data-id="3" class="itemTitle">b</span>
			       </span>
			       <div id="menuEdit3" class="menuEdit hidden">
				       <p>
					       Content or form, or nothing here. Whatever you want.
				       </p>
			       </div>
		       </div>
		       </li>
	       </ol>--%>



<%--<li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
		       <div>
			       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			       <span></span>
			       </span>
                   <strong>국어(1,100)</strong>
                    <button type="button" class="btn btn-sm btn-primary">디렉토리 등록</button>
					<button type="button" class="btn btn-sm btn-primary">디렉토리 수정</button>
					<button type="button" class="btn btn-sm btn-primary">디렉토리 삭제</button>
		       </div>

		       <ol>
			       <li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
			       <div>
				       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
				       <span></span>
				       </span>
                       <strong>시(1,100)</strong>
						<button type="button" class="btn btn-sm btn-primary">디렉토리 등록</button>
						<button type="button" class="btn btn-sm btn-primary">디렉토리 수정</button>
						<button type="button" class="btn btn-sm btn-primary">디렉토리 삭제</button>
			       </div>
			       <ol>
				       <li class="mjs-nestedSortable-leaf">
				       <div>
					       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
					       <span></span>
					       </span>
                           <strong>시즌별 (8)</strong>
								<div class="action">
									<button type="button" class="btn btn-sm btn-default">바인더 등록</button>
								</div>

								<table border="0" cellpadding="0" cellspacing="0" class="table table-list"><!-- table-a -->
								<thead>
									<tr>
										<th>No</th>
										<th>바인더명</th>
										<th>엔트리수</th>
										<th>바인더명</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>설</td>
										<td>300</td>
										<td>2015-03-01</td>
									</tr>
									<tr>
										<td>1</td>
										<td>설</td>
										<td>300</td>
										<td>2015-03-01</td>
									</tr>
									<tr>
										<td>1</td>
										<td>설</td>
										<td>300</td>
										<td>2015-03-01</td>
									</tr>
									<tr>
										<td>1</td>
										<td>설</td>
										<td>300</td>
										<td>2015-03-01</td>
									</tr>
									<tr>
										<td>1</td>
										<td>설</td>
										<td>300</td>
										<td>2015-03-01</td>
									</tr>
								</tbody>
								</table><!-- // table-a -->
				       </div>
				       </li>
			       </ol>
			       </li>
			       <li style="display: list-item;" class="mjs-nestedSortable-leaf">
			       <div>
				       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
				       <span></span>
				       </span>
                       <strong>학교별 (100)</strong>
						<button type="button" class="btn btn-sm btn-primary">디렉토리 등록</button>
						<button type="button" class="btn btn-sm btn-primary">디렉토리 수정</button>
						<button type="button" class="btn btn-sm btn-primary">디렉토리 삭제</button>
			       </div>
			       </li>
		       </ol>
		       </li>
		       <li style="display: list-item;" class="mjs-nestedSortable-leaf">
		       <div>
			       <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick">
			       <span></span>
			       </span>
                   <strong>작가별 (100)</strong>
					<button type="button" class="btn btn-sm btn-primary">디렉토리 등록</button>
					<button type="button" class="btn btn-sm btn-primary">디렉토리 수정</button>
					<button type="button" class="btn btn-sm btn-primary">디렉토리 삭제</button>
		       </div>
		       </li>
	       </ol>--%>
