﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditorTest.aspx.cs" Inherits="main_EditorTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script src="../js/ckeditor/ckeditor.js"></script>
<script src="../js/jquery-2.1.3.min.js"></script>
<link rel="stylesheet" href="../js/ckeditor/samples/sample.css"/>
<script>
    $(document).ready(function () {



        $("#btnAdd").click(function () {
            var appendHtml = '<textarea cols="80" id="editor3" name="editor3" rows="10" ></textarea>';
            $("#EntryEditor").append(appendHtml);

            CKEDITOR.replace('editor3', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] },
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],
                    { name: 'basicstyles', items: ['Bold', 'Italic'] }
                ]
            });
        });
    });

    var LTitleNumber = 1;
    var MTitleNumber = 1;
    var Basic1Number = 1;
    var Basic2Number = 1;
    var BlockNumber = 1;
    var TableNumber = 1;
    var ImageNumber = 1;
    var CommentNumber = 1;

    function addLTitleRow() {
        addRow("ltitle" + ++LTitleNumber, 'DivLTitle');
    }

    function addMTitleRow() {
        addRow("mtitle" + ++MTitleNumber, 'DivMTitle');
    }

    function addBasic1Row() {
        addRow("basic1" + ++Basic1Number, 'DivBasic1');
    }

    function addBasic1Row2() {
        addRow("basic2" + ++Basic2Number, 'DivBasic2');
    }

    function addBlockRow() {
        addRow("block" + ++BlockNumber, 'DivBlock');
    }

    function addTableRow() {
        addRow("table" + ++TableNumber, 'DivTable');
    }

    function addImageRow() {
        addRow("image" + ++ImageNumber, 'DivImage');
    }

    function addCommentRow() {
        addRow("comment" + ++CommentNumber, 'DivComment');
    }

    function addRow(editorName , containerName ) {
        var html = "<div id='" + editorName + "'><textarea cols='80' id='" + editorName + "' name='" + editorName + "' rows='5' ></textarea></div>";
        $("#" + containerName).append(html);

        CKEDITOR.replace( editorName , {
            title:['대제목'],
            toolbar: [
                { name: 'document', items: ['Source', '-', 'Preview','Table', 'Image'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],			// Defines toolbar group without name.
                { name: 'basicstyles', items: ['BulletedList','Bold','Italic','Subscript','Superscript','Underline','Link',] }
            ]
        });
    };

    function getText() {
        var editor = CKEDITOR.instances["ltitle2"];

        alert("text = " + editor.getData());
    }

</script>
<title>에디터 테스트</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <input type="button" value="대제목" onclick="addLTitleRow();"/>
        <input type="button" value="중제목" />
        <input type="button" value="기본 1" />
        <input type="button" value="기본 2" />
        <input type="button" value="블럭" />
        <input type="button" value="표" />
        <input type="button" value="이미지" />
        <input type="button" value="주석" />
    </div>
<div id="container">
    <div id="DivLTitle">
        
        <textarea cols="80" id="ltitle1" name="ltitle1" rows="5" ></textarea>
        <script>
            CKEDITOR.replace('ltitle1', {
                title: ['대제목'],
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'Preview','Table', 'Image'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],			// Defines toolbar group without name.
                    { name: 'basicstyles', items: ['BulletedList','Bold','Italic','Subscript','Superscript','Underline','Link',] }
                ]
            });
	    </script>
    </div>
    <br />

    <div id="DivMTitle">

        <textarea cols="80" id="mtitle1" name="mtitle1" rows="5" ></textarea>
        <script>
            CKEDITOR.replace('mtitle1', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],			// Defines toolbar group without name.
                    { name: 'basicstyles', items: ['Bold', 'Italic'] }
                ]
            });
	    </script>
    </div>
    
    <div id="DivBasic1">

        <textarea cols="80" id="basic11" name="basic11" rows="5" ></textarea>
        <script>
            CKEDITOR.replace('basic11', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],			// Defines toolbar group without name.
                    { name: 'basicstyles', items: ['Bold', 'Italic'] }
                ]
            });
	    </script>
    </div>
    <div id="DivBasic2">

        <textarea cols="80" id="basic21" name="basic21" rows="5" ></textarea>
        <script>
            CKEDITOR.replace('basic21', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],			// Defines toolbar group without name.
                    { name: 'basicstyles', items: ['Bold', 'Italic'] }
                ]
            });
	    </script>
    </div>
    <div id="DivBlock">

        <textarea cols="80" id="block1" name="block1" rows="5" ></textarea>
        <script>
            CKEDITOR.replace('block1', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],			// Defines toolbar group without name.
                    { name: 'basicstyles', items: ['Bold', 'Italic'] }
                ]
            });
	    </script>
    </div>
    <div id="DivTable">

        <textarea cols="80" id="table1" name="table1" rows="5" ></textarea>
        <script>
            CKEDITOR.replace('table1', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],			// Defines toolbar group without name.
                    { name: 'basicstyles', items: ['Bold', 'Italic'] }
                ]
            });
	    </script>
    </div>
    <div id="DivImage">

        <textarea cols="80" id="image1" name="image1" rows="5" ></textarea>
        <script>
            CKEDITOR.replace('image1', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],			// Defines toolbar group without name.
                    { name: 'basicstyles', items: ['Bold', 'Italic'] }
                ]
            });
	    </script>
    </div>
    <div id="DivComment">

        <textarea cols="80" id="comment1" name="comment1" rows="5" ></textarea>
        <script>
            CKEDITOR.replace('comment1', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],			// Defines toolbar group without name.
                    { name: 'basicstyles', items: ['Bold', 'Italic'] }
                ]
            });
	    </script>
    </div>
</div>
    <input type="button" value="텍스트 가져오기" onclick="getText();" />
    </form>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>
