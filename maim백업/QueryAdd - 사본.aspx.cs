﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class main_NewQuery : System.Web.UI.Page
{

    private string connectionString = string.Empty;
    private string resultCount = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

        resultCount = Request.Params["lblResultCount"];

    }
    protected void btnExercuteQuery_Click(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (txtQueryName.Text.Length == 0) { 
            showMessage("쿼리 이름을 입력하여 주십시오.");
            txtQueryName.Focus();
            return;
        }


        DBFileInfo();


        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@idx", SqlDbType.Int);
        Cmd.Parameters.Add("@queryname", SqlDbType.NVarChar, 50);
        Cmd.Parameters.Add("@querytext", SqlDbType.NText);
        Cmd.Parameters.Add("@resultcount", SqlDbType.Int);
        Cmd.Parameters.Add("@lastdate", SqlDbType.DateTime);


        Cmd.Parameters["@idx"].Value = idx.Value;
        Cmd.Parameters["@queryname"].Value = txtQueryName.Text.ToString();
        Cmd.Parameters["@querytext"].Value = txtQueryText.Text.ToString();
        //Cmd.Parameters["@resultcount"].Value = Convert.ToInt32 (lblResultCount.Text.ToString());
        Cmd.Parameters["@resultcount"].Value = Convert.ToInt32(resultCount);
        Cmd.Parameters["@lastdate"].Value = DateTime.Now;

        Cmd.CommandType = CommandType.StoredProcedure;
        Cmd.CommandText = "USP_Query_UPDATE";
        Con.Open();

        Cmd.ExecuteNonQuery();
        
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        Response.Redirect("./QueryList.aspx");
    }
    protected void btnAddCondition1_Click(object sender, EventArgs e)
    {

    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];

        //if (serverIP == "::1" || serverIP == "127.0.0.1")  //-- 테스트 서버
        //{
        //    connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        //}
        //else //-- 서비스서버
        //{
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        //}
    }

    private void showMessage(String message){

        string script = "alert(\""+ message +"\");";
        ScriptManager.RegisterStartupScript(this, GetType() , "ServerControlScript", script, true);

    }


    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        DataTable dt = executeQueryText( txtQueryText.Text.ToString() );
        String excelFileName = txtQueryName.Text.ToString() + ".xls";
        this.exportExcel(dt, excelFileName);
    }

    private DataTable executeQueryText( String query )
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(query, Con);
        Con.Open();

        SqlDataReader dataReader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        DataTable dataTable = new DataTable();
        dataTable.Load(dataReader);
        
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        return dataTable;
    }

    private void exportExcel( DataTable dataTable , String excelFileName ) {
        string attachment = "attachment; filename=" + excelFileName ;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentEncoding = System.Text.Encoding.Default;
        string tab = "";
        foreach (DataColumn dc in dataTable.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");

        int i;
        foreach (DataRow dr in dataTable.Rows)
        {
            tab = "";
            for (i = 0; i < dataTable.Columns.Count; i++)
            {
                Response.Write(tab + dr[i].ToString().TrimEnd());
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }
    protected void btnExecuteQuery_Click(object sender, EventArgs e)
    {
        DataTable dataTable = this.executeQueryText( txtQueryText.Text.ToString() );
        lblResultCount.Text = dataTable.Rows.Count.ToString();
    }
    protected void btnInit_Click(object sender, EventArgs e)
    {

    }
}