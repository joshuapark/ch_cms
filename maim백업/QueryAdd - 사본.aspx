﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QueryAdd - 사본.aspx.cs" Inherits="main_NewQuery" %>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-2.1.3.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.jqtransform.js"></script>
    <script src="../js/ui.js"></script>
    <script src="../js/jquery.js"></script>
    <script type="text/javascript">

        
        var strSelect = "";
        var strSelectHead = "SELECT ";
        var strSelectTail = " FROM TEntry";
        var strOrderBy = "";
        var strOrderByHead = " ORDER BY ";
        var strWhere = "";

        $(document).ready(function () {
            
            $("#<%=cblSelect.ClientID%>").click(function () {
                var strField = "";

                $("input[type='checkbox']:checked").each(function () {
                    var cbvalue = $(this).val()
                    strField = strField + cbvalue + ",";
                });
                strField = strField.substring(0, strField.length - 1);
                strSelect = strSelectHead + strField + strSelectTail;

                $("#<%=txtQueryText.ClientID%>").val( strSelect + strWhere + strOrderBy );
            });

            $('#btnAddWhere').click(function () {
                
                html = "";
                html += '<tr><td>';
                html += '<span>조건</span>'
                html += '<select id="selWhere">';
                html += '<option value="RevisionIdx">개정</option>';
                html += '<option value="SubjectGroupIdx">과목군</option>';
                html += '<option value="SubjectIdx">과목</option>';
                html += '<option value="SchoolIdx">학교군</option>';
                html += '<option value="BrandIdx">브랜드</option>';
                html += '<option value="GradeIdx">학년</option>';
                html += '<option value="SemesterIdx">학기</option>';
                html += '<option value="LUnitIdx">대단원</option>';
                html += '<option value="MUnitIdx">중단원</option>';
                html += '<option value="SeqNo">타이틀</option>';
                html += '<option value="EntryCode">엔트리코드</option>';
                html += '</select>'
                html += '<select id="selWhere">';
                html += '<option selected="selected">조건</option>';
                html += '<option value="=">=</option>';
                html += '<option value="<"><</option>';
                html += '<option value=">">></option>';
                html += '<option value="like">like</option>';
                html += '</select>'
                html += '<input type="text">';
                html += '<select id="selWhere">';
                html += '<option selected="selected">조건</option>';
                html += '<option value="AND">AND</option>';
                html += '<option value="OR">OR</option>';
                html += '</select>'
                html += '<input type="button" id="btnDeleteWhere" class="btn-code-del" border="0" />';
                html += '</td></tr>';

                $('#tblAddWhere').append(html);

            });

            $('#btnAddOrderBy').click(function () {

                html = "";
                html += '<tr><td value="hh">';
                html += '<span>조건</span>'
                html += '<select id="selOrderByText">';
                html += '<option value="RevisionIdx">개정</option>';
                html += '<option value="SubjectGroupIdx">과목군</option>';
                html += '<option value="SubjectIdx">과목</option>';
                html += '<option value="SchoolIdx">학교군</option>';
                html += '<option value="BrandIdx">브랜드</option>';
                html += '<option value="GradeIdx">학년</option>';
                html += '<option value="SemesterIdx">학기</option>';
                html += '<option value="LUnitIdx">대단원</option>';
                html += '<option value="MUnitIdx">중단원</option>';
                html += '<option value="SeqNo">타이틀</option>';
                html += '<option value="EntryCode">엔트리코드</option>';
                html += '</select>'
                html += '<select id="selOrderByMode">';
                html += '<option value="ASC">오름차순</option>';
                html += '<option value="DESC">내림차순</option>';
                html += '</select>'
                html += '<input type="button" id="btnDeleteOrderBy" class="btn-code-del" border="0" />';
                html += '</td></tr>';

                $('#tblAddOrderBy').append(html);

                addOrderByText();

            });

            $('#tblAddOrderBy').on('change', 'tr td select[id=selOrderByText]', function () {
                addOrderByText();
            });

            $('#tblAddOrderBy').on('change', 'tr td select[id=selOrderByMode]', function () {
                addOrderByText();
            });

            $('#btnExecuteQuery').click(function () {


                window.open("./QueryAddPopup.aspx?querytext=" + $("#<%=txtQueryText.ClientID%>").val(), "_BLANK", "width=500,height=500,scrollbars=1");

                //var retVal = window.showModalDialog(
                //             "./QueryAddPopup.aspx?querytext=" + $("#<%=txtQueryText.ClientID%>").val(), null ,
                //                "dialogWidth:440px;dialogHeight=400px;scroll=yes;status=yes;resizable=no;");

                // 팝업에서 넘겨온 값이 있다면, 출력
                //if (retVal) {
                //    alert(retVal);
                //}


            });

        })

        function addOrderByText() {

            var strField = "";
            $('#tblAddOrderBy tr td').each(function () {
                var orderByValue = $(this).children('#selOrderByText').val() + " " + $(this).children('#selOrderByMode').val();
                strField = strField + orderByValue + ",";
            });
            strField = strField.substring(0, strField.length - 1);
            strOrderBy = strOrderByHead + strField;

            $("#<%=txtQueryText.ClientID%>").val(strSelect + strWhere + strOrderBy);
        }

        function setResultCount(resultCount) {
            
            $('#lblResultCount').text(resultCount);
            $('#lblResultCount').val(resultCount);

        }

    </script>
    <script>
        $(document).ready(function () {
            var sql;
            $("#chkentry").change(function () {
                if ($(this).is(":checked")) {
                    alert("true");
                }
                else {
                    alert("false");
                }

            });
        });
    </script>
</head>

<body>

<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">

  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li class="active"><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="Statistics.aspx">통계</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

    <form id="addForm" runat="server" method="post">
    <asp:HiddenField ID="idx" runat="server" Value="-1"/>
	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">Query관리</h2>
		</div><!-- // title -->	

		<div class="title"><!-- title -->
			<h3 class="title title-success">Query</h3>
		</div><!-- // title -->

		<table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: 110px;">
			    <col style="width: auto;">
		    </colgroup>
		<tbody>
			<tr>
				<th>SELECT</th>
				<td>
                    <asp:checkboxlist ID="cblSelect" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table">
                        <asp:ListItem Name="checkbox" Text="개정" Value="RevisionIdx"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="과목군" Value="SubjectGroupIdx"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="과목" Value="SubjectIdx"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="학교군" Value="SchoolIdx"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="브랜드" Value="BrandIdx"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="학년" Value="GradeIdx"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="학기" Value="SemesterIdx"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="대단원" Value="LUnitIdx"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="중단원" Value="MUnitIdx"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="타이틀" Value="Title"></asp:ListItem>
                        <asp:ListItem Name="liSelect" Text="엔트리코드" Value="EntryCode"></asp:ListItem>
                    </asp:checkboxlist>
				</td>
			</tr>
			<tr>
				<th>Where<br>
                    <input type="button" id="btnAddWhere" value="조건 추가" />
				</th>
				<td>
                    <table id="tblAddWhere">

                    </table>

				</td>
			</tr>
			<tr>
				<th>Order by<br />
                    <input type="button" id="btnAddOrderBy" value="조건 추가" />
                </th>
				<td>
                    <table id="tblAddOrderBy">
                        
                    </table>
				</td>
			</tr>
			<tr>
				<th>편집</th>
				<td>
					<asp:TextBox runat="server" id="txtQueryText" TextMode="Multiline"  Rows="4" Columns="70"  />
				</td>
			</tr>
            
		</tbody>
		</table><!-- // table-a -->

	<div class="section-button"><!-- section-button -->
        <a class="btn-lg btn-success" id="btnExecuteQuery">Query 실행</a>
        <asp:Button CssClass="btn btn-lg btn-success" Text="초기화" ID="btnInit" runat="server" OnClick="btnInit_Click"/>
    </div><!-- section-button -->

    <div class="title"><!-- title -->
	    <h3 class="title title-success">결과</h3>
    </div><!-- // title -->
    <table border="0" cellpadding="0" cellspacing="0" class="table">
        <tr>
            <th>조회건수</th>
            <td>
                <asp:Label ID="lblResultCount" runat ="server"></asp:Label>
                <br>
                <asp:Button ID="btnExportExcel" runat="server" Text="엑셀 다운로드" OnClick="btnExportExcel_Click" />
            </td>
        </tr>
        <tr>
            <th>쿼리명</th>
            <td>
                <asp:TextBox ID="txtQueryName" runat="server" Text="쿼리이름" Columns="70" TextMode="SingleLine" ></asp:TextBox>
            </td>
        </tr>
    </table>
    
    <div class="section-button"><!-- section-button -->
        <asp:Button class="btn btn-lg btn-danger" ID="btnSave" runat="server" text="저장" onclick="btnSave_Click"/>
    </div><!-- section-button -->

	</div><!-- // contents -->

    </form>
        
	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->



</body>
</html>