﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

public partial class main_opencontents : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "1000";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    private string strCategory = string.Empty;
    private string strType = string.Empty;
    public string strTitle = "대분류";
    private string strEntryIdx = string.Empty;
    private string entryIdx = string.Empty;

    private DataTable categoryTable = new DataTable();
    private DataTable entryTable = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        ////로그인 처리
        //if (Session["uid"] == null)
        //{
        //    string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');window.opener=self;self.close();</script>";
        //    ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
        //    return;
        //}
        ////##### 권한 처리 시작
        //int uAuth = 1;
        //if (Session["uauth"].ToString().Length > 0)
        //{
        //    uAuth = Convert.ToInt32(Session["uauth"].ToString());
        //}
        //if (uAuth < 5)
        //{
        //    string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
        //    ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
        //    return;
        //}
        //else
        //    Session.Timeout = 120;
        ////##### 권한 처리 끝   

        if (!IsPostBack)
        {
            if (Request.Params["Idx"] != null)
            {
                entryIdx = Request.Params["Idx"].ToString();
                txtEntryIdx.Value = entryIdx;
                txtOutlineType.Value = "0";
                flgChangeOutline.Value = false.ToString();
            }
            BindOutlineType();
            GetOutline();
        }
        else
        {
            entryIdx = txtEntryIdx.Value;
        }
    }
    private void GetOutline()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        string strQuery = "SELECT OutlineIdx as Idx, OutlineName, OutlineData, OutlineType FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + txtEntryIdx.Value + " ORDER BY OutlineIdx ASC";
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Con.Open();

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");
        if (ds.Tables["data_list"].Rows.Count>0)
        {
            OutlineRepeater.DataSource = ds;
            OutlineRepeater.DataBind();
            flgExistOutline.Value = true.ToString();
            btnSave.Visible = true;
            txtOutlineType.Value = ds.Tables["data_list"].Rows[0]["OutlineType"].ToString();
            ddlOutlineType.SelectedValue = txtOutlineType.Value;
        }
        else
        {
            flgExistOutline.Value = false.ToString();
            btnSave.Visible = false;
        }
        //Close the connection.
        Con.Close();
    }
    private void BindOutlineType()
    {

        ddlOutlineType.Items.Clear();

        ListItem li = new ListItem("선택", "0");
        ddlOutlineType.Items.Add(li);

        ddlOutlineType.AppendDataBoundItems = true;
        
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand("SELECT OutlineType, OutlineTypeName FROM TOutlineType WHERE DelFlag=0 ORDER BY OutlineTypeName ASC", Conn);
        Conn.Open();
        SqlDataReader Reader = Cmd.ExecuteReader();

        //Set up the data binding.
        ddlOutlineType.DataSource = Reader;
        ddlOutlineType.DataTextField = "OutlineTypeName";
        ddlOutlineType.DataValueField = "OutlineType";
        ddlOutlineType.DataBind();
        ddlOutlineType.AppendDataBoundItems = false;
        //Close the connection.
        Conn.Close();
        Reader.Close();
    }


    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버  
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        // OutlineType이 변경(flgChangeOutline)되었고 OutlineType Value가 다르다면
        StringBuilder strOutline = new StringBuilder();
        strOutline.Append("<table class=\"table\">");
        DBFileInfo();
        if(!Convert.ToBoolean(flgChangeOutline.Value) && (txtOutlineType.Value==ddlOutlineType.SelectedValue))
        {
            
            try
            {
                SqlConnection Con = new SqlConnection(connectionString);
                Con.Open();
                SqlCommand Cmd = null;
                
                foreach (RepeaterItem item in OutlineRepeater.Items)
                {
                    Label lblOutlineIdx = item.FindControl("lblOutlineIdx") as Label;
                    Label lblOutlineName = item.FindControl("lblOutlineName") as Label;
                    TextBox txtOutlineData = item.FindControl("txtOutlineData") as TextBox;
                    Cmd = new SqlCommand("UPDATE TOutlineEntry SET OutlineData='"+ txtOutlineData.Text +"', EditDate=getdate() WHERE EntryIdx=" + txtEntryIdx.Value + " AND OutlineIdx=" + lblOutlineIdx.Text + " ", Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.ExecuteNonQuery();
                    strOutline.Append("<tr><th>" + lblOutlineName.Text + "</th><td>" + txtOutlineData.Text + "</td></tr>");
                    //strOutline.Append("[" + lblOutlineName.Text + "]  " + txtOutlineData.Text + "</br>");
                }

                Con.Close();

                Cmd = null;
                Con = null;
                //string strSelectEntryTitle = strRevision + strSubjectGroup + strSubject + strSchool + strBrand + strGrade + strSemester + strLUnit + strMUnit;
                //strSelectEntryTitle = strSelectEntryTitle.Substring(0, strSelectEntryTitle.Length - 1);
                //Value 설정하기
                
                //string strSelectEntryIdx = strRevision + strSubjectGroup + strSubject + strSchool + strBrand + strGrade + strSemester + strLUnit + strMUnit;
                //strSelectEntryIdx = strSelectEntryIdx.Substring(0, strSelectEntryIdx.Length - 1);

                //Response.Write("<script>opener.location.href='EntryEdit.aspx?exportIdx=" + exportIdx + "';window.opener=self;self.close();</script>");
                //Response.Write("<script>opener.document.getElementById('txtCategory').innerHTML ='" + strSelectEntryTitle + "';opener.document.getElementById('hfCategoryIdx').value ='" + strSelectEntryIdx + "';window.opener=self;self.close();</script>");
            }
            catch (Exception ex)
            {
                Response.Write("개요부 저장 중 오류가 발생했습니다.<br />오류내용:" + ex.ToString());
            }
            //Response.Redirect("ExportAdd.aspx?exportIdx=" + txtExportIdx.Value + "");
            //Response.Write("<script>opener.EntSum();self.close();</script>");
        }
        else
        {
            try
            {
                SqlConnection Con = new SqlConnection(connectionString);
                Con.Open();
                SqlCommand Cmd = null;
                //## 기존의 개요부 내용이 있었다면 삭제해준다.
                if (Convert.ToBoolean(flgExistOutline.Value))
                {
                    Cmd = new SqlCommand("DELETE FROM TOutlineEntry WHERE EntryIdx=" + txtEntryIdx.Value + " ", Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.ExecuteNonQuery();
                }
                foreach (RepeaterItem item in OutlineRepeater.Items)
                {
                    Label lblOutlineIdx = item.FindControl("lblOutlineIdx") as Label;
                    Label lblOutlineName = item.FindControl("lblOutlineName") as Label;
                    TextBox txtOutlineData = item.FindControl("txtOutlineData") as TextBox;

                    Cmd = new SqlCommand("INSERT TOutlineEntry (EntryIdx, OutlineIdx, OutlineData) VALUES (" + txtEntryIdx.Value + ", " + lblOutlineIdx.Text + ", '" + txtOutlineData.Text + "')", Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.ExecuteNonQuery();
                    strOutline.Append("<tr><th>" + lblOutlineName.Text + "</th><td>" + txtOutlineData.Text + "</td></tr>");
                    //strOutline.Append("[" + lblOutlineName.Text + "]  " + txtOutlineData.Text + "</br>");
                }

                Con.Close();

                Cmd = null;
                Con = null;

                //Response.Write("<script>alert('대단원과 중단원까지 선택하셔야 합니다.');</script>");
                //return;
            }
            catch (Exception ex)
            {
                Response.Write("개요부 입력 중 오류가 발생했습니다.<br />오류내용:" + ex.ToString());
            }
        }
        strOutline.Append("</table>");
        Response.Write("<script>alert('저장되었습니다.');opener.document.getElementById('lblOutline').innerHTML ='" + strOutline.ToString() + "';window.opener=self;self.close();</script>");

    }
    protected void ddlOutlineType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand("SELECT Idx, OutlineName, '' as OutlineData FROM TOutline WHERE OutlineType=" + ddlOutlineType.SelectedValue + " AND DelFlag=0 ORDER BY Idx ASC", Conn);
        Conn.Open();

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        OutlineRepeater.DataSource = ds;
        OutlineRepeater.DataBind();

        if (OutlineRepeater.Items.Count > 0)
            btnSave.Visible = true;
        else
            btnSave.Visible = false;
        //Close the connection.
        Conn.Close();
        flgChangeOutline.Value = true.ToString();
    }
}
