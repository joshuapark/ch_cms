﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class main_NewQuery : System.Web.UI.Page
{

    private string connectionString = string.Empty;
    private string resultCount = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }  
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝  

        resultCount = Request.Params["lblResultCount"];
        if(!IsPostBack)
        {
            btnExportExcel.Visible = false;
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (txtQueryName.Text.Length == 0) { 
            showMessage("Query명을 입력하여 주십시오.");
            txtQueryName.Focus();
            return;
        }
        DBFileInfo();


        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@idx", SqlDbType.Int);
        Cmd.Parameters.Add("@queryname", SqlDbType.NVarChar, 50);
        Cmd.Parameters.Add("@querytext", SqlDbType.NText);
        Cmd.Parameters.Add("@resultcount", SqlDbType.Int);
        Cmd.Parameters.Add("@lastdate", SqlDbType.DateTime);


        Cmd.Parameters["@idx"].Value = idx.Value;
        Cmd.Parameters["@queryname"].Value = txtQueryName.Text.ToString();
        Cmd.Parameters["@querytext"].Value = txtQueryText.Text.ToString();
        int intResultCount = 0;
        if (lblResultCount.Text.Length > 0)
            intResultCount =  Convert.ToInt32 (lblResultCount.Text);

        Cmd.Parameters["@resultcount"].Value = intResultCount;
        Cmd.Parameters["@lastdate"].Value = DateTime.Now;

        Cmd.CommandType = CommandType.StoredProcedure;
        Cmd.CommandText = "USP_Query_UPDATE";
        Con.Open();

        Cmd.ExecuteNonQuery();
        
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
        showMessage("저장되었습니다.");
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];

        //if (serverIP == "::1" || serverIP == "127.0.0.1")  //-- 테스트 서버
        //{
        //    connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        //}
        //else //-- 서비스서버
        //{
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        //}
    }

    private void showMessage(String message){

        string script = "alert(\""+ message +"\");";
        ScriptManager.RegisterStartupScript(this, GetType() , "ServerControlScript", script, true);

    }


    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        DataTable dt = executeQueryText( txtQueryText.Text.ToString() );
        String excelFileName = "Query_Export_"+ System.DateTime.Now.ToString("yyyyMMddhhmm") +".xls";
        this.exportExcel(dt, excelFileName);
    }

    private DataTable executeQueryText( String query )
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(query, Con);
        Con.Open();

        SqlDataReader dataReader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        DataTable dataTable = new DataTable();
        dataTable.Load(dataReader);
        
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        return dataTable;
    }

    private void exportExcel( DataTable dataTable , String excelFileName ) {
        string attachment = "attachment; filename=" + excelFileName ;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentEncoding = System.Text.Encoding.Default;
        string tab = "";
        foreach (DataColumn dc in dataTable.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");

        int i;
        foreach (DataRow dr in dataTable.Rows)
        {
            tab = "";
            for (i = 0; i < dataTable.Columns.Count; i++)
            {
                Response.Write(tab + dr[i].ToString().TrimEnd());
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }
    protected void btnExecuteQuery_Click(object sender, EventArgs e)
    {
        //초기화
        ItemsGrid.DataSource = null;
        ItemsGrid.DataBind();
        lblResultCount.Text = "";
        btnExportExcel.Visible = false;

        string strSelect=string.Empty, strFrom=string.Empty, strWhere=string.Empty;
        if (txtSelect.Text.Length > 0) { 
            strSelect = "SELECT " +  txtSelect.Text;
        }

        if (txtFrom.Text.Length > 0)
            strFrom = " FROM " + txtFrom.Text;

        if (txtWhere.Text.Length > 0)
            strWhere = " WHERE " + txtWhere.Text;

        string strQuery = strSelect + strFrom + strWhere;
        strQuery = strQuery.Replace("\"", "'").Replace(";" ,"");
        strQuery = Regex.Replace(strQuery, "INSERT", "", RegexOptions.IgnoreCase);
        strQuery = Regex.Replace(strQuery, "UPDATE", "", RegexOptions.IgnoreCase);
        strQuery = Regex.Replace(strQuery, "DELETE", "", RegexOptions.IgnoreCase);

        txtQueryText.Text = strQuery;
        try
        {
            DataTable dataTable = this.executeQueryText(strQuery);
            //DataView dataView = new DataView(dataTable);
            ItemsGrid.DataSource = dataTable;
            ItemsGrid.DataBind();
            lblResultCount.Text = dataTable.Rows.Count.ToString();
            if (dataTable.Rows.Count>0)
                btnExportExcel.Visible = true;

        }
        catch(Exception ex)
        {
            txtQueryText.Text = strQuery + "<br />" + "쿼리실행 중 오류가 발생했습니다. <br />" + ex.ToString().Substring(0, 120)+"...";
        }
        
    }

}