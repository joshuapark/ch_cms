﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Drawing;

public partial class main_List : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private bool _refreshState;
    private bool _isRefresh;
    private string pageNo = "1";
    private string pageSize = "50";
    private int totalCnt = 0;
    public bool IsRefresh
    {
        get { return _isRefresh; }
    }
    protected override void LoadViewState(object savedState)
    {
        object[] allStates = (object[])savedState;
        base.LoadViewState(allStates[0]);
        _refreshState = (bool)allStates[1];
        _isRefresh = _refreshState == (bool)Session["__ISREFRESH"];
    }
    protected override object SaveViewState()
    {
        Session["__ISREFRESH"] = _refreshState;
        object[] allStates = new object[2];
        allStates[0] = base.SaveViewState();
        allStates[1] = !_refreshState;
        return allStates;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  


        if (!IsPostBack)
        {
            BinderListing();
        }

    }

    protected void BinderListing()
    {
        // DB Connection String, File Path 설정
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.CommandText = "USP_Binder_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;
        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);
        Cmd.Parameters.Add("@keyword", SqlDbType.NVarChar, 255);

        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);
        Cmd.Parameters["@keyword"].Value = txtKeyword.Text;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        BinderListRepeater.DataSource = ds;
        BinderListRepeater.DataBind();

        string totalCount = "0";
        if (BinderListRepeater.Items.Count > 0)
            totalCount = ds.Tables["data_list"].Rows[0]["totalCnt"].ToString();
        lblTotalCount.Text = totalCount + "건의 검색결과가 있습니다.";

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }
  
    protected void AddButton_Click(object sender, EventArgs e)
    {
        if (txtAddBinderTitle.Text.Trim().Length > 1)  //코드타입명을 입력했는지 체크한다.
        {
            DBFileInfo();
            SqlConnection Con0 = new SqlConnection(connectionString);
            SqlCommand Cmd0 = new SqlCommand("SELECT BinderTitle FROM TBinder WHERE BinderTitle='" + txtAddBinderTitle.Text.Trim() + "' AND DelFlag=0 ", Con0);
            Cmd0.CommandType = CommandType.Text;
            Con0.Open();

            SqlDataReader reader = Cmd0.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                //msgIDCheck.InnerText = "* " + txtCodeType.Text.Trim() + " 코드타입은 이미 등록되어 있습니다. 다른 코드타입을 입력하세요.";
                string script = "<script>alert('[" + txtAddBinderTitle.Text.Trim() + "] 바인더는 이미 등록되어 있습니다.');</script>";
                txtAddBinderTitle.Text = "";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                Con0.Close();
                Con0 = null;
                Cmd0 = null;
                Response.Write(script);
            }
            else
            {
                SqlConnection Con = new SqlConnection(connectionString);

                SqlCommand Cmd = new SqlCommand("INSERT INTO TBinder (BinderTitle, BinderMemo, UserIdx) VALUES ( '" + txtAddBinderTitle.Text.Trim() + "', '" + txtAddBinderMemo.Text.Trim() + "', " + Session["uidx"].ToString() + ")", Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                Cmd = null;
                Con = null;

                txtAddBinderTitle.Text = "";

                BinderListing();
            }
        }
    }

    protected void BinderListRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //string delID;
        //Button clickedButton = (Button)sender;
        //delID = clickedButton.ID;
        Control ctl = source as Control;
        var argument = e.CommandArgument;

        //CommandEventArgs cea = e as CommandEventArgs;
        if (ctl != null)
        {
            if (argument.ToString() == "D")
            {
                RepeaterItem repeaterItem = BinderListRepeater.Items[e.Item.ItemIndex];
                Label lblBinderIdx = repeaterItem.FindControl("lblNo") as Label;
                string delID = lblBinderIdx.Text;

                DBFileInfo();

                SqlConnection Con = new SqlConnection(connectionString);

                SqlCommand Cmd = new SqlCommand("UPDATE TBinder SET DelFlag=1 WHERE BinderIdx = " + delID +"; UPDATE TBinderEntry SET DelFlag=1 WHERE BinderIdx = " + delID +"; UPDATE TBinderRelation SET DelFlag=1 WHERE BinderIdx = " + delID +";", Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                Cmd = null;
                Con = null;

                string script = "<script>alert('삭제되었습니다');</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);

                BinderListing();
            }
            else if (argument.ToString() == "G")
            {
                RepeaterItem repeaterItem = BinderListRepeater.Items[e.Item.ItemIndex];
                Label lblBinderIdx = repeaterItem.FindControl("lblNo") as Label;

                Response.Redirect("BinderEdit.aspx?Idx=" + lblBinderIdx.Text);
            }
            else if (argument.ToString() == "S")
            {
                try
                {
                    DBFileInfo();
                    SqlConnection Con = new SqlConnection(connectionString);
                    Con.Open();
                    SqlCommand Cmd = null;
                    RepeaterItem repeaterItem = BinderListRepeater.Items[e.Item.ItemIndex];
                    Label lblNo = repeaterItem.FindControl("lblNo") as Label;
                    TextBox txtBinderTitle = repeaterItem.FindControl("txtBinderTitle") as TextBox;
                    TextBox txtBinderMemo = repeaterItem.FindControl("txtBinderMemo") as TextBox;
                    if (txtBinderTitle.Text.Length > 0)
                    {
                        Cmd = new SqlCommand("UPDATE TBinder SET BinderTitle='" + txtBinderTitle.Text + "', BinderMemo='" + txtBinderMemo.Text + "' WHERE BinderIdx=" + lblNo.Text + " ", Con);
                        Cmd.CommandType = CommandType.Text;

                        Cmd.ExecuteNonQuery();
                    }
                    Con.Close();

                    Cmd = null;
                    Con = null;

                    Response.Write("<script>alert('저장되었습니다.');</script>");
                }
                catch (Exception ex)
                {
                    Response.Write("바인더 저장 중에 오류가 발생했습니다.<br />오류내용" + ex.ToString());
                }
            }
        }
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];

        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    protected void SearchButton_Click(object sender, EventArgs e)
    {
        BinderListing();
    }
}