﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

public partial class notice_List : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "1000";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }  
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝  
        
        //if (!IsPostBack)
        //{
            if (Request.Params["pageNo"] != null)
                pageNo =Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null)
                pageSize = Request.Params["pageSize"].ToString(); 
            
            Listing();
        //}
    }

    private void Listing()
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_DaumExport_LIST_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);

        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "list_data");

        LV_Notice.DataSource = ds;
        LV_Notice.DataBind();

        int totalPage = ((int)totalCnt - 1) / Convert.ToInt32(pageSize) + 1;
        MakePage(totalCnt, Convert.ToInt32(pageNo), totalPage); 
        
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    protected void ListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem item = (ListViewDataItem)e.Item;
        // 아이템의 타입이 DataItem일 경우
        if (item.ItemType == ListViewItemType.DataItem)
        { 
            // 글 번호
            Label lblNo = item.FindControl("lblNo") as Label;
            lblNo.Text = DataBinder.Eval(item.DataItem, "Idx").ToString();

            // Export작업명
            Label lblExportTitle = item.FindControl("lblExportTitle") as Label;
            //lblExportTitle.Text = "<a href=\"ExportEdit.aspx?exportIdx=" + DataBinder.Eval(item.DataItem, "Idx").ToString() + "\">" + DataBinder.Eval(item.DataItem, "ExportTitle").ToString() + "</a>";
            lblExportTitle.Text = DataBinder.Eval(item.DataItem, "ExportTitle").ToString() ;

            // 엔트리수
            Label lblEntryCount = item.FindControl("lblEntryCount") as Label;
            lblEntryCount.Text = DataBinder.Eval(item.DataItem, "EntryCount").ToString();

            // 최초발급일
            Label lblFirstDate = item.FindControl("lblFirstDate") as Label;
            lblFirstDate.Text = DataBinder.Eval(item.DataItem, "FirstDate").ToString();

            // 작업명
            //Label lblTaskTitle = item.FindControl("lblTaskTitle") as Label;
            
            //if (Convert.ToInt16(DataBinder.Eval(item.DataItem, "Status").ToString()) == 0)
            //    lblTaskTitle.Text = "<a href=\"TaskEdit.aspx?TaskIdx=" + DataBinder.Eval(item.DataItem, "Idx").ToString() + "\">" + DataBinder.Eval(item.DataItem, "TaskTitle").ToString() +"</a>";
            //else
            //    lblTaskTitle.Text = DataBinder.Eval(item.DataItem, "TaskTitle").ToString();


            // 파일링크를 설정한다.
            Label lblList = item.FindControl("lblList") as Label;
            //hlEntryList.NavigateUrl = 
            string strLink = "<a target='_blank' href=\"DownloadFile.aspx?dfn=" + DataBinder.Eval(item.DataItem, "XMLFile").ToString() + "&Type=X\">"
                + DataBinder.Eval(item.DataItem, "XMLFile").ToString() + "</a><br />"
                + "<a target='_blank' href=\"/cms100data/EntryData/" + DataBinder.Eval(item.DataItem, "ImgFile").ToString() + "\">"                
                + DataBinder.Eval(item.DataItem, "ImgFile").ToString() + "</a><br />"
                + "<a target='_blank' href=\"DownloadFile.aspx?dfn=" + DataBinder.Eval(item.DataItem, "MultimediaXML").ToString() + "&Type=X\">"
                + DataBinder.Eval(item.DataItem, "MultimediaXML").ToString() + "</a><br />"
                + "<a target='_blank' href=\"DownloadFile.aspx?dfn=" + DataBinder.Eval(item.DataItem, "CategoryXML").ToString() + "&Type=X\">"
                + DataBinder.Eval(item.DataItem, "CategoryXML").ToString() + "</a>"; 
            lblList.Text = strLink;
            
            totalCnt = Convert.ToInt32(DataBinder.Eval(item.DataItem, "totalCnt"));
        }
    }


    protected void AddButton_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("DaumExportAdd.aspx?pageNo={0}&pageSize={1}", pageNo, pageSize));
    }

    private void MakePage(int TotalRecord, int CurPage, int TotalPage)
    {
        //링크 문자열
        string Path = Request.ServerVariables["PATH_INFO"].ToString() + "?pageNo=";
        string addParam = "&pageSize=" + pageSize;
        //FromPage 페이지 네비게이션 시작 페이지 번호
        //Curpage 페이지 네비게이션 마지막 페이지 번호
        int FromPage, ToPage;
        FromPage = (int)((CurPage - 1) / 10) * 10 + 1;
        if (TotalPage > FromPage + 9)
        {
            ToPage = FromPage + 9;
        }
        else ToPage = TotalPage;
        string Pager = "";
        int i;

        //이전 10개 표시
        if ((int)((CurPage - 1) / 10) > 0)
        {
            Pager = Pager + "<li><a href='" + Path + (FromPage - 1).ToString() + addParam + "'><font size='2'>이전페이지</font></a></li>";
        }

        //페이지 네비게이션 표시
        for (i = FromPage; i <= ToPage; i++)
        {
            if (i == CurPage)
            {
                Pager += "<li class='active'><a>" + i.ToString() + "</a></li>";
            }
            else
            {
                Pager = Pager + "<li><a href='" + Path + i.ToString() + addParam + "'>" + i.ToString() + "</a></li>";
            }
        }

        //다음 10개 표시
        if (ToPage < TotalPage)
        {
            Pager = Pager + "<li><a href='" + Path + (ToPage + 1).ToString() + addParam + "'><font size='2'>다음페이지</font></a></li>";
        }

        //페이지 네비게이션 출력하기
        lblPage = Pager;
        //Prev, Next 버튼의 링크 구성하기
        //if (CurPage > 1)
        //    hlPagePrev.NavigateUrl = Path + (CurPage - 1).ToString();
        //if (CurPage < ToPage)
        //    hlPageNext.NavigateUrl = Path + (CurPage + 1).ToString();
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }                                                        
}
