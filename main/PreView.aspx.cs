﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class main_PreView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string content = string.Empty;
        string html = string.Empty;
        if (Request.Params["previewContent"] != null)
        {
            if (Request.Params["previewContent"].ToString().Length > 0)
            {
                content = Request.Params["previewContent"].ToString();

                Edu edu = JsonConvert.DeserializeObject<Edu>(content);
                html += "<h2 align=\"center\">" + edu.Title + "</h2>";
                
                //대분류
                List<LUnit> lunitList = edu.LUnit;
                foreach (LUnit lunit in lunitList)
                {
                    List<Child> lunitChildList = lunit.list;
                    foreach (Child secondItem in lunitChildList)
                    {

                        if (secondItem.content != null)
                        {
                            string secondContent = string.Empty;
                            
                                if (Convert.ToString(secondItem.rectype).Equals("MAINTITLE"))
                                {
                                    html += "<h3 class=\"chapterTitle\">" + secondItem.content + "</h3>";
                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("SUBTITLE"))
                                {
                                    html += "<h4 class=\"unitTitle\">" + secondItem.content + "</h4>";
                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("BASIC")) {

                                    html += "<p class=\"deftext align\">";

                                    int imgEndIndex = 0;
                                    string strBasic = secondItem.content;
                                    int imgStartIndex = strBasic.IndexOf("<img", 0);
                                    if (imgStartIndex > -1)
                                    {

                                        html += strBasic.Substring( 0  , imgStartIndex);

                                        while (imgStartIndex > -1)
                                        {
                                            imgEndIndex = strBasic.IndexOf(">", imgStartIndex);

                                            string strImg = strBasic.Substring(imgStartIndex, imgEndIndex - imgStartIndex + 1);
                                            int strImgEndIndex = strImg.IndexOf(">", 0);
                                            strImg = strImg.Insert(strImgEndIndex, " class=\"imgSpan_inLine\"/");
                                            imgStartIndex = strBasic.IndexOf("<img", imgEndIndex);
                                            html += strImg;
                                            if (imgStartIndex > -1)
                                            {
                                                html += strBasic.Substring(imgEndIndex + 1, imgStartIndex - imgEndIndex -1);
                                            }

                                        }

                                        html += strBasic.Substring(imgEndIndex + 1 , strBasic.Length - imgEndIndex -1);
                                    }
                                    else 
                                    {
                                        html += strBasic;
                                    }

                                    html += "</p>";
                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("BASIC2"))
                                {

                                    html += "<p class=\"subtext align\">";

                                    int imgEndIndex = 0;
                                    string strBasic = secondItem.content;
                                    int imgStartIndex = strBasic.IndexOf("<img", 0);
                                    if (imgStartIndex > -1)
                                    {
                                        html += strBasic.Substring(0, imgStartIndex);

                                        while (imgStartIndex > -1)
                                        {
                                            imgEndIndex = strBasic.IndexOf(">", imgStartIndex);
                                            string strImg = strBasic.Substring(imgStartIndex, imgEndIndex - imgStartIndex + 1);
                                            int strImgEndIndex = strImg.IndexOf(">", 0);
                                            strImg = strImg.Insert(strImgEndIndex, " class=\"imgSpan_inLine\"/");
                                            imgStartIndex = strBasic.IndexOf("<img", imgEndIndex);
                                            html += strImg;

                                            if (imgStartIndex > -1)
                                            {
                                                html += strBasic.Substring(imgEndIndex + 1, imgStartIndex - imgEndIndex - 1);
                                            }

                                        }

                                        html += strBasic.Substring(imgEndIndex + 1, strBasic.Length - imgEndIndex - 1);
                                    }
                                    else
                                    {
                                        html += strBasic;
                                    }

                                    html += "</p>";
                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("BOX"))
                                {
                                    html += "<p class=\"blocktext align2\">" + secondItem.content + "</p>";

                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("MATRIX_TABLE")) 
                                {
                                    List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);

                                    string strTable = string.Empty;
                                    string strCaption = string.Empty;
                                    string strDescription = string.Empty;

                                    foreach (Child child in childList)
                                    {
                                        if (!child.content.Equals("<p><br></p>"))
                                        {
                                            if (child.rectype.Equals(string.Empty)) //이미지 노드
                                            {
                                                strTable = child.content.Replace("</br>", string.Empty).Replace("<br>", string.Empty).Replace("<br/>", string.Empty);
                                                html += strTable;
                                            }
                                            else if (child.rectype.Equals("CAPTION", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                strCaption = child.content.Replace("</br>", string.Empty).Replace("<br>", string.Empty).Replace("<br/>", string.Empty);
                                            }
                                            else if (child.rectype.Equals("DESCRIPTION", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                strDescription = child.content.Replace("</br>", string.Empty).Replace("<br>", string.Empty).Replace("<br/>", string.Empty);
                                            }
                                        }
                                    }

                                    if (!strCaption.Equals(string.Empty))
                                    {
                                        html += "<p class=\"text_caption\">" + strCaption + "</p>";
                                    }

                                    if (!strDescription.Equals(string.Empty))
                                    {
                                        html += "<p class=\"text_caption\">" + strDescription + "</p>";
                                    }

                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("IMAGE"))
                                {
                                    html += "<figure class=\"imgSpan\">";

                                    List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);

                                    List<string> imageList = new List<string>();
                                    string strCaption = string.Empty;
                                    string strLink = string.Empty;
                                    string strDescription = string.Empty;

                                    foreach (Child child in childList)
                                    {
                                        if (!child.content.Equals("<p><br></p>"))
                                        {
                                            if (child.rectype.Equals(string.Empty)) //이미지 노드
                                            {
                                                //이미지가 몇개 있는지 확인한다.
                                                int imgEndIndex = 0;
                                                string strImageNode = child.content;
                                                int imgStartIndex = strImageNode.IndexOf("<img", 0);
                                                while (imgStartIndex > -1)
                                                {
                                                    imgEndIndex = strImageNode.IndexOf(">", imgStartIndex);
                                                    string strImg = strImageNode.Substring(imgStartIndex, imgEndIndex - imgStartIndex + 1);
                                                    int strImgEndIndex = strImg.IndexOf(">", 0);

                                                    if (strCaption.Equals(string.Empty))
                                                    {
                                                        //strImg = strImg.Insert(strImgEndIndex, " class=\"imgT1\"/");

                                                    }
                                                    else
                                                    {
                                                        //strImg = strImg.Insert(strImgEndIndex, " alt=\"" + strCaption + "\"  title=\"" + strCaption + "\" class=\"imgT1\"/");
                                                        strImg = strImg.Insert(strImgEndIndex, " alt=\"" + strCaption + "\"  title=\"" + strCaption + "\"/");
                                                    }


                                                    if (!strLink.Equals(string.Empty))
                                                    {
                                                        strImg = "<a href=\"" + strLink + "\" target=\"_blank\" class=\"img_hyplink\">" + strImg + "</a>";
                                                    }
                                                    strImg = strImg + "</br>";
                                                    html += strImg;

                                                    imgStartIndex = strImageNode.IndexOf("<img", imgEndIndex);
                                                }


                                            }
                                            else if (child.rectype.Equals("CAPTION", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                strCaption = child.content.Replace("</br>", string.Empty).Replace("<br>", string.Empty).Replace("<br/>", string.Empty);
                                            }
                                            else if (child.rectype.Equals("Link", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                strLink = child.content.Replace("</br>", string.Empty).Replace("<br>", string.Empty).Replace("<br/>", string.Empty);
                                            }
                                            else if (child.rectype.Equals("DESCRIPTION", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                strDescription = child.content.Replace("</br>", string.Empty).Replace("<br>", string.Empty).Replace("<br/>", string.Empty);
                                            }
                                        }
                                    }

                                    if (!strCaption.Equals(string.Empty))
                                    {
                                        html += "<p class=\"text_caption\">" + strCaption + "</p>";
                                    }

                                    if (!strDescription.Equals(string.Empty))
                                    {
                                        html += "<p class=\"text_caption\">" + strDescription + "</p>";
                                    }


                                    html += "</figure>";
                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("ANNOTATION"))
                                {
                                    html += "<p class=\"subtext align\">" + secondItem.content + "</p>";
                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("RELATEDSEARCH"))
                                {
                                    html += "<p class=\"othertext\">" + secondItem.content + "</p>";
                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("CHDIVIDE"))
                                {


                                    string strChDivide = secondItem.content.Replace("<br/>", string.Empty);
                                    if (!strChDivide.Equals(string.Empty))
                                    {
                                        string[] splitedChDivide = strChDivide.Split(new char[] { '|' });
                                        if (splitedChDivide.Length > 1)
                                        {
                                            html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";

                                            int index = splitedChDivide.Length / 2;
                                            html += "<tr>";
                                            for (int i = 0; i < index; i++)
                                            {
                                                html += "<td class=\"tdHeadType1\">" + splitedChDivide[2 * i] + "</td>";

                                            }
                                            html += "</tr>";
                                            html += "<tr>";
                                            for (int i = 0; i < index; i++)
                                            {
                                                html += "<td class=\"sp_chi\">" + splitedChDivide[2 * i + 1].Replace("_", string.Empty) + "</td>";

                                            }
                                            html += "</tr>";
                                            html += "</tbody></table></br>";
                                        }
                                    }

                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("SOUND") || Convert.ToString(secondItem.rectype).Equals("VIDEO"))
                                {
                                    html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";

                                    List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);

                                    List<string> imageList = new List<string>();
                                    string strCaption = string.Empty;
                                    string strLink = string.Empty;
                                    string strDescription = string.Empty;

                                    foreach (Child child in childList)
                                    {
                                        if (!child.content.Equals("<p><br></p>"))
                                        {
                                            if (child.rectype.Equals(string.Empty)) //이미지 노드
                                            {
                                                html += "<tr>";
                                                html += "<td class=\"question_1\">파일명</td>";
                                                html += "<td class=\"question_2\"><a target=\"_blank\" href=\"" + edu.FileUrl + child.content.Replace("<br/>", string.Empty) + "\">" + child.content.Replace("<br/>", string.Empty) + "</a></td>";
                                                html += "</tr>";
                                            }
                                            else if (child.rectype.Equals("SENTENCE", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                html += "<tr>";
                                                html += "<td class=\"question_1\">문장</td>";
                                                html += "<td class=\"question_2\">" + child.content + "</td>";

                                                html += "</tr>";
                                            }
                                        }
                                    }

                                    html += "</tbody></table></br>";

                                }
                                else if (Convert.ToString(secondItem.rectype).Equals("PAIRSENTENCE"))
                                {
                                    html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";

                                    List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);

                                    List<string> imageList = new List<string>();
                                    string strCaption = string.Empty;
                                    string strLink = string.Empty;
                                    string strDescription = string.Empty;

                                    foreach (Child child in childList)
                                    {
                                        if (!child.content.Equals("<p><br></p>"))
                                        {

                                            if (child.rectype.Equals(string.Empty)) //이미지 노드
                                            {
                                                html += "<tr>";
                                                html += "<td class=\"question_1\">파일명</td>";
                                                html += "<td class=\"question_2\"><a target=\"_blank\" href=\"" + edu.FileUrl + child.content.Replace("<br/>", string.Empty) + "\">" + child.content.Replace("<br/>", string.Empty) + "</a></td>";
                                                html += "</tr>";
                                            }
                                            else if (child.rectype.Equals("SENTENCE", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                html += "<tr>";
                                                html += "<td class=\"question_1\">문장</td>";
                                                html += "<td class=\"question_2\">" + child.content + "</td>";

                                                html += "</tr>";
                                            }
                                            else if (child.rectype.Equals("TRANSLATION", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                html += "<tr>";
                                                html += "<td class=\"question_1\">해석</td>";
                                                html += "<td class=\"question_2\">" + child.content + "</td>";
                                                html += "</tr>";
                                            }
                                            else if (child.rectype.Equals("GRAMMAR", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                html += "<tr>";
                                                html += "<td class=\"question_1\">문법</td>";
                                                html += "<td class=\"question_2\">" + child.content + "</td>";
                                                html += "</tr>";
                                            }
                                        }
                                    }

                                    html += "</tbody></table></br>";
                                }
                                else
                                {
                                    html += secondItem.content;
                                }
                                
                            }
                        }
                }

                //퀴즈
                List<Quiz> quizList = edu.Quiz;
                if (quizList != null && quizList.Count > 0 )
                {
                    html += "<h3 class=\"chapterTitle\">관련문제</h3>";

                    foreach (Quiz quiz in quizList)
                    {
                        List<Child> quizChildList = quiz.list;

                        foreach (Child secondItem in quizChildList)
                        {

                            if (secondItem.content != null)
                            {
                                string secondContent = string.Empty;
                                
                                    if (Convert.ToString(secondItem.rectype).Equals("QUESTION"))
                                    {
                                        html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";
                                        html += "<tr>";
                                        html += "<td class=\"question_1\">문제</td>";
                                        html += "<td class=\"question_2\">" + secondItem.content.Replace("<br/>", string.Empty) + "</td>";
                                        html += "</tr>";
                                        html += "</tbody></table></br>";
                                    }
                                    else if (Convert.ToString(secondItem.rectype).Equals("LEVEL"))
                                    {
                                        html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";
                                        html += "<tr>";
                                        html += "<td class=\"question_1\">난이도</td>";
                                        html += "<td class=\"question_2\">" + secondItem.content.Replace("<br/>", string.Empty) + "</td>";
                                        html += "</tr>";
                                        html += "</tbody></table></br>";
                                    }
                                    else if (Convert.ToString(secondItem.rectype).Equals("DISTRACTOR"))
                                    {
                                        html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";
                                        html += "<tr>";
                                        html += "<td class=\"question_1\">선택지</td>";
                                        html += "<td class=\"question_2\">" + secondItem.content.Replace("<br/>", string.Empty) + "</td>";
                                        html += "</tr>";
                                        html += "</tbody></table></br>";
                                        
                                    }
                                    else if (Convert.ToString(secondItem.rectype).Equals("EXAMPLE"))
                                    {
                                        html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";
                                        html += "<tr>";
                                        html += "<td class=\"question_1\">보기</td>";
                                        html += "<td class=\"question_2\">" + secondItem.content.Replace("<br/>", string.Empty) + "</td>";
                                        html += "</tr>";
                                        html += "</tbody></table></br>";
                                    }
                                    else if (Convert.ToString(secondItem.rectype).Equals("ANSWER"))
                                    {
                                        html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";
                                        html += "<tr>";
                                        html += "<td class=\"question_1\">정답</td>";
                                        html += "<td class=\"question_2\">" + secondItem.content.Replace("<br/>", string.Empty) + "</td>";
                                        html += "</tr>";
                                        html += "</tbody></table></br>";

                                    }
                                    else if (Convert.ToString(secondItem.rectype).Equals("EXPLANATION"))
                                    {
                                        html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";
                                        html += "<tr>";
                                        html += "<td class=\"question_1\">해설</td>";
                                        html += "<td class=\"question_2\">" + secondItem.content.Replace("<br/>", string.Empty) + "</td>";
                                        html += "</tr>";
                                        html += "</tbody></table></br>";
                                    }
                                    else if (Convert.ToString(secondItem.rectype).Equals("SOUND") || Convert.ToString(secondItem.rectype).Equals("VIDEO"))
                                    {
                                        html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";

                                        List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);

                                        List<string> imageList = new List<string>();
                                        string strCaption = string.Empty;
                                        string strLink = string.Empty;
                                        string strDescription = string.Empty;

                                        foreach (Child child in childList)
                                        {
                                            if (!child.content.Equals("<p><br></p>"))
                                            {
                                                if (child.rectype.Equals(string.Empty)) //이미지 노드
                                                {
                                                    html += "<tr>";
                                                    html += "<td class=\"question_1\">파일명</td>";
                                                    html += "<td class=\"question_2\"><a target=\"_blank\" href=\"" + edu.FileUrl + child.content.Replace("<br/>", string.Empty) + "\">" + child.content.Replace("<br/>", string.Empty) + "</a></td>";
                                                    html += "</tr>";
                                                }
                                                else if (child.rectype.Equals("SENTENCE", StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    html += "<tr>";
                                                    html += "<td class=\"question_1\">문장</td>";
                                                    html += "<td class=\"question_2\">" + child.content + "</td>";

                                                    html += "</tr>";
                                                }
                                            }
                                        }

                                        html += "</tbody></table></br>";

                                    }
                                    else if (Convert.ToString(secondItem.rectype).Equals("PAIRSENTENCE"))
                                    {
                                        html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";

                                        List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);

                                        List<string> imageList = new List<string>();
                                        string strCaption = string.Empty;
                                        string strLink = string.Empty;
                                        string strDescription = string.Empty;

                                        foreach (Child child in childList)
                                        {
                                            if (!child.content.Equals("<p><br></p>"))
                                            {

                                                if (child.rectype.Equals(string.Empty)) //이미지 노드
                                                {
                                                    html += "<tr>";
                                                    html += "<td class=\"question_1\">파일명</td>";
                                                    html += "<td class=\"question_2\"><a target=\"_blank\" href=\"" + edu.FileUrl + child.content.Replace("<br/>", string.Empty) + "\">" + child.content.Replace("<br/>", string.Empty) + "</a></td>";
                                                    html += "</tr>";
                                                }
                                                else if (child.rectype.Equals("SENTENCE", StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    html += "<tr>";
                                                    html += "<td class=\"question_1\">문장</td>";
                                                    html += "<td class=\"question_2\">" + child.content + "</td>";

                                                    html += "</tr>";
                                                }
                                                else if (child.rectype.Equals("TRANSLATION", StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    html += "<tr>";
                                                    html += "<td class=\"question_1\">해석</td>";
                                                    html += "<td class=\"question_2\">" + child.content + "</td>";
                                                    html += "</tr>";
                                                }
                                                else if (child.rectype.Equals("GRAMMAR", StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    html += "<tr>";
                                                    html += "<td class=\"question_1\">문법</td>";
                                                    html += "<td class=\"question_2\">" + child.content + "</td>";
                                                    html += "</tr>";
                                                }
                                            }
                                        }

                                        html += "</tbody></table></br>";
                                    }
                                    else
                                    {
                                        html += secondItem.content;
                                    }
                                }
                            }
                        }

                }

                html += "<h3 class=\"chapterTitle\">태그</h3>";
                //태그
                Tag tag = edu.Tag;
                if (tag != null && !tag.content.Equals(string.Empty))
                {
                    html += "<table style=\"width:95%;\" cellspacing=\"0\" ><thead></thead><tbody>";
                    html += "<tr>";
                    html += "<td class=\"question_2\">" + tag.content.Replace("<br/>", string.Empty) + "</td>";
                    html += "</tr>";
                    html += "</tbody></table></br>";
                }

                divContent.InnerHtml = html;
            }
        }
    }

    public class Edu
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("lunit")]
        public List<LUnit> LUnit { get; set; }
        [JsonProperty("quiz")]
        public List<Quiz> Quiz { get; set; }
        [JsonProperty("tag")]
        public Tag Tag { get; set; }
        [JsonProperty("fileurl")]
        public string FileUrl { get; set; }
    }


    public class Tag
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }
    }

    public class LUnit
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("list")]
        public List<Child> list { get; set; }
    }

    public class Quiz
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("list")]
        public List<Child> list { get; set; }
    }



    public class Child
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }
    }

}