﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BinderEdit.aspx.cs" Inherits="main_TemplateAdd" validateRequest="false" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>       <html class="no-js"> <![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../js/jquery-2.1.3.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.jqtransform.js"></script>
    <script src="../js/ui.js"></script>
    <script src="../js/jquery.js"></script>
    <script type="text/javascript">
        function EntSum(){
            var TCnt = document.getElementById('selTEntryCnt').value; 
            var QCnt = document.getElementById('selQEntryCnt').value; 
            var CCnt = document.getElementById('selCEntryCnt').value; 
            document.getElementById('sumEntry').value = TCnt + QCnt + CCnt;
        }
    </script>
</head>

<body>

<header id="header"><!-- header -->
    <div class="container">
	    <h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

	    <div id="utility"><!-- utility -->
		    <asp:Label ID="lblLogIn" runat="server"></asp:Label>
		    <a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
	    </div><!-- // utility -->
    </div>
</header><!-- header -->
<div class="container">
  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2 active"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
                        <li><a href="DaumExportList.aspx">다음Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
                        <li><a href="OutlineList.aspx">개요부관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->
    <form id="addForm" runat="server">
    <div id="contents">
        <div class="title"><!-- title -->
			<h2 class="title">바인더 관리</h2>
		</div><!-- // title -->	

		<div class="section-button"><!-- section-button -->
			<div class="pull-right">
                <a class="btn btn-sm btn-primary" style="float:right;margin-left:5px" href="BinderList.aspx" >바인더목록</a> 
			</div>
		</div><!-- // section-button -->

		<div class="title"><!-- title -->
			<h3 class="title title-success">바인더정보</h3>
		</div><!-- // title -->
        <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: 110px;">
			    <col style="width: auto;">
			    <col style="width: 110px;">
			    <col style="width: auto;">
		    </colgroup>
		    <tbody>
			    <tr>
				    <th>바인더명</th>
				    <td>
				        <asp:Label ID="lblBinderTitle" runat="server"></asp:Label>                            
				    </td>
				    <th>바인더설명</th>
                    <td>
				        <asp:Label ID="lblBinderMemo" runat="server"></asp:Label>                            
                        <asp:HiddenField ID="txtBinderIdx" runat="server"></asp:HiddenField>                            
                    </td>
			    </tr>
		    </tbody>
		</table><!-- // table-a -->
        <br />       
		<div class="title"><!-- title -->
			<h3 class="title title-primary">
                상위바인더
                <asp:Button runat="server" ID="AddReationButton" class="btn btn-sm btn-primary pull-right" text="추가" CommandName="RelationType" CommandArgument="1" OnClick="AddReationButton_Click" />
			</h3>
		</div><!-- // title -->
        <table border="0" cellpadding="0" cellspacing="0" class="table">
            <asp:Repeater ID="rpRelationList" runat="server" OnItemCommand="rpRelationList_ItemCommand" >
                <HeaderTemplate>
<%
    if (rpRelationList.Items.Count == 0)
    {
%>
                    <tr>
                        <td style="text-align:center">조회된 바인더가 없습니다.</td>
                    </tr>                    
<%
    }
%>
                    <table class="table-bordered" >
                        <tr>
                    
                </HeaderTemplate>
                    <ItemTemplate>
                        <%# (Container.ItemIndex != 0 && Container.ItemIndex % 10 == 0) ? @"</tr><tr><td span=2 height=1px ></td></tr><tr>" : string.Empty  %>
                        <td class="btn-default" style="padding:5px 5px;margin:2px 2px;width:10%">
                            <asp:HiddenField ID="hfBinderRelationIdx" Value='<%#DataBinder.Eval(Container.DataItem , "Idx")%>'  runat="server" />
                            <a onclick="location.href='BinderEdit.aspx?idx=<%#DataBinder.Eval(Container.DataItem , "RelationBinderIdx")%>'"><asp:Label ID="lblBinderName" Text='<%#DataBinder.Eval(Container.DataItem , "BinderTitle")%>'  runat="server" /></a>
                            <asp:Button runat="server" cssClass="btn btn-sm btn-close" CausesValidation="false" />
                        </td>
                    </ItemTemplate>
                <FooterTemplate></tr></table></FooterTemplate>
            </asp:Repeater>
		</table><!-- // table-a -->
        <br /><br />
		<div class="title"><!-- title -->
			<h3 class="title title-primary">
                연관바인더
                <asp:Button runat="server" ID="AddReationButton2" class="btn btn-sm btn-primary pull-right" text="추가" CommandName="RelationType" CommandArgument="2" OnClick="AddReationButton_Click" />
			</h3>
		</div><!-- // title -->
        <table border="0" cellpadding="0" cellspacing="0" class="table">
            <asp:Repeater ID="rpRelationList2" runat="server" OnItemCommand="rpRelationList2_ItemCommand" >
                <HeaderTemplate>
<%
    if (rpRelationList2.Items.Count == 0)
    {
%>
                    <tr>
                        <td style="text-align:center">조회된 바인더가 없습니다.</td>
                    </tr>                    
<%
    }
%>
                    <table class="table-bordered" >
                        <tr>
                    
                </HeaderTemplate>
                    <ItemTemplate>
                        <%# (Container.ItemIndex != 0 && Container.ItemIndex % 10 == 0) ? @"</tr><tr><td span=2 height=1px ></td></tr><tr>" : string.Empty  %>
                        <td class="btn-default" style="width:10%;padding:5px 10px;margin:2px 2px;">
                            <asp:HiddenField ID="hfBinderRelationIdx" Value='<%#DataBinder.Eval(Container.DataItem , "Idx")%>'  runat="server" />
                            <a onclick="location.href='BinderEdit.aspx?idx=<%#DataBinder.Eval(Container.DataItem , "RelationBinderIdx")%>'"><asp:Label ID="lblBinderName" Text='<%#DataBinder.Eval(Container.DataItem , "BinderTitle")%>'  runat="server" /></a>
                            <asp:Button runat="server" cssClass="btn btn-sm btn-close" CausesValidation="false" />
                        </td>
                    </ItemTemplate>
                <FooterTemplate></tr></table></FooterTemplate>
            </asp:Repeater>
		</table><!-- // table-a -->
	</div><!-- // contents -->
    <br /><br />
		<div class="title"><!-- title -->
			<h3 class="title title-success">
                엔트리
                <asp:Button runat="server" ID="AddEntry" class="btn btn-sm btn-success pull-right" text="엔트리추가" OnClick="AddEntry_Click" />
			</h3>
		</div><!-- // title -->
        <table border="0" cellpadding="0" cellspacing="0" class="table">
            <asp:Repeater ID="EntryList" runat="server" OnItemCommand="EntryList_ItemCommand" >
                <HeaderTemplate>
                    <colgroup>
                        <col style="width: 10%;">
                        <col style="width: 50%;">
			            <col style="width: 35%;">
			            <col style="width: 5%;">
		            </colgroup>
                    <tr>
                        <th>Entry No</th>
				        <th>Category</th>
				        <th>타이틀</th>
				        <th>삭제</th>
                    </tr>
<%
    if (EntryList.Items.Count == 0)
    {
%>
                    <tr>
                        <td colspan="4" style="text-align:center">조회된 엔트리가 없습니다.</td>
                    </tr>                    
<%
    }
%>
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hfBinderEntryIdx" Value='<%#DataBinder.Eval(Container.DataItem , "Idx")%>' runat="server"/>
                            <asp:Label ID="lblEntryIdx" Text='<%#DataBinder.Eval(Container.DataItem , "EntryIdx")%>' runat="server"/>
                        </td>
                        <td>
                            <asp:Label ID="lblCategory" Text='<%#DataBinder.Eval(Container.DataItem , "Category")%>' runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblEntryTitle" Text='<%#DataBinder.Eval(Container.DataItem , "EntryTitle")%>' runat="server" />
                        </td>
                        <td>
                            <asp:Button runat="server" cssClass="btn-code-del" BorderWidth="0" OnClientClick="javascript:return confirm('삭제하시겠습니까?');" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
		</table><!-- // table-a -->
	</div><!-- // contents -->
        
	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>

    </form>

</div>
</body>
</html>
