﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CodeView.aspx.cs" Inherits="main_List" %>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/bootstrap.css" rel="stylesheet" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="../js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript">
	    showPopup = function () {
	        $("#popLayer").show();
	        $("#popLayer").center();
	    }
	    hidePopup = function () {
	        $("#popLayer").hide();
	    }
	    jQuery.fn.center = function () {
	        this.css("position", "absolute");
	        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
	        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
	        return this;
	    }

	    var img_L = 0;
	    var img_T = 0;
	    var targetObj;

	    function getLeft(o) {
	        return parseInt(o.style.left.replace('px', ''));
	    }
	    function getTop(o) {
	        return parseInt(o.style.top.replace('px', ''));
	    }

	    // Div 움직이기
	    function moveDrag(e) {
	        var e_obj = window.event ? window.event : e;
	        var dmvx = parseInt(e_obj.clientX + img_L);
	        var dmvy = parseInt(e_obj.clientY + img_T);
	        targetObj.style.left = dmvx + "px";
	        targetObj.style.top = dmvy + "px";
	        return false;
	    }

	    // 드래그 시작
	    function startDrag(e, obj) {
	        targetObj = obj;
	        var e_obj = window.event ? window.event : e;
	        img_L = getLeft(obj) - e_obj.clientX;
	        img_T = getTop(obj) - e_obj.clientY;

	        document.onmousemove = moveDrag;
	        document.onmouseup = stopDrag;
	        if (e_obj.preventDefault) e_obj.preventDefault();
	    }

	    // 드래그 멈추기
	    function stopDrag() {
	        document.onmousemove = null;
	        document.onmouseup = null;
	    }
	</script>
    <style type="text/css">
        .popLayer {display:none; position:absolute; width:520px; z-index:10; padding:30px 30px 35px; margin-left:-235px; background-color:#fff; border:1px solid #000;}
    </style>
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>

<div class="container">


    <form id="editForm" runat="server">
	
    <div id="contents"><!-- contents -->

		<div class="col-md-8 code-manage"><!-- col -->
			<div class="title"><!-- title -->
				<h3 class="title">상세코드</h3>
				<div class="action">
					<button type="button" class="btn btn-sm btn-info" onclick="javascript:showPopup()">추가</button>
				</div>
			</div><!-- // title -->

			<div class="code-list row"><!-- list -->
                <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
                    <asp:Repeater ID="CodeTableRepeater" runat="server" OnItemCommand="CodeTableRepeater_ItemCommand">
                        <HeaderTemplate>
                            <tr>
                                <th></th>
                                <th>코드</th>
                                <th>코드명</th>
                                <th></th>
                            </tr>
                        <% if (CodeTableRepeater.Items.Count == 0) { %>
                            <tr>
                                <td colspan="4" style="text-align:center">등록된 코드가 없습니다.</td>
                            </tr>                    
                        <%  }  %>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td></td>
                                <td><asp:Label ID="CodeID" runat="server" Text=<%#DataBinder.Eval(Container.DataItem , "Code")%> /></td>
                                <td><asp:Label ID="CodeName" Text=<%#DataBinder.Eval(Container.DataItem , "CodeName")%> runat="server"/></td>
                                <td><asp:Button runat="server" cssClass="btn-code-del" BorderWidth="0" OnClientClick="javascript:return confirm('삭제하시겠습니까?')"/></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>

                <!--div popLayer start -->
                <div id="popLayer" class="popLayer" draggable="true" style="position:absolute; top:50px; right:50px; cursor:pointer; cursor:hand" border="0">
                    <div class="title" onmousedown="startDrag(event, popLayer);"><!-- title -->
				        <h3 class="title">코드추가</h3>
				        <div class="action">
					        <button type="button" class="btn btn-sm btn-close" onclick="javascript:hidePopup()"></button>
				        </div>
			        </div><!-- // title -->
                    <div class="table-responsive">
		                <table class="table">
			                <tbody>
				                <tr>
				                    <td>코드타입</td>
				                    <td>
				                        <div class="input-group" >
                                            <asp:DropDownList class="form-control" ID="ddlCodeType" runat="server" aria-describedby="basic-addon2">
                						        </asp:DropDownList>
					                    </div>
				                    </td>
                                </tr>
				                <tr>
				                    <td>코드</td>
				                    <td>
				                        <div class="input-group" onmousedown="txtCode.focus();">
    					                    <asp:TextBox Runat="server" Width="100" ID="txtCode" class="form-control" aria-describedby="basic-addon2"></asp:TextBox>
                                            <br /><label id="msgIDCheck" runat="server"></label>
					                    </div>
				                    </td>
                                </tr>
				                <tr>
				                    <td>코드명</td>
				                    <td>
				                        <div class="input-group" onmousedown="txtCodeName.focus();">
    					                    <asp:TextBox Runat="server" Width="100" ID="txtCodeName" class="form-control" aria-describedby="basic-addon2" ></asp:TextBox>
					                    </div>
				                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <asp:Button ID="AddButton" class="btn btn-success" style="float: left; padding: 10px 90px;" runat="server" text="추가" onclick="AddButton_Click"></asp:Button> 
                    </div>
                </div>
                <!--- div popLayer end  -->


			</div><!-- // list -->
            



		</div><!-- col -->

	</div><!-- // contents -->

    </form>

</div><!-- // container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>


 
