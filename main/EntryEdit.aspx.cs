﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Text;
using System.Net;

public partial class main_EntryEdit2 : System.Web.UI.Page
{
    
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;
    private string strTaskIdx = string.Empty;
    private string pageNo = "1";
    private string pageSize = "100";
    private string strCateIdx = string.Empty;

    public DataTable entryTable  = null;
    public DataTable tagTable = null;
    public string entryTag = string.Empty;
    public string idx = string.Empty;
    public string usrAuth = string.Empty;
    public int entryStatus = 1;
    private static string imgURL = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }

        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
            usrAuth = uAuth.ToString();
        }
        //##### 권한 처리 끝  

        if(!this.IsPostBack){

            // 1. EntryIDX 받기
            if (Request.Params["idx"] != null)
            {
                idx = Request.Params["idx"].ToString();
                if (idx.Length == 0)
                    Response.Redirect("EntryList.aspx");
            }
            else
            {
                Response.Redirect("EntryList.aspx");
            }
            if (Request.Params["pageNo"] != null) 
                pageNo = Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null) 
                pageSize = Request.Params["pageSize"].ToString();
            
            //if (Request.Params["Idx"]==null)
            //{
            //    idx = "23579"; //디버그용 코드 추후 삭제
            //}
            //else
            //{
            //    idx = Request.Params["Idx"].ToString();
            //}

            txtEntryNo.Text = idx;
        }else{
            idx = txtEntryNo.Text;
            
        }
        
        //// 엔트리 저장 //##필요없는 Con.Open이면 삭제 일단주석처리
        //DBFileInfo();
        //SqlConnection Con = new SqlConnection(connectionString);
        //Con.Open();


        //엔트리 삭제
        string deleteIDStrings = deleteIDString.Value;
        if (deleteIDStrings.Length != 0)
        {
            removeEntry(deleteIDStrings);
        }

        //엔트리 저장
        string jsonSaveString = saveString.Value;
        if (jsonSaveString.Length != 0)
        {

            //엔트리의 이미지를 삭제한다.
            this.deleteEntryFile();

            if (saveMode.Value.Equals("SAVE"))
            {
                this.saveEntry(jsonSaveString);
            }
            else if(saveMode.Value.Equals("SAVEAS") ) {

                this.saveAsEntry(jsonSaveString);
            }
        }

        //엔트리 정보 불러오기
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Con.Open();
        Cmd.Parameters.Add("@Idx", SqlDbType.Int);
        Cmd.Parameters["@Idx"].Value = idx;
        Cmd.CommandText = "USP_Entry_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);

        if (reader.Read())
        {
            txtTitle.Text = Server.HtmlDecode(reader["EntryTitle"].ToString());
            txtTitle_K.Text = Server.HtmlDecode(reader["EntryTitleK"].ToString());
            txtTitle_E.Text = Server.HtmlDecode(reader["EntryTitleE"].ToString());
            txtTitle_C.Text = Server.HtmlDecode(reader["EntryTitleC"].ToString());
            txtSummary.Text = Server.HtmlDecode(reader["Summary"].ToString());
            txtTitle_Sub1.Text = Server.HtmlDecode(reader["EntryTitleSub1"].ToString());
            txtTitle_Sub2.Text = Server.HtmlDecode(reader["EntryTitleSub2"].ToString());
            txtSynonym.Text = Server.HtmlDecode(reader["Synonym"].ToString());
            txtUser.Text = reader["UserName"].ToString();
            txtCategory.Text = reader["Category"].ToString();
            txtEditDate.Text = reader["EditDate"].ToString();
            txtEditor.Text = reader["EditorName"].ToString();
            entryStatus = Convert.ToInt16(reader["Status"].ToString());
            entryTag = reader["Tag"].ToString();
            txtTag.Text = entryTag;

            HttpCookie myCookie = new HttpCookie("UserSettings");
            myCookie["EntryIdx"] = idx;
            string taskIdx = reader["TaskIdx"].ToString();
            txtTaskIdx.Value = taskIdx;
            strTaskIdx = taskIdx;
            myCookie["TaskIdx"] = taskIdx;
            myCookie.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(myCookie);

            string rootUrl = "http://" + Request.ServerVariables["HTTP_HOST"];
            imgURL = rootUrl + "/CMS100Data/EntryData/" + taskIdx + "/";
            fileUrl.Value = imgURL;

            GetOutline();
        }
        reader.Close();


        //엔트리 정보 불러오기(RecType)
        SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM TEntryData WHERE EntryIdx = " + idx + " AND DelFlag=0", Con);
        this.entryTable = new DataTable();
        sda.Fill(entryTable);

        Con.Close();
    }
                
    private void GetOutline()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        string strQuery = "SELECT OutlineIdx as Idx, OutlineName, OutlineData, OutlineType FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + txtEntryNo.Text + " ORDER BY OutlineIdx ASC";
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Con.Open();

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");
        if (ds.Tables["data_list"].Rows.Count > 0)
        {
            StringBuilder strOutline = new StringBuilder();
            strOutline.Append("<table class=\"table\">");
            try
            {

                foreach (DataRow dr in ds.Tables["data_list"].Rows)
                {

                    strOutline.Append("<tr><th>" + dr["OutlineName"].ToString() + "</th><td>" + dr["OutlineData"].ToString() + "</td></tr>");

                }
            }
            catch (Exception ex)
            {
                Response.Write("개요부 저장 중 오류가 발생했습니다.<br />오류내용:" + ex.ToString());
            }


            strOutline.Append("</table>");
            lblOutline.Text = strOutline.ToString();
        }

        //Close the connection.
        Con.Close();
    }


    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    public string getRecTypeName( string recType ) {
        string result = String.Empty;
        switch (recType) { 

            case "INDEXCONTENT":
                result = "대분류";
                break;
            case "MAINTITLE":
                result = "대제목";
                break;
            case "SUBTITLE":
                result = "중제목";
                break;
            case "BOX":
                result="블럭";
                break;
            case "BASIC":
                result = "기본";
                break;
            case "BASIC2":
                result = "기본2";
                break;
            case "RELATEDSEARCH":
                result = "연관검색어";
                break;
            case "MATRIX_TABLE":
                result="표";
                break;
            case "ANNOTATION":
                result = "주석";
                break;
            case "IMAGE":
                result="이미지";
                break;
            case "TAG":
                result = "태그";
                break;
            case "QUIZ":
                result = "문제";
                break;
            case "INFOTABLE":
                result = "정보";
                break;
            case "QUESTION":
                result = "문제";
                break;
            case "EXAMPLE":
                result = "보기";
                break;
            case "DISTRACTOR":
                result = "선택지";
                break;
            case "EXPLANATION":
                result = "설명";
                break;
            case "ANSWER":
                result = "정답";
                break;
            case "LEVEL":
                result = "난이도";
                break;
            case "CHDIVIDE":
                result = "한자분해";
                break;
            case "SOUND":
                result = "사운드";
                break;
            case "PAIRSENTENCE":
                result = "Pair Sentence";
                break;
            case "VIDEO":
                result = "동영상";
                break;
        }
        
        return result;

    }


    public static string convertXmlToHtml(string strXml, string recType)
    {

        //align이 있는지 확인한다.
        bool isAlign = false;
        string strAlign = String.Empty;
        int startAlignIndex = strXml.IndexOf("<ALIGN>");
        string alignEndTag = "</ALIGN>";
        int endAlignIndex = strXml.LastIndexOf(alignEndTag);
        if (startAlignIndex > -1)
        {
            isAlign = true;
            string alignTag = strXml.Substring(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
            strAlign = alignTag.Replace("<ALIGN>", "").Replace("</ALIGN>", "");
            strXml = strXml.Remove(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
        }
        

        if (recType.Equals("IMAGE"))
        {

            if (strXml.Contains("<IMGS>")) {
                if (!strXml.Contains("</IMGS>")) {
                    strXml += "</IMGS>";
                }
            }
            
            return parseXmlToHtml(strXml, isAlign, strAlign);
        }
        else if (recType.Equals("MATRIX_TABLE"))
        {
            return  parseTableXmlToHtml(strXml, isAlign, strAlign);
        }
        else if (recType.Equals("SOUND") || recType.Equals("PAIRSENTENCE") || recType.Equals("VIDEO")) 
        {
            strXml = strXml.Replace("<FILEANME>", "");
            strXml = strXml.Replace("</FILEANME>", "");
            return strXml;
        }
        else if (recType.Equals("LEVEL")) {
            return strXml;
        }
        else
        {

            strXml = strXml.Replace("<img src=\"", "<img src=\"" + imgURL);
            strXml = strXml.Replace("<br>", "<br/>");

            string result = string.Empty;
            strXml = "<edu>" + strXml + "</edu>";
            using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.CDATA:
                            if (isAlign)
                            {
                                StringWriter stringWriter = new StringWriter();
                                HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                                htmlWriter.Write(reader.Value);
                                htmlWriter.RenderEndTag();
                                result += stringWriter.ToString();
                            }
                            else
                            {
                                result += reader.Value;
                            }
                            break;
                        case XmlNodeType.Text:
                            result += reader.Value;
                            break;

                    }
                }
            }

            return result;

        }


    }

    private static string pasteImgURL(string strSrc) {

        ////이미지 태그가 있다면 src를 전체 경로로 만들어준다.
        XmlDocument xml = new XmlDocument();
        xml.LoadXml("<edu>" + strSrc + "</edu>");
        XmlNodeList imgNodeList = xml.GetElementsByTagName("img");
        if (imgNodeList.Count > 0)
        {
            foreach (XmlNode imgNode in imgNodeList)
            {
                XmlAttributeCollection attributes = imgNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Name.Contains("src"))
                    {
                        string imgSrc = imgURL + attribute.Value;
                        attribute.Value = imgSrc;
                        break;
                    }
                }
            }

            //문자열로 출력시킨다.
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
            {
                xml.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                strSrc = stringWriter.GetStringBuilder().ToString();
            }

            strSrc = strSrc.Replace("<edu>", string.Empty);
            strSrc = strSrc.Replace("</edu>", string.Empty);

        }

        return strSrc;
    }

    private static string parseXmlToHtml(string strXml, bool isAlign, string strAlign)
    {
        //루트태그를 붙여줌
        strXml = "<edu>" + strXml + "</edu>";

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

        using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
        {

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.CDATA)
                {
                    htmlWriter.Write(reader.Value);

                }
                else if (reader.NodeType == XmlNodeType.Element)
                {

                    switch (reader.Name)
                    {
                        case "TEXT":

                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "IMGS":
                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "IMG":
                            break;
                        case "WIDTH":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Width, reader.Value);
                            break;
                        case "HEIGHT":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Height, reader.Value);
                            break;
                        case "FILENAME":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Src, imgURL + reader.Value);

                            break;
                    }

                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    switch (reader.Name)
                    {
                        case "TEXT":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMGS":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMG":
                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Img);
                            htmlWriter.RenderEndTag();
                            break;
                        case "WIDTH":
                            break;
                        case "HEIGHT":
                            break;
                    }
                }
            }
        }

        return stringWriter.ToString();
    }

    private static string parseTableXmlToHtml(string strXml, bool isAlign, string strAlign)
    {
        //루트태그를 붙여줌
        strXml = "<edu>" + strXml + "</edu>";

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

        using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
        {

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.CDATA)
                {

                    string valueString = reader.Value;
                    valueString = valueString.Replace("src=\"", "src=\"" + imgURL);

                    htmlWriter.Write(valueString);
                }
                else if (reader.NodeType == XmlNodeType.Element)
                {

                    switch (reader.Name)
                    {
                        case "TEXT":

                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "WIDTH":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Width, reader.Value);
                            break;
                        case "HEIGHT":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Height, reader.Value);
                            break;
                        case "FILENAME":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Src, imgURL + reader.Value);
                            break;
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    switch (reader.Name)
                    {
                        case "TEXT":
                            htmlWriter.RenderEndTag();
                            break;
                        case "WIDTH":
                            break;
                        case "HEIGHT":
                            break;
                    }
                }
            }
        }

        return stringWriter.ToString();
    }


    private string convertHtmlToXml(string strHtml, string recType)
    {
        //공백을 없앤다.
        strHtml = strHtml.Replace("&nbsp;", " ");

        //hr태그에 닫는 태그를 넣는다.
        strHtml = strHtml.Replace("<hr>", "<hr/>");

        //img 태그와 br 태그의 닫는 태그를 넣는다.
        strHtml = strHtml.Replace("<br>", "<br/>");
        int imgStartIndex = 0;
        int imgEndIndex = 0;

        imgStartIndex = strHtml.IndexOf("<img", 0);
        while (imgStartIndex > -1)
        {

            imgEndIndex = strHtml.IndexOf(">", imgStartIndex);
            strHtml = strHtml.Insert(imgEndIndex, "/");
            imgStartIndex = strHtml.IndexOf("<img", imgEndIndex);

        }

        //이미지의 이상한 문자를 지운다.
        StringBuilder sb = new StringBuilder(strHtml);
        sb.Replace(char.ConvertFromUtf32(8203), string.Empty);
        strHtml = sb.ToString();

        //div 태그가 있는지 확인한다.
        //div 태그가 있다면 정렬을 가져온다.
        string strAlign = string.Empty;
        XmlDocument divXml = new XmlDocument();
        divXml.LoadXml("<edu>" + strHtml + "</edu>");

        XmlNodeList divNodeList = divXml.GetElementsByTagName("div");
        if (divNodeList.Count > 0)
        {
            foreach (XmlNode divNode in divNodeList)
            {
                XmlAttributeCollection attributes = divNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Value.Contains("text-align"))
                    {
                        strAlign = "<ALIGN>" + attribute.Value.Replace("text-align:", "").Replace(";", string.Empty).Trim() + "</ALIGN>";
                    }
                }
            }


            int divStartIndex = strHtml.IndexOf("<div", 0);
            while (divStartIndex > -1)
            {
                int divEndIndex = strHtml.IndexOf(">", divStartIndex) + 1;
                strHtml = strHtml.Remove(divStartIndex, divEndIndex - divStartIndex);
                divStartIndex = strHtml.IndexOf("<div", divStartIndex);
            }
            //div 태그를 지운다.
            strHtml = strHtml.Replace("</div>", string.Empty);
        }

        
        //링크 태그의 쓰레기 속성을 지운다.
        XmlDocument xml = new XmlDocument();
        xml.PreserveWhitespace = true;
        xml.LoadXml("<edu>" + strHtml + "</edu>");
        XmlNodeList aNodeList = xml.GetElementsByTagName("a");
        if (aNodeList.Count > 0)
        {
            foreach (XmlNode aNode in aNodeList)
            {
                XmlAttributeCollection attributes = aNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Name.Contains("data-cke-saved-href"))
                    {
                        attributes.Remove(attribute);
                        break;
                    }
                }
            }
        }


        //이미지 태그의 쓰레기 속성을 지우고 이미지를 저장한다.
        if (!recType.Equals("IMAGE")) 
        { 

            XmlNodeList imgNodeList = xml.GetElementsByTagName("img");
            
            if (imgNodeList.Count > 0)
            {

                foreach (XmlNode imgNode in imgNodeList)
                {
                    XmlAttributeCollection attributes = imgNode.Attributes;
                    foreach (XmlAttribute attribute in attributes)
                    {
                        if (attribute.Name.Contains("data-cke-saved-src"))
                        {
                            attributes.Remove(attribute);
                            break;
                        }
                    }

                    attributes = imgNode.Attributes;
                    foreach (XmlAttribute attribute in attributes)
                    {
                        if (attribute.Name.Contains("alt"))
                        {
                            attributes.Remove(attribute);
                            break;
                        }
                    }

                    attributes = imgNode.Attributes;
                    if (attributes.GetNamedItem("src").Value != null) 
                    {

                        string dataType = attributes.GetNamedItem("data-type").Value;
                        string imgSrc = attributes.GetNamedItem("src").Value;
                        if (!imgSrc.Equals(string.Empty))
                        {
                            string[] splitedFileName = imgSrc.Split(new char[] { '/' });
                            string fileName = splitedFileName[splitedFileName.Length - 1];
                            string newFileName = saveImageFileWithDataType(fileName, dataType);
                            attributes.GetNamedItem("src").Value = newFileName;
                            if (newFileName != null && !newFileName.Equals(string.Empty))
                                this.saveEntryFile( newFileName, "IMG" );
                        }
                    }

                    attributes = imgNode.Attributes;
                    if (attributes.GetNamedItem("style") == null) {
                        XmlAttribute styleAttribute = xml.CreateAttribute("style");
                        styleAttribute.Value = "";
                        attributes.SetNamedItem(styleAttribute);
                    }
                }
            }
        }

        //문자열로 출력시킨다.
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.OmitXmlDeclaration = true;
        using (var stringWriter = new StringWriter())
        using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
        {
            xml.WriteTo(xmlTextWriter);
            xmlTextWriter.Flush();
            strHtml = stringWriter.GetStringBuilder().ToString();
        }

        strHtml = strHtml.Replace("<edu>", string.Empty);
        strHtml = strHtml.Replace("</edu>", string.Empty);

        //태그를 파싱한다.
        string strConvert = string.Empty;
        if (recType.Equals("IMAGE"))
        {
            if (strAlign.Equals(string.Empty)) 
            {
                strAlign = "<ALIGN></ALIGN>";
            }
            
            strConvert = this.parseTagContent(strHtml) + strAlign;

        }
        else if (recType.Equals("MATRIX_TABLE"))
        {
            if (strAlign.Equals(string.Empty))
            {
                strAlign = "<ALIGN>left</ALIGN>";
            }
            strConvert = this.parseTagContent(strHtml) + strAlign;
        }
        else if (recType.Equals("SOUND") || recType.Equals("PAIRSENTENCE") || recType.Equals("VIDEO"))
        {
            strHtml = strHtml.Replace("<br />", string.Empty);
            strConvert = "<FILENAME>" + strHtml + "</FILENAME>";

            string fileName = strHtml;
            if (fileName != null && !fileName.Equals(string.Empty))
            {
                string type = string.Empty;
                //사운드와 비디오를 저장한다.
                if (recType.Equals("VIDEO"))
                {
                    saveEntryFile(strHtml, "MOV");
                }
                else
                {
                    saveEntryFile(strHtml, "SOU");
                }
            }
        }
        else if(recType.Equals("LEVEL")){
            strConvert = strHtml;
        }
        else
        {
            StringWriter strWriter = new StringWriter();
            XmlTextWriter xmlWriter = new XmlTextWriter(strWriter);

            xmlWriter.WriteStartElement("TEXT");
            xmlWriter.WriteCData(strHtml);
            xmlWriter.WriteEndElement();

            if (recType.Equals("BASIC") || recType.Equals("BASIC2") || recType.Equals("BOX"))
            {
                if (strAlign.Equals(string.Empty))
                {
                    strAlign = "<ALIGN>left</ALIGN>";
                }
            }

            strConvert = strWriter.ToString() + strAlign;
        }

        return strConvert;
    }

    private string parseTagContent(string strPTagContent)
    {

        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);

        strPTagContent = "<edu>" + strPTagContent + "</edu>";

        string strText = string.Empty;
        Boolean startedImageTage = false;

        using (XmlReader reader = XmlReader.Create(new StringReader(strPTagContent)))
        {
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Text:
                        if (startedImageTage)
                        {
                            xmlWriter.WriteEndElement();
                            startedImageTage = false;
                        }

                        strText += reader.Value;

                        break;
                    case XmlNodeType.Element:

                        switch (reader.Name)
                        {
                            case "table":
                                if (startedImageTage)
                                {
                                    xmlWriter.WriteEndElement();
                                    startedImageTage = false;
                                }

                                if (strText != string.Empty)
                                {
                                    xmlWriter.WriteStartElement("TEXT");
                                    xmlWriter.WriteCData(strText);
                                    xmlWriter.WriteEndElement();
                                    strText = string.Empty;
                                }


                                string tableStyle = reader.GetAttribute("style");
                                string tableHeight = string.Empty;
                                string tableWidth = string.Empty;
                                if (tableStyle != null)
                                {
                                    string[] splitedImgStyle = tableStyle.Split(new char[] { ';' });

                                    foreach (string attribute in splitedImgStyle)
                                    {
                                        if (attribute.Contains("width"))
                                        {
                                            tableWidth = attribute.Replace("width:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                        else if (attribute.Contains("height"))
                                        {
                                            tableHeight = attribute.Replace("height:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                    }
                                }

                                xmlWriter.WriteStartElement("CONTENTS");
                                xmlWriter.WriteCData(reader.ReadOuterXml());
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("HEIGHT");
                                xmlWriter.WriteValue(tableHeight);
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("WIDTH");
                                xmlWriter.WriteValue(tableWidth);
                                xmlWriter.WriteEndElement();
                                break;
                            case "img":
                                if (strText != string.Empty)
                                {
                                    xmlWriter.WriteStartElement("TEXT");
                                    xmlWriter.WriteCData(strText);
                                    xmlWriter.WriteEndElement();
                                    strText = string.Empty;
                                }

                                if (!startedImageTage)
                                {
                                    xmlWriter.WriteStartElement("IMGS");
                                    startedImageTage = true;
                                }

                                xmlWriter.WriteStartElement("IMG");

                                string imgHeight = string.Empty;
                                string imgWidth = string.Empty;
                                string imgStyle = reader.GetAttribute("style");
                                string fileSrc = reader.GetAttribute("src");
                                

                                if (imgStyle != null)
                                {
                                    string[] splitedImgStyle = imgStyle.Split(new char[] { ';' });

                                    foreach (string attribute in splitedImgStyle)
                                    {
                                        if (attribute.Contains("width"))
                                        {
                                            imgWidth = attribute.Replace("width:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                        else if (attribute.Contains("height"))
                                        {
                                            imgHeight = attribute.Replace("height:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                    }
                                }

                                xmlWriter.WriteStartElement("FILENAME");
                                string[] splitedFileName = fileSrc.Split(new char[] { '/' });
                                string fileName = splitedFileName[splitedFileName.Length - 1];

                                if (fileName != null && !fileName.Equals(string.Empty))
                                {
                                    this.saveEntryFile(fileName, "IMG");
                                }
                                xmlWriter.WriteValue(fileName);
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteRaw("<WIDTH>" + imgWidth + "</WIDTH>");

                                xmlWriter.WriteRaw("<HEIGHT>" + imgHeight + "</HEIGHT>");
                                xmlWriter.WriteEndElement();
                                break;
                        }

                        break;
                    case XmlNodeType.EndElement:

                        switch (reader.Name)
                        {
                            case "table":
                                break;
                            case "img":
                                xmlWriter.WriteEndElement();
                                break;
                        }
                        break;
                }
            }
        }

        StringBuilder sb = new StringBuilder(strText);
        sb.Replace(char.ConvertFromUtf32(8203), string.Empty);
        strText = sb.ToString();

        if (strText != string.Empty)
        {
            xmlWriter.WriteStartElement("TEXT");
            xmlWriter.WriteCData(strText);
            xmlWriter.WriteEndElement();
        }
        if (startedImageTage) xmlWriter.WriteEndElement();

        return stringWriter.ToString();
    }

    [System.Web.Services.WebMethod]
    public static string convertTexImage(string tex)
    {


        string entryIdx = null;
        string taskIdx = null;
        if (HttpContext.Current.Request.Cookies["UserSettings"] != null)
        {
            if (HttpContext.Current.Request.Cookies["UserSettings"]["EntryIdx"] != null)
            {
                entryIdx = HttpContext.Current.Request.Cookies["UserSettings"]["EntryIdx"];
            }

            if (HttpContext.Current.Request.Cookies["UserSettings"]["TaskIdx"] != null)
            {
                taskIdx = HttpContext.Current.Request.Cookies["UserSettings"]["TaskIdx"];
            }
        }

        

        string encodeUrl = HttpUtility.UrlEncode(tex);
        string fileName = taskIdx + "_" + entryIdx + "_" + DateTime.Now.ToString("HHmmss") + ".png";
        string imgDirectory = HostingEnvironment.MapPath("~/") + "\\CMS100Data\\EntryData\\" + taskIdx + "\\";
        if (!Directory.Exists(imgDirectory))
        {
            Directory.CreateDirectory(imgDirectory);
        }

        string filePath = HostingEnvironment.MapPath("~/") + "\\CMS100Data\\EntryData\\" + taskIdx + "\\" + fileName;

        using (WebClient wc = new WebClient())
        {
            wc.DownloadFile("http://hyonga.iptime.org:10800/tex/getTexImage.htm?texString=" + tex, filePath);
        }

        System.Drawing.Size size = System.Drawing.Image.FromFile(filePath).Size;
        String strWidth = Convert.ToString(size.Width);
        String strHeight = Convert.ToString(size.Height);

        //새로운 이미지코드명 생성
        string strImageCode = fileName;
        string strFileName = fileName;

        string connectionString = string.Empty;

        string serverIP = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }

        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmdn = new SqlCommand();
        Cmdn.Parameters.Add("@FileName", SqlDbType.VarChar, 255);
        Cmdn.Parameters.Add("@TaskIdx", SqlDbType.Int);
        Cmdn.Parameters.Add("@EntryIdx", SqlDbType.Int);
        Cmdn.Parameters.Add("@OriginName", SqlDbType.VarChar);
        Cmdn.Parameters.Add("@Description", SqlDbType.VarChar);
        Cmdn.Parameters.Add("@Caption", SqlDbType.VarChar, 255);
        Cmdn.Parameters.Add("@Width", SqlDbType.VarChar, 10);
        Cmdn.Parameters.Add("@Height", SqlDbType.VarChar, 10);
        Cmdn.Parameters.Add("@FileType", SqlDbType.VarChar, 10);

        Cmdn.Parameters["@FileName"].Value = strImageCode;  //신규 이미지 명
        Cmdn.Parameters["@TaskIdx"].Value = taskIdx;
        Cmdn.Parameters["@EntryIdx"].Value = entryIdx;
        Cmdn.Parameters["@OriginName"].Value = strFileName; //원 이미지파일 명
        Cmdn.Parameters["@Description"].Value = "";
        Cmdn.Parameters["@Caption"].Value = "";
        Cmdn.Parameters["@Width"].Value = strWidth;
        Cmdn.Parameters["@Height"].Value = strHeight;
        Cmdn.Parameters["@FileType"].Value = "IMG";

        Cmdn.CommandText = "USP_File_INSERT";
        Cmdn.CommandType = CommandType.StoredProcedure;
        Cmdn.Connection = Conn;
        Conn.Open();
        Cmdn.ExecuteNonQuery();
        Conn.Close();


        string rootUrl = "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
        string url = rootUrl + "/CMS100Data/EntryData/" + taskIdx + "/" + fileName;

        return url;
    }

    


    private void saveCategory(string entryIdx)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        //Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        //Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@MUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@EntryIdx", SqlDbType.Int);
        string[] arrCateIdx = hfCategoryIdx.Value.Split(new Char[] { '>' });

        Cmd.Parameters["@RevisionIdx"].Value = arrCateIdx[0];
        //Cmd.Parameters["@SubjectGroupIdx"].Value = ddlSubjectGroup.SelectedValue;
        Cmd.Parameters["@SubjectIdx"].Value = arrCateIdx[1];
        //Cmd.Parameters["@SchoolIdx"].Value = ddlSchool.SelectedValue;
        Cmd.Parameters["@BrandIdx"].Value = arrCateIdx[2];
        Cmd.Parameters["@GradeIdx"].Value = arrCateIdx[3];
        Cmd.Parameters["@SemesterIdx"].Value = arrCateIdx[4];
        Cmd.Parameters["@LUnitIdx"].Value = arrCateIdx[5];
        Cmd.Parameters["@MUnitIdx"].Value = arrCateIdx[6];
        Cmd.Parameters["@EntryIdx"].Value = entryIdx;
        Cmd.CommandText = "USP_Category_UPDATE";
        Cmd.CommandType = CommandType.StoredProcedure;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

    }
    private void saveEntry( string jsonSaveString ) {

        if (hfCategoryIdx.Value.Length > 0)
        {
            this.saveCategory(idx);
        }
        
        strTaskIdx = txtTaskIdx.Value;

        DBFileInfo();

        //엔트리 정보를 업데이트 합니다.
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Con.Open();
        Cmd.Parameters.Add("@Idx", SqlDbType.Int);
        Cmd.Parameters.Add("@title", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@title_k", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@title_e", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@title_c", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@summary", SqlDbType.NText);
        Cmd.Parameters.Add("@subtitle1", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@subtitle2", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@synonym", SqlDbType.NVarChar, 500);
        Cmd.Parameters.Add("@editorIdx", SqlDbType.Int);

        Cmd.Parameters["@Idx"].Value = txtEntryNo.Text;
        Cmd.Parameters["@title"].Value = Server.HtmlEncode(txtTitle.Text);
        Cmd.Parameters["@title_k"].Value = Server.HtmlEncode(txtTitle_K.Text);
        Cmd.Parameters["@title_e"].Value = Server.HtmlEncode(txtTitle_E.Text);
        Cmd.Parameters["@title_c"].Value = Server.HtmlEncode(txtTitle_C.Text);
        Cmd.Parameters["@summary"].Value = Server.HtmlEncode(txtSummary.Text);
        Cmd.Parameters["@subtitle1"].Value = Server.HtmlEncode(txtTitle_Sub1.Text);
        Cmd.Parameters["@subtitle2"].Value = Server.HtmlEncode(txtTitle_Sub2.Text);
        Cmd.Parameters["@synonym"].Value = Server.HtmlEncode(txtSynonym.Text);
        Cmd.Parameters["@editorIdx"].Value = Session["uidx"].ToString();
        
        Cmd.CommandText = "USP_Entry_UPDATE";
        Cmd.CommandType = CommandType.StoredProcedure;
        Cmd.ExecuteNonQuery();

        int sortNo = 0;

        Edu edu = JsonConvert.DeserializeObject<Edu>(jsonSaveString);

        List<LUnit> lunitList = edu.LUnit;

        foreach (LUnit lunit in lunitList)
        {
            //대분류 DB에 저장하기
            SqlCommand lunitCommand = null;
            if (Convert.ToInt32(lunit.idx) == -1)
            {
                lunitCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount,TaskIdx) VALUES(" + Convert.ToInt32(txtEntryNo.Text) + ", 0 , " + sortNo + " , '" + lunit.rectype + "' , '' , " + lunit.list.Count + "," + Convert.ToInt32(strTaskIdx) + ")", Con);
                lunitCommand.ExecuteNonQuery();
                sortNo++;

                //제일 마지막에 넣은 것을 가져와서 idx로 설정을 해야 한다.
                lunitCommand = new SqlCommand("SELECT TOP 1 * FROM TEntryData Where EntryIdx=" + Convert.ToInt32(txtEntryNo.Text) + " AND TaskIdx=" + Convert.ToInt32(strTaskIdx) + " ORDER BY idx DESC", Con);
                SqlDataReader sdr = lunitCommand.ExecuteReader(CommandBehavior.SingleResult);
                sdr.Read();
                lunit.idx = Convert.ToString(sdr["idx"]);
                sdr.Close();

            }
            else
            {
                lunitCommand = new SqlCommand("UPDATE TEntryData SET ChildCount=" + lunit.list.Count + " , SortNo=" + sortNo + ",TaskIdx=" + strTaskIdx + " WHERE idx =" + lunit.idx, Con);
                lunitCommand.ExecuteNonQuery();
                sortNo++;
            }

            string lunitContent = string.Empty;
            List<Child> lunitChildList = lunit.list;
            foreach (Child secondItem in lunitChildList)
            {

                if (secondItem.content != null)
                {
                    string secondContent = string.Empty;
                    string strRectype = Convert.ToString(secondItem.rectype);
                    if (strRectype.Equals("MATRIX_TABLE") || strRectype.Equals("SOUND") || strRectype.Equals("PAIRSENTENCE") || strRectype.Equals("VIDEO"))
                    {

                        List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                        string childString = string.Empty;
                        foreach (Child child in childList)
                        {
                            
                            if (child.rectype.Equals(string.Empty))
                            {
                                string childContent = this.convertHtmlToXml(child.content, secondItem.rectype);
                                childString += childContent;
                            }
                            else
                            {
                                string childContent = this.convertHtmlToXml(child.content, child.rectype);
                                childString += "<" + child.rectype + ">" + childContent + "</" + child.rectype + ">";
                            }
                        }

                        secondContent = childString;
                    }
                    else if (strRectype.Equals("IMAGE")) 
                    {

                        string caption = string.Empty;
                        string description = string.Empty;
                        string link = string.Empty;

                        List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                        string childString = string.Empty;
                        foreach (Child child in childList)
                        {
                            if (child.rectype.Equals(string.Empty))
                            {
                                string childContent = this.parseImageHtmlToXml(child.content, child.rectype, caption, description, link);
                                childString += childContent;
                            }
                            else
                            {
                                string childContent = this.convertHtmlToXml(child.content, child.rectype);
                                childString += "<" + child.rectype + ">" + childContent + "</" + child.rectype + ">";

                                if (child.rectype.Equals("CAPTION")) 
                                {
                                    caption = child.content.Replace("<br>", "<br/>");
                                }
                                else if (child.rectype.Equals("DESCRIPTION")) 
                                {
                                    description = child.content.Replace("<br>", "<br/>");
                                }
                                else if (child.rectype.Equals("LINK")) 
                                {
                                    link = child.content.Replace("<br/>", string.Empty).Replace("<br>", string.Empty);
                                }
                            }
                        }

                        secondContent = childString;
                    }
                    else
                    {
                        secondContent = convertHtmlToXml(secondItem.content, secondItem.rectype);
                    }


                    lunitContent += "<" + secondItem.rectype + ">" + secondContent + "</" + secondItem.rectype + ">";
                    SqlCommand secondCommand = null;
                    if (Convert.ToInt32(secondItem.idx) == -1)
                    {
                        secondCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount , TaskIdx) VALUES(" + Convert.ToInt32(txtEntryNo.Text) + "," + lunit.idx + " , " + sortNo + " , '" + secondItem.rectype + "' , '" + secondContent.Replace("'", "''") + "' , 0," + Convert.ToInt32(strTaskIdx) + ")", Con);
                        sortNo++;
                    }
                    else
                    {
                        secondCommand = new SqlCommand("UPDATE TEntryData SET SortNo=" + sortNo + ", Content='" + secondContent.Replace("'", "''") + "',TaskIdx=" + Convert.ToInt32(strTaskIdx) + ",ParentIdx=" + lunit.idx + " WHERE idx=" + secondItem.idx, Con);
                        sortNo++;
                    }

                    secondCommand.ExecuteNonQuery();
                }
            }

            lunitCommand = new SqlCommand("UPDATE TEntryData SET Content='" + lunitContent.Replace("'", "''") + "' WHERE idx =" + lunit.idx, Con);
            lunitCommand.ExecuteNonQuery();
        }

        //퀴즈 DB에 넣기
        List<Quiz> quizList = edu.Quiz;
        if (quizList != null)
        {
            foreach (Quiz quiz in quizList)
            {
                SqlCommand quizCommand = null;

                if (Convert.ToInt32(quiz.idx) == -1)
                {
                    quizCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount , TaskIdx) VALUES(" + Convert.ToInt32(txtEntryNo.Text) + ", 0 , " + sortNo + " , '" + quiz.rectype + "' , '' , " + quiz.list.Count + "," + Convert.ToInt32(strTaskIdx) + ")", Con);
                    quizCommand.ExecuteNonQuery();
                    sortNo++;

                    //제일 마지막에 넣은 것을 가져와서 idx로 설정을 해야 한다.
                    quizCommand = new SqlCommand("SELECT TOP 1 * FROM TEntryData Where EntryIdx=" + Convert.ToInt32(txtEntryNo.Text) + " AND TaskIdx=" + Convert.ToInt32(strTaskIdx) + " ORDER BY idx DESC", Con);
                    SqlDataReader sdr = quizCommand.ExecuteReader(CommandBehavior.SingleResult);
                    sdr.Read();
                    quiz.idx = Convert.ToString(sdr["idx"]);
                    sdr.Close();

                }
                else
                {
                    quizCommand = new SqlCommand("UPDATE TEntryData SET ChildCount=" + quiz.list.Count + " , SortNo=" + sortNo + ",TaskIdx=" + Convert.ToInt32(strTaskIdx) + " WHERE idx =" + quiz.idx, Con);
                    quizCommand.ExecuteNonQuery();
                    sortNo++;
                }

                string quizContent = string.Empty;
                List<Child> quizChildList = quiz.list;
                foreach (Child secondItem in quizChildList)
                {

                    if (secondItem.content != null)
                    {
                        string secondContent = String.Empty;
                        string strRectype = Convert.ToString(secondItem.rectype);
                        if (strRectype.Equals("SOUND") || strRectype.Equals("PAIRSENTENCE") || strRectype.Equals("VIDEO"))
                        {
                            List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                            string childString = string.Empty;
                            foreach (Child child in childList)
                            {

                                if (child.rectype.Equals(string.Empty))
                                {
                                    string childContent = this.convertHtmlToXml(child.content, secondItem.rectype);
                                    childString += childContent;
                                }
                                else
                                {
                                    string childContent = this.convertHtmlToXml(child.content, child.rectype);
                                    childString += "<" + child.rectype + ">" + childContent + "</" + child.rectype + ">";
                                }
                            }

                            secondContent = childString;
                        }
                        else 
                        {
                            secondContent = convertHtmlToXml(secondItem.content, secondItem.rectype);
                        }

                        quizContent += "<" + secondItem.rectype + ">" + secondContent + "</" + secondItem.rectype + ">";
                        SqlCommand secondCommand = null;
                        if (Convert.ToInt32(secondItem.idx) == -1)
                        {
                            secondCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount, TaskIdx) VALUES(" + Convert.ToInt32(txtEntryNo.Text) + "," + quiz.idx + " , " + sortNo + " , '" + secondItem.rectype + "' , '" + secondContent.Replace("'", "''") + "' , 0 , " + Convert.ToInt32(strTaskIdx) + ")", Con);
                            sortNo++;
                        }
                        else
                        {
                            secondCommand = new SqlCommand("UPDATE TEntryData SET SortNo=" + sortNo + ", Content='" + secondContent.Replace("'", "''") + "',TaskIdx=" + Convert.ToInt32(strTaskIdx) + ",ParentIdx=" + quiz.idx + " WHERE idx=" + secondItem.idx, Con);
                            sortNo++;
                        }

                        secondCommand.ExecuteNonQuery();
                    }
                }

                quizCommand = new SqlCommand("UPDATE TEntryData SET Content='" + quizContent.Replace("'", "''") + "' WHERE idx =" + quiz.idx, Con);
                quizCommand.ExecuteNonQuery();
            }
        }

        //태그에 DB에 넣기
        Tag tag = edu.Tag;
        if (tag != null)
        {
            SqlCommand tagCommand = new SqlCommand("Update TEntry SET Tag='" + tag.content.Replace("'", "''") + "' Where idx =" + tag.idx, Con);
            tagCommand.ExecuteNonQuery();
        }

        Con.Close();

        string script = "alert(\"저장되었습니다.\");";
        ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
    }

    private void saveAsEntry(string jsonSaveString)
    {
        //if (hfCategoryIdx.Value.Length > 0)
        //    saveCategory();
        strTaskIdx = txtTaskIdx.Value;

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();

        //엔트리 정보를 삽입하여 엔트리 idx를 알아온다.
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.Parameters.Add("@sourceidx", SqlDbType.Int);
        Cmd.Parameters.Add("@title", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@title_k", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@title_e", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@title_c", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@summary", SqlDbType.NText);
        Cmd.Parameters.Add("@subtitle1", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@subtitle2", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@synonym", SqlDbType.NVarChar, 500);
        Cmd.Parameters.Add("@editorIdx", SqlDbType.Int);

        Cmd.Parameters["@sourceidx"].Value = txtEntryNo.Text;
        Cmd.Parameters["@title"].Value = Server.HtmlEncode(txtTitle.Text);
        Cmd.Parameters["@title_k"].Value = Server.HtmlEncode(txtTitle_K.Text);
        Cmd.Parameters["@title_e"].Value = Server.HtmlEncode(txtTitle_E.Text);
        Cmd.Parameters["@title_c"].Value = Server.HtmlEncode(txtTitle_C.Text);
        Cmd.Parameters["@summary"].Value = Server.HtmlEncode(txtSummary.Text);
        Cmd.Parameters["@subtitle1"].Value = Server.HtmlEncode(txtTitle_Sub1.Text);
        Cmd.Parameters["@subtitle2"].Value = Server.HtmlEncode(txtTitle_Sub2.Text);
        Cmd.Parameters["@synonym"].Value = Server.HtmlEncode(txtSynonym.Text);
        Cmd.Parameters["@editorIdx"].Value = Session["uidx"].ToString();

        Cmd.CommandText = "USP_Entry_SAVEAS";
        Cmd.CommandType = CommandType.StoredProcedure;

        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.SingleResult);
        int entryIdx = 0;
        if (reader.Read())
        {
            entryIdx = Convert.ToInt32(reader["idx"]);
        }
        reader.Close();


        //DB에 데이터를 저장한다.
        int sortNo = 0;

        Edu edu = JsonConvert.DeserializeObject<Edu>(jsonSaveString);
        List<LUnit> lunitList = edu.LUnit;

        foreach (LUnit lunit in lunitList)
        {
            //대분류 DB에 저장하기
            SqlCommand lunitCommand = new SqlCommand();
            lunitCommand.Connection = Con;
            lunitCommand.Parameters.Add("@entryIdx", SqlDbType.Int).Value = entryIdx;
            lunitCommand.Parameters.Add("@sortNo", SqlDbType.Int).Value = sortNo;
            lunitCommand.Parameters.Add("@recType", SqlDbType.VarChar, 20).Value = lunit.rectype;
            lunitCommand.Parameters.Add("@childCount", SqlDbType.Int).Value = lunit.list.Count;
            lunitCommand.Parameters.Add("@taskIdx", SqlDbType.Int).Value = Convert.ToInt32(strTaskIdx);

            lunitCommand.CommandText = "USP_EntryData_INSERT_RETURN";
            lunitCommand.CommandType = CommandType.StoredProcedure;

            SqlDataReader lunitReader = lunitCommand.ExecuteReader(CommandBehavior.SingleResult);
            lunitReader.Read();
            lunit.idx = Convert.ToString( lunitReader["idx"] );
            lunitReader.Close();

            sortNo++;

            string lunitContent = string.Empty;
            List<Child> lunitChildList = lunit.list;
            foreach (Child secondItem in lunitChildList)
            {

                if (secondItem.content != null)
                {
                    string secondContent = string.Empty;

                    if (Convert.ToString(secondItem.rectype).Equals("MATRIX_TABLE") ||
                        Convert.ToString(secondItem.rectype).Equals("SOUND") ||
                        Convert.ToString(secondItem.rectype).Equals("PAIRSENTENCE") ||
                        Convert.ToString(secondItem.rectype).Equals("VIDEO"))
                    {
                        List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                        string childString = string.Empty;
                        foreach (Child child in childList)
                        {
                            
                            if (child.rectype.Equals(string.Empty))
                            {
                                string childContent = this.convertHtmlToXml(child.content, secondItem.rectype);
                                childString += childContent;
                            }
                            else
                            {
                                string childContent = this.convertHtmlToXml(child.content, child.rectype);
                                childString += "<" + child.rectype + ">" + childContent + "</" + child.rectype + ">";
                            }
                        }

                        secondContent = childString;
                    }
                    else if (Convert.ToString(secondItem.rectype).Equals("IMAGE"))
                    {

                        string caption = string.Empty;
                        string description = string.Empty;
                        string link = string.Empty;

                        List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                        string childString = string.Empty;
                        foreach (Child child in childList)
                        {
                            if (child.rectype.Equals(string.Empty))
                            {
                                string childContent = this.parseImageHtmlToXml(child.content, child.rectype, caption, description, link);
                                childString += childContent;
                            }
                            else
                            {
                                string childContent = this.convertHtmlToXml(child.content, child.rectype);
                                childString += "<" + child.rectype + ">" + childContent + "</" + child.rectype + ">";

                                if (child.rectype.Equals("CAPTION"))
                                {
                                    caption = child.content.Replace("<br>", "<br/>");
                                }
                                else if (child.rectype.Equals("DESCRIPTION"))
                                {
                                    description = child.content.Replace("<br>", "<br/>");
                                }
                                else if (child.rectype.Equals("LINK"))
                                {
                                    link = child.content.Replace("<br/>", string.Empty).Replace("<br>", string.Empty);
                                    link = link.Replace("<p>", string.Empty).Replace("</p>", string.Empty);
                                }
                            }
                        }

                        secondContent = childString;
                    }
                    else
                    {
                        secondContent = convertHtmlToXml(secondItem.content , secondItem.rectype);
                    }


                    lunitContent += "<" + secondItem.rectype + ">" + secondContent + "</" + secondItem.rectype + ">";
                    SqlCommand secondCommand = new SqlCommand();
                    secondCommand.Connection = Con;
                    secondCommand.Parameters.Add("@entryIdx", SqlDbType.Int).Value = entryIdx;
                    secondCommand.Parameters.Add("@parentIdx", SqlDbType.Int).Value = Convert.ToInt32(lunit.idx);
                    secondCommand.Parameters.Add("@sortNo", SqlDbType.Int).Value = sortNo;
                    secondCommand.Parameters.Add("@recType", SqlDbType.VarChar, 20).Value = secondItem.rectype;
                    secondCommand.Parameters.Add("@content", SqlDbType.NText).Value = secondContent.Replace("'", "''");
                    secondCommand.Parameters.Add("@taskIdx", SqlDbType.Int).Value = Convert.ToInt32(strTaskIdx);

                    secondCommand.CommandText = "USP_EntryData_INSERT";
                    secondCommand.CommandType = CommandType.StoredProcedure;
                    secondCommand.ExecuteNonQuery();
                    sortNo++;
                }
            }

            lunitCommand = new SqlCommand("UPDATE TEntryData SET Content='" + lunitContent.Replace("'", "''") + "' WHERE idx =" + lunit.idx, Con);
            lunitCommand.ExecuteNonQuery();
        }

        //퀴즈 DB에 넣기
        List<Quiz> quizList = edu.Quiz;
        if (quizList != null)
        {
            foreach (Quiz quiz in quizList)
            {
                SqlCommand quizCommand = new SqlCommand();
                quizCommand.Connection = Con;
                quizCommand.Parameters.Add("@entryIdx", SqlDbType.Int).Value = entryIdx;
                quizCommand.Parameters.Add("@sortNo", SqlDbType.Int).Value = sortNo;
                quizCommand.Parameters.Add("@recType", SqlDbType.VarChar, 20).Value = quiz.rectype;
                quizCommand.Parameters.Add("@childCount", SqlDbType.Int).Value = quiz.list.Count;
                quizCommand.Parameters.Add("@taskIdx", SqlDbType.Int).Value = Convert.ToInt32(strTaskIdx);

                quizCommand.CommandText = "USP_EntryData_INSERT_RETURN";
                quizCommand.CommandType = CommandType.StoredProcedure;

                SqlDataReader quizReader = quizCommand.ExecuteReader(CommandBehavior.SingleResult);
                quizReader.Read();
                quiz.idx = Convert.ToString(quizReader["idx"]);
                quizReader.Close();
                sortNo++;
                
                string quizContent = string.Empty;
                List<Child> quizChildList = quiz.list;
                foreach (Child secondItem in quizChildList)
                {
                    if (secondItem.content != null)
                    {
                        string secondContent = string.Empty;
                        secondContent = convertHtmlToXml(secondItem.content , secondItem.rectype);
                        quizContent += "<" + secondItem.rectype + ">" + secondContent + "</" + secondItem.rectype + ">";
                        SqlCommand secondCommand = new SqlCommand();
                        secondCommand.Connection = Con;
                        secondCommand.Parameters.Add("@entryIdx", SqlDbType.Int).Value = entryIdx;
                        secondCommand.Parameters.Add("@parentIdx", SqlDbType.Int).Value = Convert.ToInt32(quiz.idx);
                        secondCommand.Parameters.Add("@sortNo", SqlDbType.Int).Value = sortNo;
                        secondCommand.Parameters.Add("@recType", SqlDbType.VarChar, 20).Value = secondItem.rectype;
                        secondCommand.Parameters.Add("@content", SqlDbType.NText).Value = secondContent.Replace("'", "''");
                        secondCommand.Parameters.Add("@taskIdx", SqlDbType.Int).Value = Convert.ToInt32(strTaskIdx);

                        secondCommand.CommandText = "USP_EntryData_INSERT";
                        secondCommand.CommandType = CommandType.StoredProcedure;
                        secondCommand.ExecuteNonQuery();
                        sortNo++;
                    }

                }
                quizCommand = new SqlCommand("UPDATE TEntryData SET Content='" + quizContent.Replace("'", "''") + "' WHERE idx =" + quiz.idx, Con);
                quizCommand.ExecuteNonQuery();
            }
        }

        //태그에 DB에 넣기
        Tag tag = edu.Tag;
        if (tag != null)
        {
            SqlCommand tagCommand = new SqlCommand("Update TEntry SET Tag='" + tag.content.Replace("'", "''") + "' Where idx =" + entryIdx, Con);
            tagCommand.ExecuteNonQuery();
        }

        Con.Close();

        if(hfCategoryIdx.Value.Length>0)
        {
            saveCategory(entryIdx.ToString());
        }

        string script = "alert(\"저장되었습니다.\");location.href='EntryEdit.aspx?Idx=" + entryIdx + "&pageNo=" + pageNo + "&pageSize=" + pageSize + "';";
        ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
    }

    private void removeEntry(string deleteIDStrings)
    {
        string[] splitedDeleteIDString = deleteIDStrings.Split(',');
        foreach (string deleteID in splitedDeleteIDString)
        {
            if (deleteID.Trim().Length != 0)
            {
                this.deleteEntry(Convert.ToInt32(deleteID));
            }
        }
    }

    private void deleteEntry(int deleteID)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlCommand deleteCommand = new SqlCommand("UPDATE TEntryData SET DelFlag=1 WHERE Idx=" + deleteID, Con);
        deleteCommand.ExecuteNonQuery();

        if (deleteID > 0 && deleteID != null)
        {
            SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx FROM TEntryData WHERE ParentIdx=" + deleteID, Con);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow entryDatarow in dt.Rows)
                {
                    deleteEntry(Convert.ToInt32(entryDatarow["Idx"]));
                }
            }
        }
        if (Con.State == ConnectionState.Open)
            Con.Close();
    }

    private void deleteEntryFile( )
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        int entryIdx = Convert.ToInt32( txtEntryNo.Text );
        SqlCommand deleteCommand = new SqlCommand("DELETE FROM TFileData WHERE EntryIdx=" + entryIdx  , Con);

        deleteCommand.ExecuteNonQuery();
        Con.Close();
    }


    private void saveEntryFile(string fileName , string type)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();

        int entryIdx = Convert.ToInt32(txtEntryNo.Text);
        int tIdx = Convert.ToInt32(strTaskIdx);
        string strQuery = "INSERT INTO TFileData(FileName , TaskIdx , EntryIdx,FileType) VALUES('" + fileName + "',"+ tIdx +"," + entryIdx + ",'" + type + "')";
        SqlCommand deleteCommand = new SqlCommand(strQuery , Con);
        deleteCommand.ExecuteNonQuery();
        Con.Close();
    }

    private void saveEntryFile(string fileName, string type, string caption, string description, string link) {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();

        int entryIdx = Convert.ToInt32(txtEntryNo.Text);
        int tIdx = Convert.ToInt32(strTaskIdx);
        string strQuery = "INSERT INTO TFileData(FileName , TaskIdx , EntryIdx, FileType , FilePath , Description , Caption ) VALUES('" + fileName + "'," + tIdx + "," + entryIdx + ",'" + type + "' , '" + link.Replace("'", "''") + "','" + description.Replace("'", "''") + "','" + caption.Replace("'", "''") + "')";
        SqlCommand saveCommand = new SqlCommand(strQuery, Con);
        saveCommand.ExecuteNonQuery();
        Con.Close();
    }

    private string saveImageFileWithDataType( string fileName , string dataType ) {

        string result = string.Empty;

        string newFileName = fileName.Replace("_sym1", string.Empty).Replace("_sym2", string.Empty).Replace("_sym3", string.Empty).Replace("_han", string.Empty);
        
        if( !dataType.Equals(string.Empty) )
        {

            int dotIndex = newFileName.LastIndexOf(".");
            newFileName = newFileName.Insert(dotIndex, "_" + dataType);
        }

        if (!fileName.Equals(newFileName))
        {
            string originalFilePath = Server.MapPath("../") + "\\CMS100Data\\EntryData\\" + strTaskIdx + "\\" + fileName;
            string newFilePath = Server.MapPath("../") + "\\CMS100Data\\EntryData\\" + strTaskIdx + "\\" + newFileName;

            File.Copy(originalFilePath, newFilePath , true);
            //File.Delete(originalFilePath);

            result = newFileName;
        }
        else {
            result = fileName;
        }

        return result;
    }

    private string parseImageHtmlToXml(string strHtml, string recType , string caption, string description , string link)
    {
        //공백을 없앤다.
        strHtml = strHtml.Replace("&nbsp;", " ");

        //img 태그와 br 태그의 닫는 태그를 넣는다.
        strHtml = strHtml.Replace("<br>", "<br/>");
        int imgStartIndex = 0;
        int imgEndIndex = 0;

        imgStartIndex = strHtml.IndexOf("<img", 0);
        while (imgStartIndex > -1)
        {

            imgEndIndex = strHtml.IndexOf(">", imgStartIndex);
            strHtml = strHtml.Insert(imgEndIndex, "/");
            imgStartIndex = strHtml.IndexOf("<img", imgEndIndex);

        }

        //이미지의 이상한 문자를 지운다.
        StringBuilder sb = new StringBuilder(strHtml);
        sb.Replace(char.ConvertFromUtf32(8203), string.Empty);
        strHtml = sb.ToString();

        //div 태그가 있는지 확인한다.
        //div 태그가 있다면 정렬을 가져온다.
        string strAlign = string.Empty;
        XmlDocument divXml = new XmlDocument();
        divXml.LoadXml("<edu>" + strHtml + "</edu>");

        XmlNodeList divNodeList = divXml.GetElementsByTagName("div");
        if (divNodeList.Count > 0)
        {
            foreach (XmlNode divNode in divNodeList)
            {
                XmlAttributeCollection attributes = divNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Value.Contains("text-align"))
                    {
                        strAlign = "<ALIGN>" + attribute.Value.Replace("text-align:", "").Replace(";", string.Empty).Trim() + "</ALIGN>";
                    }
                }
            }


            int divStartIndex = strHtml.IndexOf("<div", 0);
            while (divStartIndex > -1)
            {
                int divEndIndex = strHtml.IndexOf(">", divStartIndex) + 1;
                strHtml = strHtml.Remove(divStartIndex, divEndIndex - divStartIndex);
                divStartIndex = strHtml.IndexOf("<div", divStartIndex);
            }
            //div 태그를 지운다.
            strHtml = strHtml.Replace("</div>", string.Empty);
        }


        //링크 태그의 쓰레기 속성을 지운다.
        XmlDocument xml = new XmlDocument();
        xml.PreserveWhitespace = true;
        xml.LoadXml("<edu>" + strHtml + "</edu>");


        //이미지 태그의 쓰레기 속성을 지운다.
        XmlNodeList imgNodeList = xml.GetElementsByTagName("img");

        if (imgNodeList.Count > 0)
        {

            foreach (XmlNode imgNode in imgNodeList)
            {
                XmlAttributeCollection attributes = imgNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Name.Contains("data-cke-saved-src"))
                    {
                        attributes.Remove(attribute);
                        break;
                    }
                }

                attributes = imgNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Name.Contains("alt"))
                    {
                        attributes.Remove(attribute);
                        break;
                    }
                }
            }
        }
        

        //문자열로 출력시킨다.
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.OmitXmlDeclaration = true;
        using (var stringWriter = new StringWriter())
        using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
        {
            xml.WriteTo(xmlTextWriter);
            xmlTextWriter.Flush();
            strHtml = stringWriter.GetStringBuilder().ToString();
        }

        strHtml = strHtml.Replace("<edu>", string.Empty);
        strHtml = strHtml.Replace("</edu>", string.Empty);

        //태그를 파싱한다.
        string strConvert = string.Empty;
        if (strAlign.Equals(string.Empty))
        {
            strAlign = "<ALIGN></ALIGN>";
        }

        StringWriter imgStringWriter = new StringWriter();
        XmlTextWriter xmlWriter = new XmlTextWriter(imgStringWriter);
        string imgResult = strHtml;
        imgResult = "<edu>" + imgResult + "</edu>";

        string strText = string.Empty;
        Boolean startedImageTage = false;

        using (XmlReader reader = XmlReader.Create(new StringReader(imgResult)))
        {
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Text:
                        if (startedImageTage)
                        {
                            xmlWriter.WriteEndElement();
                            startedImageTage = false;
                        }

                        strText += reader.Value;

                        break;
                    case XmlNodeType.Element:

                        switch (reader.Name)
                        {
                            case "img":
                                if (strText != string.Empty)
                                {
                                    xmlWriter.WriteStartElement("TEXT");
                                    xmlWriter.WriteCData(strText);
                                    xmlWriter.WriteEndElement();
                                    strText = string.Empty;
                                }

                                if (!startedImageTage)
                                {
                                    xmlWriter.WriteStartElement("IMGS");
                                    startedImageTage = true;
                                }

                                xmlWriter.WriteStartElement("IMG");

                                string imgHeight = string.Empty;
                                string imgWidth = string.Empty;
                                string imgStyle = reader.GetAttribute("style");
                                string fileSrc = reader.GetAttribute("src");

                                if (imgStyle != null)
                                {
                                    string[] splitedImgStyle = imgStyle.Split(new char[] { ';' });

                                    foreach (string attribute in splitedImgStyle)
                                    {
                                        if (attribute.Contains("width"))
                                        {
                                            imgWidth = attribute.Replace("width:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                        else if (attribute.Contains("height"))
                                        {
                                            imgHeight = attribute.Replace("height:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                    }
                                }

                                xmlWriter.WriteStartElement("FILENAME");
                                string[] splitedFileName = fileSrc.Split(new char[] { '/' });
                                string fileName = splitedFileName[splitedFileName.Length - 1];

                                if (fileName != null && !fileName.Equals(string.Empty))
                                {
                                    this.saveEntryFile(fileName, "IMG", caption, description, link);
                                }
                                xmlWriter.WriteValue(fileName);
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteRaw("<WIDTH>" + imgWidth + "</WIDTH>");

                                xmlWriter.WriteRaw("<HEIGHT>" + imgHeight + "</HEIGHT>");
                                xmlWriter.WriteEndElement();
                                break;
                        }

                        break;
                    case XmlNodeType.EndElement:

                        switch (reader.Name)
                        {
                            case "img":
                                xmlWriter.WriteEndElement();
                                break;
                        }
                        break;
                }
            }
        }

        StringBuilder stringBuilder = new StringBuilder( strText);
        stringBuilder.Replace(char.ConvertFromUtf32(8203), string.Empty);
        strText = stringBuilder.ToString();

        if (strText != string.Empty)
        {
            xmlWriter.WriteStartElement("TEXT");
            xmlWriter.WriteCData(strText);
            xmlWriter.WriteEndElement();
        }
        if (startedImageTage) xmlWriter.WriteEndElement();

        return imgStringWriter.ToString() + strAlign;
    }



public class Edu {
    [JsonProperty("lunit")]
    public List<LUnit> LUnit { get; set; }
    [JsonProperty("quiz")]
    public List<Quiz> Quiz { get; set; }
    [JsonProperty("tag")]
    public Tag Tag { get; set; }
}


public class Tag
{
    [JsonProperty("idx")]
    public string idx { get; set; }
    [JsonProperty("rectype")]
    public string rectype { get; set; }
    [JsonProperty("content")]
    public string content { get; set; }
}

 public class LUnit
 {
      [JsonProperty("idx")]
     public string idx { get; set; }
     [JsonProperty("rectype")]
     public string rectype { get; set; }
     [JsonProperty("list")]
     public List<Child> list { get; set; }
 }

 public class Quiz
 {
     [JsonProperty("idx")]
     public string idx { get; set; }
     [JsonProperty("rectype")]
     public string rectype { get; set; }
     [JsonProperty("list")]
     public List<Child> list { get; set; }
 }



 public class Child
 {
     [JsonProperty("idx")]
     public string idx { get; set; }
     [JsonProperty("rectype")]
     public string rectype { get; set; }
     [JsonProperty("content")]
     public string content { get; set; }
 }


}