﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Text;
using System.Net;


public partial class main_GetContents : System.Web.UI.Page
{
    private static string imgURL = string.Empty;
    private static string mOriginTaskIdx = string.Empty;
    private static string mCurrentTaskIdx = string.Empty;
    private static string mOriginFolderPath = string.Empty;
    private static string mCurrentFolderPath = string.Empty;
    private static string mCurrentEntryIdx = string.Empty;
    private static int mFileNo = 0;
    private static string mImageUrl = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    public static string getContents(string idxList, string taskIdx)
    {
        mOriginTaskIdx = taskIdx;
        
        if (HttpContext.Current.Request.Cookies["UserSettings"] != null)
        {
            if (HttpContext.Current.Request.Cookies["UserSettings"]["TaskIdx"] != null)
            {
                mCurrentTaskIdx = HttpContext.Current.Request.Cookies["UserSettings"]["TaskIdx"];
                mCurrentEntryIdx = HttpContext.Current.Request.Cookies["UserSettings"]["EntryIdx"];
            }
        }


        string rootUrl = "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"];

        string rootPath = HttpContext.Current.Server.MapPath("../");
        mOriginFolderPath = rootPath + "CMS100Data\\EntryData\\" + mOriginTaskIdx + "\\";
        mCurrentFolderPath = rootPath + "CMS100Data\\EntryData\\" + mCurrentTaskIdx + "\\";
        mImageUrl = rootUrl +"/CMS100Data/EntryData/" + mCurrentTaskIdx + "/";

        //불러오기 아이디 리스트를 분해한다.
        string[] arrIdxList = idxList.Split(new char[] { ',' });

        DataTable entryTable = new DataTable();
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        SqlConnection Con = new SqlConnection(connectionString);



        foreach (string unitIdx in arrIdxList)
        {

            if (unitIdx.Trim().Length > 0)
            {
                

                Con.Open();

                string strQuery = "SELECT * FROM TEntryData Where idx=" + unitIdx + " AND DelFlag=0";
                SqlCommand cmd = new SqlCommand(strQuery, Con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(entryTable);

                DataRow[] entryRowList = entryTable.Select("idx=" + unitIdx);
                int childCount = Convert.ToInt32(entryRowList[0]["childcount"]);

                if (childCount > 0)
                {

                    strQuery = "SELECT * FROM TEntryData Where parentidx=" + unitIdx + " AND DelFlag=0";
                    cmd = new SqlCommand(strQuery, Con);
                    sda = new SqlDataAdapter(cmd);
                    sda.Fill(entryTable);

                }

                Con.Close();
            }
        }

        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

        Dictionary<string, object> row;
        foreach (DataRow dr in entryTable.Rows)
        {

            String recType = Convert.ToString(dr["RecType"]);
            row = new Dictionary<string, object>();
            foreach (DataColumn col in entryTable.Columns)
            {
                if (col.ColumnName.Equals("Content"))
                {
                    if (recType.Equals("MATRIX_TABLE"))
                    {
                        string content = Convert.ToString(dr[col]);
                        Dictionary<string, object> subParseRow = new Dictionary<string, object>();

                        int captionStartIndex = content.IndexOf("<CAPTION>");
                        if (captionStartIndex != -1)
                        {
                            string captionEndTag = "</CAPTION>";
                            int captionEndIndex = content.IndexOf(captionEndTag);
                            string caption = content.Substring(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                            content = content.Remove(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                            Dictionary<string, object> captionRow = new Dictionary<string, object>();
                            subParseRow.Add("caption", convertXmlToHtml(caption, "CAPTION"));
                        }

                        int descriptionStartIndex = content.IndexOf("<DESCRIPTION>");
                        if (descriptionStartIndex != -1)
                        {
                            string descriptionEndTag = "</DESCRIPTION>";
                            int descriptionEndIndex = content.IndexOf(descriptionEndTag);
                            string description = content.Substring(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex + descriptionEndTag.Length);
                            content = content.Remove(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex + descriptionEndTag.Length);
                            Dictionary<string, object> descriptionRow = new Dictionary<string, object>();
                            subParseRow.Add("description", convertXmlToHtml(description, "DESCRIPTION"));
                        }

                        Dictionary<string, object> imageRow = new Dictionary<string, object>();
                        subParseRow.Add("node", convertXmlToHtml(content, recType));

                        string parseContent = serializer.Serialize(subParseRow);
                        row.Add(col.ColumnName, parseContent);

                    }
                    else if (recType.Equals("IMAGE"))
                    {
                        string content = Convert.ToString(dr[col]);
                        Dictionary<string, object> subParseRow = new Dictionary<string, object>();

                        int captionStartIndex = content.IndexOf("<CAPTION>");
                        if (captionStartIndex != -1)
                        {
                            string captionEndTag = "</CAPTION>";
                            int captionEndIndex = content.IndexOf(captionEndTag);
                            string caption = content.Substring(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                            content = content.Remove(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                            Dictionary<string, object> captionRow = new Dictionary<string, object>();
                            subParseRow.Add("caption", convertXmlToHtml(caption, "CAPTION"));
                        }

                        int descriptionStartIndex = content.IndexOf("<DESCRIPTION>");
                        if (descriptionStartIndex != -1)
                        {
                            string descriptionEndTag = "</DESCRIPTION>";
                            int descriptionEndIndex = content.IndexOf(descriptionEndTag);
                            string description = content.Substring(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex + descriptionEndTag.Length);
                            content = content.Remove(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex + descriptionEndTag.Length);
                            Dictionary<string, object> descriptionRow = new Dictionary<string, object>();
                            subParseRow.Add("description", convertXmlToHtml(description, "DESCRIPTION"));
                        }

                        int linkStartIndex = content.IndexOf("<LINK>");
                        if (linkStartIndex != -1)
                        {
                            string linkEndTag = "</LINK>";
                            int linkEndIndex = content.IndexOf(linkEndTag);
                            string link = content.Substring(linkStartIndex, linkEndIndex - linkStartIndex + linkEndTag.Length);
                            content = content.Remove(linkStartIndex, linkEndIndex - linkStartIndex + linkEndTag.Length);
                            Dictionary<string, object> linkRow = new Dictionary<string, object>();
                            subParseRow.Add("link", convertXmlToHtml(link, "LINK"));
                        }

                        Dictionary<string, object> imageRow = new Dictionary<string, object>();
                        subParseRow.Add("node", convertXmlToHtml(content, recType));

                        string parseContent = serializer.Serialize(subParseRow);
                        row.Add(col.ColumnName, parseContent);

                    }
                    else if (recType.Equals("SOUND"))
                    {
                        string content = Convert.ToString(dr[col]);
                        Dictionary<string, object> subParseRow = new Dictionary<string, object>();

                        int sentenceStartIndex = content.IndexOf("<SENTENCE>");
                        if (sentenceStartIndex != -1)
                        {
                            string sentenceEndTag = "</SENTENCE>";
                            int sentenceEndIndex = content.IndexOf(sentenceEndTag);
                            string sentence = content.Substring(sentenceStartIndex, sentenceEndIndex - sentenceStartIndex + sentenceEndTag.Length);
                            content = content.Remove(sentenceStartIndex, sentenceEndIndex - sentenceStartIndex + sentenceEndTag.Length);
                            Dictionary<string, object> sentenceRow = new Dictionary<string, object>();
                            subParseRow.Add("sentence", convertXmlToHtml(sentence, "SENTENCE"));
                        }

                        Dictionary<string, object> soundRow = new Dictionary<string, object>();
                        subParseRow.Add("node", convertXmlToHtml(content, recType));

                        string parseContent = serializer.Serialize(subParseRow);
                        row.Add(col.ColumnName, parseContent);

                    }
                    else if (recType.Equals("PAIRSENTENCE"))
                    {
                        string content = Convert.ToString(dr[col]);
                        Dictionary<string, object> subParseRow = new Dictionary<string, object>();

                        int sentenceStartIndex = content.IndexOf("<SENTENCE>");
                        if (sentenceStartIndex != -1)
                        {
                            string sentenceEndTag = "</SENTENCE>";
                            int sentenceEndIndex = content.IndexOf(sentenceEndTag);
                            string sentence = content.Substring(sentenceStartIndex, sentenceEndIndex - sentenceStartIndex + sentenceEndTag.Length);
                            content = content.Remove(sentenceStartIndex, sentenceEndIndex - sentenceStartIndex + sentenceEndTag.Length);
                            Dictionary<string, object> sentenceRow = new Dictionary<string, object>();
                            subParseRow.Add("sentence", convertXmlToHtml(sentence, "SENTENCE"));
                        }

                        int translationStartIndex = content.IndexOf("<TRANSLATION>");
                        if (translationStartIndex != -1)
                        {
                            string translationEndTag = "</TRANSLATION>";
                            int translationEndIndex = content.IndexOf(translationEndTag);
                            string translation = content.Substring(translationStartIndex, translationEndIndex - translationStartIndex + translationEndTag.Length);
                            content = content.Remove(translationStartIndex, translationEndIndex - translationStartIndex + translationEndTag.Length);
                            Dictionary<string, object> translationRow = new Dictionary<string, object>();
                            subParseRow.Add("translation", convertXmlToHtml(translation , "TRANSLATION"));
                        }

                        int grammarStartIndex = content.IndexOf("<GRAMMAR>");
                        if (grammarStartIndex != -1)
                        {
                            string grammarEndTag = "</GRAMMAR>";
                            int grammarEndIndex = content.IndexOf(grammarEndTag);
                            string grammar = content.Substring(grammarStartIndex, grammarEndIndex - grammarStartIndex + grammarEndTag.Length);
                            content = content.Remove(translationStartIndex, grammarEndIndex - grammarStartIndex + grammarEndTag.Length);
                            Dictionary<string, object> translationRow = new Dictionary<string, object>();
                            subParseRow.Add("grammar", convertXmlToHtml(grammar, "GRAMMAR"));
                        }

                        Dictionary<string, object> pairSentenceRow = new Dictionary<string, object>();
                        subParseRow.Add("node", convertXmlToHtml(content, recType));

                        string parseContent = serializer.Serialize(subParseRow);
                        row.Add(col.ColumnName, parseContent);

                    }
                    else if (recType.Equals("VIDEO"))
                    {
                        string content = Convert.ToString(dr[col]);
                        Dictionary<string, object> subParseRow = new Dictionary<string, object>();

                        int sentenceStartIndex = content.IndexOf("<SENTENCE>");
                        if (sentenceStartIndex != -1)
                        {
                            string sentenceEndTag = "</SENTENCE>";
                            int sentenceEndIndex = content.IndexOf(sentenceEndTag);
                            string sentence = content.Substring(sentenceStartIndex, sentenceEndIndex - sentenceStartIndex + sentenceEndTag.Length);
                            content = content.Remove(sentenceStartIndex, sentenceEndIndex - sentenceStartIndex + sentenceEndTag.Length);
                            Dictionary<string, object> sentenceRow = new Dictionary<string, object>();
                            subParseRow.Add("sentence", convertXmlToHtml(sentence, "SETENCE"));
                        }

                        Dictionary<string, object> videoRow = new Dictionary<string, object>();
                        subParseRow.Add("node", convertXmlToHtml(content, recType));

                        string parseContent = serializer.Serialize(subParseRow);
                        row.Add(col.ColumnName, parseContent);
                    }
                    else if (recType.Equals("INDEXCONTENT") || recType.Equals("QUIZ") )
                    { 

                    }
                    else
                    {
                        string parseContent = convertXmlToHtml(Convert.ToString(dr[col]), recType);
                        row.Add(col.ColumnName, parseContent);
                    }

                }
                else
                {
                    row.Add(col.ColumnName, dr[col]);
                }

            }
            rows.Add(row);
        }

        return serializer.Serialize(rows);
    }

    public static string convertXmlToHtml(string strXml, string recType)
    {

        //align이 있는지 확인한다.
        bool isAlign = false;
        string strAlign = String.Empty;
        int startAlignIndex = strXml.IndexOf("<ALIGN>");
        string alignEndTag = "</ALIGN>";
        int endAlignIndex = strXml.LastIndexOf(alignEndTag);
        if (startAlignIndex > -1)
        {
            isAlign = true;
            string alignTag = strXml.Substring(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
            strAlign = alignTag.Replace("<ALIGN>", "").Replace("</ALIGN>", "");
            strXml = strXml.Remove(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
        }


        if (recType.Equals("IMAGE"))
        {

            if (strXml.Contains("<IMGS>"))
            {
                if (!strXml.Contains("</IMGS>"))
                {
                    strXml += "</IMGS>";
                }
            }

            return parseXmlToHtml(strXml, isAlign, strAlign);
        }
        else if (recType.Equals("MATRIX_TABLE"))
        {
            return parseXmlToHtml(strXml, isAlign, strAlign);
        }
        else if (recType.Equals("SOUND") || recType.Equals("PAIRSENTENCE") || recType.Equals("VIDEO"))
        {
            strXml = strXml.Replace("<FILENAME>", "");
            strXml = strXml.Replace("</FILENAME>", "");
            string originalFileName = strXml;
            string[] splitedOriginalFileName = originalFileName.Split(new char[]{'.'});
            string extension = splitedOriginalFileName[splitedOriginalFileName.Length - 1];
            string newFileName = mCurrentTaskIdx + "_" + mCurrentEntryIdx + "_" + DateTime.Now.ToString("HHmmss") + "_" +  mFileNo++ +"." + extension;
            
            string orginalFile = Path.Combine(mOriginFolderPath , originalFileName );
            string newFile = Path.Combine(mCurrentFolderPath, newFileName);

            //파일을 복사한다.
            if (!System.IO.Directory.Exists(mCurrentFolderPath))
            {
                System.IO.Directory.CreateDirectory( mCurrentFolderPath );
            }

            File.Copy(orginalFile, newFile , true);
            saveFile(originalFileName, newFileName, extension);

            return newFileName;
        }
        else if (recType.Equals("LEVEL"))
        {
            return strXml;
        }
        else
        {

            strXml = strXml.Replace("<img src=\"", "<img src=\"" + imgURL);
            strXml = strXml.Replace("<br>", "<br/>");

            string result = string.Empty;
            strXml = "<edu>" + strXml + "</edu>";
            using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.CDATA:
                            if (isAlign)
                            {
                                StringWriter stringWriter = new StringWriter();
                                HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                                htmlWriter.Write(reader.Value);
                                htmlWriter.RenderEndTag();
                                result += stringWriter.ToString();
                            }
                            else
                            {
                                result += reader.Value;
                            }
                            break;
                        case XmlNodeType.Text:
                            result += reader.Value;
                            break;

                    }
                }
            }

            return result;

        }


    }

    private static string parseXmlToHtml(string strXml, bool isAlign, string strAlign)
    {
        //루트태그를 붙여줌
        strXml = "<edu>" + strXml + "</edu>";

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

        using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
        {

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.CDATA)
                {
                    htmlWriter.Write(reader.Value);

                }
                else if (reader.NodeType == XmlNodeType.Element)
                {

                    switch (reader.Name)
                    {
                        case "TEXT":

                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "IMGS":
                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "IMG":
                            break;
                        case "WIDTH":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Width, reader.Value);
                            break;
                        case "HEIGHT":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Height, reader.Value);
                            break;
                        case "FILENAME":
                            reader.Read();
                            
                            string originalFileName = reader.Value;
                            string[] splitedOriginalFileName = originalFileName.Split(new char[]{'.'});
                            string extension = splitedOriginalFileName[splitedOriginalFileName.Length - 1];
                            string newFileName = mCurrentTaskIdx + "_" + mCurrentEntryIdx + "_" + DateTime.Now.ToString("HHmmss") + "_" +  mFileNo++ +"." + extension;
            
                            string orginalFile = Path.Combine(mOriginFolderPath , originalFileName );
                            string newFile = Path.Combine(mCurrentFolderPath, newFileName);

                            //파일을 복사한다.
                            if (!System.IO.Directory.Exists(mCurrentFolderPath))
                            {
                                System.IO.Directory.CreateDirectory(mCurrentFolderPath);
                            }

                            File.Copy(orginalFile, newFile , true);

                            //파일을 데이터 베이스에 저장한다.
                            saveFile(originalFileName, newFileName, extension);

                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Src, mImageUrl + newFileName );
                            break;
                    }

                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    switch (reader.Name)
                    {
                        case "TEXT":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMGS":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMG":
                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Img);
                            htmlWriter.RenderEndTag();
                            break;
                        case "WIDTH":
                            break;
                        case "HEIGHT":
                            break;
                    }
                }
            }
        }

        return stringWriter.ToString();
    }


    private static void saveFile(string originalFileName , string newFileName , string extension ) 
    {

        string connectionString = string.Empty;

        //데이터베이스에 이름을 저장한다.
        String strWidth = string.Empty;
        String strHeight = string.Empty;
        String fileType = string.Empty;

        if (extension.Equals("jpg", StringComparison.InvariantCultureIgnoreCase) || extension.Equals("gif", StringComparison.InvariantCultureIgnoreCase) || extension.Equals("png", StringComparison.InvariantCultureIgnoreCase))
        {
            System.Drawing.Size size = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("../") + "\\CMS100Data\\EntryData\\" + mCurrentTaskIdx + "\\" + newFileName).Size;
            strWidth = Convert.ToString(size.Width);
            strHeight = Convert.ToString(size.Height);
            fileType = "IMG";
        }
        else if
            (
            extension.Equals("wav", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("mp3", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("ogg", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("aac", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("3gp", StringComparison.InvariantCultureIgnoreCase)
            )
        {
            fileType = "SOU";
        }
        else if (
            extension.Equals("avi", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("mpg", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("mpeg", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("wmv", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("asf", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("asx", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("mov", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("mp4", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals("flv", StringComparison.InvariantCultureIgnoreCase)
            )
        {
            fileType = "MOV";
        }

        string serverIP = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }

        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmdn = new SqlCommand();
        Cmdn.Parameters.Add("@FileName", SqlDbType.VarChar, 255);
        Cmdn.Parameters.Add("@TaskIdx", SqlDbType.Int);
        Cmdn.Parameters.Add("@EntryIdx", SqlDbType.Int);
        Cmdn.Parameters.Add("@OriginName", SqlDbType.VarChar);
        Cmdn.Parameters.Add("@Description", SqlDbType.VarChar);
        Cmdn.Parameters.Add("@Caption", SqlDbType.VarChar, 255);
        Cmdn.Parameters.Add("@Width", SqlDbType.VarChar, 10);
        Cmdn.Parameters.Add("@Height", SqlDbType.VarChar, 10);
        Cmdn.Parameters.Add("@FileType", SqlDbType.VarChar, 10);

        Cmdn.Parameters["@FileName"].Value = newFileName;  //신규 이미지 명
        Cmdn.Parameters["@TaskIdx"].Value = mCurrentTaskIdx;
        Cmdn.Parameters["@EntryIdx"].Value = mCurrentEntryIdx;
        Cmdn.Parameters["@OriginName"].Value = originalFileName; //원 이미지파일 명
        Cmdn.Parameters["@Description"].Value = "";
        Cmdn.Parameters["@Caption"].Value = "";
        Cmdn.Parameters["@Width"].Value = strWidth;
        Cmdn.Parameters["@Height"].Value = strHeight;
        Cmdn.Parameters["@FileType"].Value = fileType;


        Cmdn.CommandText = "USP_File_INSERT";
        Cmdn.CommandType = CommandType.StoredProcedure;
        Cmdn.Connection = Conn;
        Conn.Open();
        Cmdn.ExecuteNonQuery();
        Conn.Close();
    }


}