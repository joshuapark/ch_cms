﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NestedSortableTest.aspx.cs" Inherits="main_NestedSortableTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title></title>
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/jquery.ui/jquery.ui.js"></script>
<script src="../js/jquery.nestedSortable.js"></script>
<script src="../js/jquery.simplecolorpicker.js"></script>
<script>
    $(document).ready(function () {

        $('.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: "div",
            placeholder: "editor_placeholder",
            items: "li",
            revert: 250,
            tabSize: 10,
            toleranceElement: "> div",
            maxLevels: 2,						/* 움직이는 레벨 영역 지정 */
            protectRoot: "true",
            cancel: ".editor_textarea",

            isAllowed: function (item, parent) {
                /** DRAG & DROP 허용 조건에 대해 넣어 줌: false이면 못들어감 **/

                return true;
            }
        });

        $("#toHIER").click(function (e) {

            hiered = $('ol.sortable').nestedSortable('toHierarchy', { startDepthCount: 0 });
            hiered = dump(hiered);

            d = window.open("about:blank", "_BLANK", "width=500,height=500,scrollbars=1");

            d.document.write("<pre>");
            d.document.write(hiered);

        });

        $("#addItem").click(function (e) {
            var html = '<li id="list_6" valuie="BASIC"><div class="editor_textarea">Some content</div><ol id="olMUnit"></ol></li>';
            $("#olTest").append(html);
        });

        $("#addMUnit").click(function (e) {
            var html = '<li id="list_7" valuie="BASIC"><div class="editor_textarea">MUnit content</div></li>';
            $("#olMUnit").append(html);
        });


    });

    function dump(arr, level) {
        var dumped_text = "";
        if (!level) level = 0;

        //The padding given at the beginning of the line.
        var level_padding = "";
        for (var j = 0; j < level + 1; j++) level_padding += "    ";

        if (typeof (arr) == 'object') { //Array/Hashes/Objects
            for (var item in arr) {
                var value = arr[item];

                if (typeof (value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += dump(value, level + 1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else { //Strings/Chars/Numbers etc.
            dumped_text = "===>" + arr + "<===(" + typeof (arr) + ")";
        }
        return dumped_text;
    }

</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ol class="sortable">
            <li id="list_1" value="LCLASS"><div class="editor_textarea">Some content</div></li>
            <li id="list_2" value="MCLASS"><div class="editor_textarea">Some content</div>
                <ol id="olTest">
                    <li id="list_3" value="IMAGE"><div ></div></li>
                    <li id="list_4" value="BASIC"><div class="editor_textarea">Some sub-item content</div></li>
                    <li id="list_5" valuie="QUIZ"><div class="editor_textarea">Some content</div></li>
                </ol>
            </li>
        </ol>
    </div>
    <button id="toHIER" type="button" class="btn btn-sm btn-primary">DUMP to Hierarchy</button>
    <button id="addItem" type="button" class="btn btn-sm btn-primary">Add Test</button>
    <button id="addMUnit" type="button" class="btn btn-sm btn-primary">Add MUnit</button>

    </form>
</body>
</html>
