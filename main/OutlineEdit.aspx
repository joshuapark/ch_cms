﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="OutlineEdit.aspx.cs" Inherits="main_opencontents" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>
<div class="container">
	
    <form id="addForm" runat="server">

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h3 class="title title-success">개요부 관리</h3>
		</div><!-- // title -->

    <asp:HiddenField ID="recType" runat="server" />


		<table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
		    </colgroup>
  
		    <tbody>
			    <tr>
				    <th>유형</th>
				    <td>
					    <div class="de-select">
                            <asp:DropDownList class="de-select" ID="ddlOutlineType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOutlineType_SelectedIndexChanged"></asp:DropDownList>
					    </div>
				    </td>
			    </tr>
		    </tbody>
		</table><!-- // table-a -->
        <asp:HiddenField ID="txtEntryIdx" runat="server" />
        <asp:HiddenField ID="flgExistOutline" runat="server" />
        <asp:HiddenField ID="txtOutlineType" runat="server" />
        <asp:HiddenField ID="flgChangeOutline" runat="server" />
        <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
            <asp:Repeater ID="OutlineRepeater" runat="server">
                <HeaderTemplate>
                <% if (OutlineRepeater.Items.Count == 0)
                   { %>
                    <tr>
                        <td colspan="3" style="text-align:center">등록된 항목이 없습니다.</td>
                    </tr>                    
                <%  }  %>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><asp:Label ID="lblOutlineIdx" runat="server" Text=<%#DataBinder.Eval(Container.DataItem , "Idx")%> /></td>
                        <td><asp:Label ID="lblOutlineName" runat="server" Text=<%#DataBinder.Eval(Container.DataItem , "OutlineName")%> /></td>
                        <td><asp:TextBox ID="txtOutlineData" runat="server" CssClass="large" Text=<%#DataBinder.Eval(Container.DataItem , "OutlineData")%> /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
		<div class="section-button"><!-- section-button -->
            <asp:Button ID="btnSave" cssClass="btn btn-lg btn-success" runat="server" OnClientClick="if (!confirm('저장하시겠습니까?')) return false;" OnClick="btnSave_Click" Text ="저장"></asp:Button>
		</div><!-- // section-button -->

	</div><!-- // contents -->
    
    </form>

</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>  
</body>
</html>