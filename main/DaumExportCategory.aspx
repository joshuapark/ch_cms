﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="DaumExportCategory.aspx.cs" Inherits="main_opencontents" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->

    <script type="text/javascript">
        function checkClick() {
            if ($("#chkAll").prop("checked")) {
                $("input[type=checkbox]").each(function () {
                    $(this).attr("checked", true);
                });
            } else {
                $("input[type=checkbox]").each(function () {
                    $(this).attr("checked", false);
                });
            }
        }
    </script>
</head>

<body>
<div class="container">
	
    <form id="addForm" runat="server">

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title title-success">분류 검색</h2>
		</div><!-- // title -->

    <asp:HiddenField ID="recType" runat="server" />


		<table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
		</colgroup>
  
		<tbody>
			<tr>
				<th>개정</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlRevision" runat="server"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvDdlRevision" runat="server" InitialValue="0" ErrorMessage="* 개정을 선택해주세요." ControlToValidate="ddlRevision" ForeColor="OrangeRed"></asp:RequiredFieldValidator>
					</div>
				</td>
				<th>과목</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlSubject" runat="server"></asp:DropDownList>	
                        <asp:RequiredFieldValidator ID="rfvDdlSubject" runat="server" InitialValue="0" ErrorMessage="* 과목을 선택해주세요." ControlToValidate="ddlSubject" ForeColor="OrangeRed"></asp:RequiredFieldValidator>			
					</div>
				</td>
                <th>브랜드</th>
				<td>
					<div class="de-select" id="divBrand">
                        <asp:DropDownList class="de-select" ID="ddlBrand" runat="server"></asp:DropDownList>					
                        <asp:RequiredFieldValidator ID="rfvDdlBrand" runat="server" InitialValue="0" ErrorMessage="* 브랜드를 선택해주세요." ControlToValidate="ddlBrand" ForeColor="OrangeRed"></asp:RequiredFieldValidator>			
					</div>
				</td>
            </tr>
            <tr>
                <th onclick="$('#divGrade').toggle();">학년</th>
				<td>
					<div class="de-select" id="divGrade">
                        <asp:DropDownList class="de-select" ID="ddlGrade" runat="server"></asp:DropDownList>					
                        <asp:RequiredFieldValidator ID="rfvDdlGrade" runat="server" InitialValue="0" ErrorMessage="* 학년을 선택해주세요." ControlToValidate="ddlGrade" ForeColor="OrangeRed"></asp:RequiredFieldValidator>				
					</div>			
				</td>
				<th onclick="$('#divSemester').toggle();">학기</th>
				<td>
					<div class="de-select" id="divSemester">
                        <asp:DropDownList class="de-select" ID="ddlSemester" runat="server"></asp:DropDownList>					
                        <asp:RequiredFieldValidator ID="rfvDdlSemester" runat="server" InitialValue="0" ErrorMessage="* 학기를 선택해주세요." ControlToValidate="ddlSemester" ForeColor="OrangeRed"></asp:RequiredFieldValidator>			
					</div>		
				</td>
                <th></th>
				<td><asp:HiddenField ID="txtExportIdx" runat="server"/></td>
			</tr>
		</tbody>
		</table><!-- // table-a -->

		<div class="section-button"><!-- section-button -->
			<asp:Button runat="server" ID="SearchButton" class="btn btn-lg btn-success" text="조회" OnClick="SearchButton_Click"/>
		</div><!-- // section-button -->
     
        <asp:Label ID="lblTotalCnt" runat="server" />
        <div id="entryList" style="height:500px; overflow-y: scroll;">
		    <table border="0" class="table table-list" style="padding:0; border-spacing:0">
                <asp:Repeater ID="EntryList" runat="server" >
                    <HeaderTemplate>
                        <tr>
				            <th><input type="checkbox" id="chkAll" onclick="javascript:checkClick();" /></th>
				            <th>개정</th>
				            <th>과목</th>
				            <th>브랜드</th>
				            <th>학년</th>
				            <th>학기</th>
				            <th>대단원</th>
				            <th>중단원</th>
				            <th>타이틀</th>
				            <th>엔트리No</th>
                        </tr>
    <%
        if (EntryList.Items.Count == 0)
        {
    %>
                        <tr>
                            <td colspan="10" style="text-align:center">등록된 엔트리가 없습니다.</td>
                        </tr>                    
    <%
        }
    %>
                    </HeaderTemplate>
                    <ItemTemplate>

                        <tr>
                            <td class="singleCheckbox">
                                <asp:CheckBox ID="chkID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem , "Idx")%>'  />
                            </td>
                            <td>
                                <asp:Label ID="lblRevision" runat="server" Text='<%#DataBinder.Eval(Container.DataItem , "Revision")%>' />
                            </td>
                            <td>
                                <asp:Label ID="lblSubject" runat="server" Text='<%#DataBinder.Eval(Container.DataItem , "Subject")%>' />
                            </td>
                            <td>
                                <asp:Label ID="lblBrand" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem , "Brand")%>' />
                            </td>
                            <td>
                                <asp:Label ID="lblGrade" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem , "Grade")%>' />
                            </td>
                            <td>
                                <asp:Label ID="lblSemester" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem , "Semester")%>' />
                            </td>
                            <td>
                                <asp:Label ID="lblLUnit" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem , "LUnit")%>' />
                            </td>
                            <td>
                                <asp:Label ID="lblMUnit" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem , "MUnit")%>' />
                            </td>
                            <td>
                                <%--<asp:HyperLink ID="lblTitle" runat="server" />--%>
                                <asp:Label ID="lblTitle" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem , "Title")%>' />
                            </td>
                            <td>
                                <asp:HiddenField ID="txtIdx" Value=<%#DataBinder.Eval(Container.DataItem , "Idx")%> runat="server" />
                                <asp:Label ID="lblEntryNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem , "Idx")%>'/>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
		<div class="section-button"><!-- section-button -->
			<asp:Button ID="btnExport" cssClass="btn btn-lg btn-success" runat="server" OnClick="btnExport_Click" Text="확인"></asp:Button>
		</div><!-- // section-button -->

	</div><!-- // contents -->
    
    </form>

</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>  
</body>
</html>