﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NetSort.aspx.cs" Inherits="main_NetSort" %>




<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link href="../css/import.css" rel="stylesheet">
	<link href="../css/editor.css" rel="stylesheet">
	<link href="../css/jquery-ui.css" rel="stylesheet">
	<link href="../css/jquery.simplecolorpicker.css" rel="stylesheet">
    
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/ckeditor/ckeditor.js"></script>
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/jquery.ui/jquery.ui.js"></script>
<script src="../js/jquery.nestedSortable.js"></script>
<script src="../js/jquery.simplecolorpicker.js"></script>

<script>
/** jquery.nestedSortable.js 내의 소스 참고 **/

    $(document).ready(function(){

        $(".editor").nestedSortable({
            forcePlaceholderSize: true,
			handle: "div",
            placeholder:"editor_placeholder",
			items: "li",
			revert: 250,
			tabSize: 10,
            toleranceElement: "> div",
			maxLevels:2,						/* 움직이는 레벨 영역 지정 */
			protectRoot:"true",
			cancel: ".editor_textarea",
			
			isAllowed: function(item,parent){
				/** DRAG & DROP 허용 조건에 대해 넣어 줌: false이면 못들어감 **/
				
				return true;
			}
        });

		$('#toHIER').click(function(e){
			
			//컨텐츠를 어레이로 뽑아 냅니다. --> jquery.nestedSortable.js 안의 toHierarchy 참고
			
			hiered = $('ol.editor').nestedSortable('toHierarchy', {startDepthCount: 0});
			hiered = dump(hiered);
			
			d=window.open("about:blank","_BLANK","width=500,height=500,scrollbars=1");
			
			d.document.write("<pre>");
			d.document.write(hiered);
		})

		$('#toArr').click(function(e){
			arraied = $('ol.editor').nestedSortable('toArray', {startDepthCount: 0});
			arraied = dump(arraied);
			
			d=window.open("about:blank","_BLANK","width=500,height=500,scrollbars=1");
			
			d.document.write("<pre>");
			d.document.write(arraied);
		})
		
		//마크업 내에 
		// 컬러 피커 구동 ../js/jquery.simplecolorpicker.js
		$('select[name="colorpicker"]').simplecolorpicker({ picker: true });

    });

	
	function dump(arr,level) {
		var dumped_text = "";
		if(!level) level = 0;

		//The padding given at the beginning of the line.
		var level_padding = "";
		for(var j=0;j<level+1;j++) level_padding += "    ";

		if(typeof(arr) == 'object') { //Array/Hashes/Objects
			for(var item in arr) {
				var value = arr[item];

				if(typeof(value) == 'object') { //If it is an array,
					dumped_text += level_padding + "'" + item + "' ...\n";
					dumped_text += dump(value,level+1);
				} else {
					dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
				}
			}
		} else { //Strings/Chars/Numbers etc.
			dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
		}
		return dumped_text;
	}	
</script>
<body>

<div class="container">

	<div id="contents"><!-- contents -->

        <div class="panel panel-gray"><!-- panel -->
            
            <div class="panel-heading">

				<span class="glyphicon glyphicon-chevron-up btn-head"></span>			
				
				<span class="panel-title editor_head_title">대분류<span class="badge">1</span></span>
				
				<a href="#!" class="btn-close"><!-- close --></a>
				
				<span class="editor_head_container">
				<button type="button" class="btn btn-sm btn-primary">대분류+</button>
				<button type="button" class="btn btn-sm btn-primary">불러오기</button>  
				</span>
            </div>

            
                         
        
            <div class="panel-body">

                    <ol class='editor vertical'>
                    <li id="list_0" value="INDEXCONTENT">
                    <div>
				        <span class="glyphicon glyphicon-move">분류</span> 
				        <a href="#!" class="btn-close" style='float:right'></a>
				        <span class="editor_control_container">
				            <span class="label label-info editor_controls" >대제목 </span>
				            <span class="label label-info editor_controls" >중제목 </span>
				            <span class="label label-info editor_controls" >기본1 </span>
				            <span class="label label-info editor_controls" >기본2 </span>
				            <span class="label label-info editor_controls" >블럭 </span>
				            <span class="label label-info editor_controls" >표 </span>
				            <span class="label label-info editor_controls" >이미지 </span>
				            <span class="label label-info editor_controls" >주석 </span>			  
				        </span>
                    </div>
                    <ol>
            
        
                
                            <li id="list_1" value="MAINTITLE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 대제목</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p>열팽창</p></div>
                            </li>
         
                
                            <li id="list_2" value="SUBTITLE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 중제목</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p>1. 열팽창</p></div>
                            </li>
         
                
                            <li id="list_3" value="BASIC" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 기본</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p style="text-align:left;">물체가 열을 받아 물체의 길이나 부피가 늘어나는 현상</p></div>
                            </li>
         
                
                            <li id="list_4" value="SUBTITLE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 중제목</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p>2. 열팽창이 일어나는 이유</p></div>
                            </li>
         
                
                            <li id="list_5" value="BASIC" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 기본</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p style="text-align:left;">열을 받으면 물질을 이루는 분자의 운동이 활발해지면서 분자 사이의 거리가 멀어진다.</p></div>
                            </li>
         
                
                            <li id="list_6" value="IMAGE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 이미지</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><img src="http://hyonga.iptime.org:15080/UploadFile/EntryImage/C-E-D2W-72-0503-00002-01.png" width="650" height="" style="text-align:left;" /><caption>
	
</caption></div>
                            </li>
         

                    </ol>     
                    </li>
            </div><!-- panel body -->
        
            <div class="panel-body">

                    <li id="list_7" value="INDEXCONTENT">
                    <div>
				        <span class="glyphicon glyphicon-move">분류</span> 
				        <a href="#!" class="btn-close" style='float:right'></a>
				        <span class="editor_control_container">
				            <span class="label label-info editor_controls" >대제목 </span>
				            <span class="label label-info editor_controls" >중제목 </span>
				            <span class="label label-info editor_controls" >기본1 </span>
				            <span class="label label-info editor_controls" >기본2 </span>
				            <span class="label label-info editor_controls" >블럭 </span>
				            <span class="label label-info editor_controls" >표 </span>
				            <span class="label label-info editor_controls" >이미지 </span>
				            <span class="label label-info editor_controls" >주석 </span>			  
				        </span>
                    </div>
                    <ol>
            
        
                
                            <li id="list_8" value="MAINTITLE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 대제목</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p>고체와 액체의 열팽창</p></div>
                            </li>
         
                
                            <li id="list_9" value="SUBTITLE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 중제목</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p>1. 열팽창 하는 정도</p></div>
                            </li>
         
                
                            <li id="list_10" value="BASIC" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 기본</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p style="text-align:left;">고체＜액체＜기체</p></div>
                            </li>
         
                
                            <li id="list_11" value="BASIC" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 기본</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p style="text-align:left;"><EMPHASIS>*</EMPHASIS> 액체와 고체는 물질의 종류에 따라 열팽창 정도가 다르다.</p></div>
                            </li>
         
                
                            <li id="list_12" value="BASIC" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 기본</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p style="text-align:left;"><EMPHASIS>*</EMPHASIS> 물체의 온도가 많이 변할수록, 물체의 길이나 부피가 클수록 길이와 부피가 많이 변한다.</p></div>
                            </li>
         

                    </ol>     
                    </li>
            </div><!-- panel body -->
        
            <div class="panel-body">

                    <li id="list_13" value="INDEXCONTENT">
                    <div>
				        <span class="glyphicon glyphicon-move">분류</span> 
				        <a href="#!" class="btn-close" style='float:right'></a>
				        <span class="editor_control_container">
				            <span class="label label-info editor_controls" >대제목 </span>
				            <span class="label label-info editor_controls" >중제목 </span>
				            <span class="label label-info editor_controls" >기본1 </span>
				            <span class="label label-info editor_controls" >기본2 </span>
				            <span class="label label-info editor_controls" >블럭 </span>
				            <span class="label label-info editor_controls" >표 </span>
				            <span class="label label-info editor_controls" >이미지 </span>
				            <span class="label label-info editor_controls" >주석 </span>			  
				        </span>
                    </div>
                    <ol>
            
        
                
                            <li id="list_14" value="MAINTITLE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 대제목</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><p>열팽창과 우리 생활</p></div>
                            </li>
         
                
                            <li id="list_15" value="IMAGE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 이미지</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><img src="http://hyonga.iptime.org:15080/UploadFile/EntryImage/C-E-D2W-72-0503-00002-02.png" width="660" height="" style="text-align:left;" /><caption>
	
</caption></div>
                            </li>
         
                
                            <li id="list_16" value="MATRIX_TABLE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 표</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><table style="width:680px; margin-top:5px; margin-bottom:5px; margin-Left:20px;  border:1px solid #e4e4e4; "><thead></thead><tbody>

	<tr>
		<td style="padding:10px; font-size:10pt; line-height: 22px; border:1px solid #e4e4e4; background-color:#ffffdd; font-weight:bold; text-align:center; ">고체의 열팽창</td>
	</tr>
	<tr>
		<td style="padding:10px; font-size:10pt; line-height: 22px; border:1px solid #e4e4e4;  background-color:#FFFFFF;  text-align:left;  "><EMPHASIS>*</EMPHASIS> 기차 선로와 다리 이음매에 틈을 두어 여름에 선로가 팽창하여 휘는 것을 방지한다.<br/><EMPHASIS>*</EMPHASIS> 송유관과 가스관을 ㄷ자 모양으로 연결하여 열팽창에 의한 사고를 막는다.<br/><EMPHASIS>*</EMPHASIS> 포개진 그릇을 뺄 때 안쪽 그릇에는 찬물을 넣고, 바깥쪽 그릇은 더운물에 담근다.<br/><EMPHASIS>*</EMPHASIS> 유리병의 금속 뚜껑이 열리지 않을 때 입구에 뜨거운 물을 붓는다.</td>
	</tr>
			</tbody></table></div>
                            </li>
         
                
                            <li id="list_17" value="MATRIX_TABLE" >
                                <div>
				                    <span class="glyphicon glyphicon-move"> 표</span>
				
				                    <a href="#!" class="btn-close" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea" contenteditable="true"><table style="width:680px; margin-top:5px; margin-bottom:5px; margin-Left:20px;  border:1px solid #e4e4e4; "><thead></thead><tbody>

	<tr>
		<td style="padding:10px; font-size:10pt; line-height: 22px; border:1px solid #e4e4e4; background-color:#ffffdd; font-weight:bold; text-align:center; ">액체의 열팽창</td>
	</tr>
	<tr>
		<td style="padding:10px; font-size:10pt; line-height: 22px; border:1px solid #e4e4e4;  background-color:#FFFFFF;  text-align:left;  "><EMPHASIS>*</EMPHASIS> 온도계 : 온도계 속의 액체가 열을 받으면 부피가 증가해 눈금이 올라간다.<br/><EMPHASIS>*</EMPHASIS> 음료수 병에 액체를 담을 때 공간을 두어 병이 터지는 것을 막는다. → 액체가 열팽창 하면 부피가 늘어나므로<br/><EMPHASIS>*</EMPHASIS> 주유는 밤에 한다. →  온도가 낮은 밤에는 부피가 줄어들기 때문에 같은 부피라도 더 많은 질량의 연료가 주유되므로 이득이다.</td>
	</tr>
			</tbody></table></div>
                            </li>
         

                    </ol>     
                    </li>
            </div><!-- panel body -->
        
          
             </ol>  
        </div><!-- panel --> 
		 
	  
		<div class="section-button"><!-- section-button -->
			<div class="pull-center">
				<button id="toArr" type="button" class="btn btn-sm btn-primary">DUMP to Array</button>
				<button id="toHIER" type="button" class="btn btn-sm btn-primary">DUMP to Hierarchy</button>
			</div>
		</div>
		
    </div><!-- Contents -->

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>

<div id="markup_dialog" title="Basic dialog">
</div>



</div><!-- // container -->

</body>
</html>

