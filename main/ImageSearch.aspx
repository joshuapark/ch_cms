﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImageSearch.aspx.cs" Inherits="main_TemplateAdd" validateRequest="false" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>       <html class="no-js"> <![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../js/jquery-2.1.3.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.jqtransform.js"></script>
    <script src="../js/ui.js"></script>
    <script src="../js/jquery.js"></script>
    <script type="text/javascript">
        function EntSum(){
            var TCnt = document.getElementById('selTEntryCnt').value; 
            var QCnt = document.getElementById('selQEntryCnt').value; 
            var CCnt = document.getElementById('selCEntryCnt').value; 
            document.getElementById('sumEntry').value = TCnt + QCnt + CCnt;
        }
    </script>
</head>

<body>
<div class="container">
  
    <form id="addForm" runat="server">
    <div id="contents">
		<div class="title"><!-- title -->
			<h3 class="title title-success">이미지검색</h3>
		</div><!-- // title -->
		<div class="row">
            <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		        <tbody>
			        <tr>
                        <td><asp:TextBox ID="txtKeyword" runat="server" CssClass="large"></asp:TextBox></td>
                       <td><asp:Button runat="server" ID="SearchButton" class="btn btn-sm btn-success" text="검색" OnClick="SearchButton_Click"/></td>
                    </tr>
		    </tbody>
		    </table><!-- // table-a -->
        </div>

        <br />       

        <table border="0" cellpadding="0" cellspacing="0" class="table">
            <asp:Repeater ID="rpRelationList" runat="server" OnItemCommand="rpRelationList_ItemCommand" >
                <HeaderTemplate>
<%
    if (rpRelationList.Items.Count == 0)
    {
%>
                    <tr>
                        <td style="text-align:center">검색된 이미지가 없습니다.</td>
                    </tr>                    
<%
    }
%>
                    <table class="table-bordered" >
                        <tr>
                    
                </HeaderTemplate>
                    <ItemTemplate>
                        <%# (Container.ItemIndex != 0 && Container.ItemIndex % 10 == 0) ? @"</tr><tr><td span=2 height=1px ></td></tr><tr>" : string.Empty  %>
                        <td class="btn-default" style="padding:5px 5px;margin:2px 2px;width:10%">
                            <asp:HiddenField ID="hfBinderRelationIdx" Value='<%#DataBinder.Eval(Container.DataItem , "Idx")%>'  runat="server" />
                            <a onclick="location.href='BinderEdit.aspx?idx=<%#DataBinder.Eval(Container.DataItem , "RelationBinderIdx")%>'">
                                <asp:Label ID="lblBinderName" Text='<%#DataBinder.Eval(Container.DataItem , "BinderTitle")%>'  runat="server" />
                            </a>
                            <asp:Button runat="server" cssClass="btn btn-sm btn-close" CausesValidation="false" />
                        </td>
                    </ItemTemplate>
                <FooterTemplate></tr></table></FooterTemplate>
            </asp:Repeater>
		</table><!-- // table-a -->
        <br /><br />
	</div><!-- // contents -->

    </form>

</div>
</body>
</html>
