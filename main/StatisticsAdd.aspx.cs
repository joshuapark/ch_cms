﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.IO;

public partial class main_opencontents : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "1000";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    private string strCategory = string.Empty;
    private string strType = string.Empty;
    public string strTitle = "대분류";
    private string strEntryIdx = string.Empty;
    private string exportIdx = string.Empty;

    private DataTable categoryTable = new DataTable();
    private DataTable entryTable = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝  
        if (!IsPostBack)
        {
            //BindType();
            BindCheckBox();
        }
        else
        {

        }
    }


    private void BindCheckBox()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, CodeType, Code, CodeName FROM TCode WHERE DelFlag=0 ORDER BY Idx ASC", Conn);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TCode");
        //Set up the data binding. 
        Conn.Close();
        DataTable dt = ds.Tables["TCode"];
        dt.DefaultView.RowFilter = "CodeType = 1";
        cblRevision.DataSource = dt;
        cblRevision.DataTextField = "CodeName";
        cblRevision.DataValueField = "Idx";
        cblRevision.CssClass = "de-radio";
        cblRevision.DataBind();

        dt.DefaultView.RowFilter = "CodeType = 3";
        cblSubject.DataSource = dt;
        cblSubject.DataTextField = "CodeName";
        cblSubject.DataValueField = "Idx";
        cblSubject.CssClass = "de-radio";
        cblSubject.DataBind();

        dt.DefaultView.RowFilter = "CodeType = 5";
        cblBrand.DataSource = dt;
        cblBrand.DataTextField = "CodeName";
        cblBrand.DataValueField = "Idx";
        cblBrand.CssClass = "de-radio";
        cblBrand.DataBind();

        //Close the connection.
        dt = null;
        sda = null;
    }
        
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string strQuery = string.Empty;
        string strSelectEntryIdx = string.Empty;
        string strSelectEntryTitle = string.Empty;
        int totalEntCount = 0;


        string excelFileName = "Statistics_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xls";
        string SubjectItems = string.Empty;
        string BrandItems = string.Empty;
        int seqNo = 1;
        foreach (ListItem i in cblBrand.Items)
        {
            if (i.Selected == true)
            {

                foreach (ListItem j in cblSubject.Items)
                {
                    if (j.Selected == true)
                    {
                        SubjectItems += j.Value + ",";

                        for (int k = 103; k < 119;k++ )
                        {

                            for (int l = 119; l < 122; l++ )
                            {
                                string StatusItems = "(";
                                foreach (ListItem m in cblStatus.Items)
                                {
                                    if (m.Selected == true)
                                    {
                                        StatusItems += "Status = "+ m.Value + " or ";
                                        if ( m.Value=="3")
                                        {
                                            StatusItems += "Status = 5 or ";
                                        }
                                    }
                                }
                                StatusItems = StatusItems.Substring(0, (StatusItems.Length - 3));
                                StatusItems += ")";

                                string RevisionItems = "(";
                                foreach (ListItem n in cblRevision.Items)
                                {
                                    if (n.Selected == true)
                                    {
                                        RevisionItems += "RevisionIdx = "+ n.Value + " or ";
                                    }
                                }
                                RevisionItems = RevisionItems.Substring(0, (RevisionItems.Length - 3));
                                RevisionItems += ")";

                                DBFileInfo();

                                strQuery = "SELECT count(Idx) as Sum FROM TEntry WHERE " + StatusItems + " AND " + RevisionItems 
                                    + " AND DelFlag=0 AND BrandIdx=" + i.Value + " AND SubjectIdx=" + j.Value + " "
                                    + " AND GradeIdx=" + k + " AND SemesterIdx=" + l +" ";
                                SqlConnection Con = new SqlConnection(connectionString);
                                SqlCommand Cmd = new SqlCommand(strQuery, Con);
                                Con.Open();
                                SqlDataReader reader = Cmd.ExecuteReader();
                                string sumData = string.Empty;
                                if (reader.Read())
                                {
                                    sumData = reader["Sum"].ToString();
                                    if (Con.State == ConnectionState.Open)
                                        Con.Close();
                                }            

                                strQuery = "INSERT INTO TStatTemp (number, brandIdx, subjectIdx, gradeIdx, semesterIdx, sumCnt, title, fileName) "
                                    + " VALUES (" + seqNo + "," + i.Value + "," + j.Value + ", " + k + "," + l + "," + sumData + ", '" + txtTitle.Text + "', '" + excelFileName  + "')";
                                Con = new SqlConnection(connectionString);
                                Cmd = new SqlCommand(strQuery, Con);
                                Con.Open();
                                Cmd.ExecuteNonQuery();
                                seqNo++;
                                if (Con.State == ConnectionState.Open)
                                    Con.Close();
                                Cmd = null;
                                Con = null;

                            }
                        }
                    }
                }
            }
        }

        DataTable dt = executeQueryText(excelFileName);
        
        this.exportExcel(dt, excelFileName);
        //this.saveExcel(dt, excelFileName);
        DBFileInfo();
        strQuery = "INSERT INTO TStatistics (Title, FileName, InsertDate) "
            + " VALUES ('" + txtTitle.Text + "', '" + excelFileName + "', getdate())";
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmdd = new SqlCommand(strQuery, Conn);
        Conn.Open();
        Cmdd.ExecuteNonQuery();
        
        if (Conn.State == ConnectionState.Open)
            Conn.Close();
        Cmdd = null;
        Conn = null;

        Response.Write("<script>opener.location.href='Statistics.aspx';window.opener=self;self.close();</script>");
    }

    private DataTable executeQueryText(string FileName)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Con.Open();
        Cmd.CommandText = "USP_Statistics_SELECT '" + FileName + "'";
        Cmd.CommandType = CommandType.Text;

        SqlDataReader dataReader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        DataTable dataTable = new DataTable();
        dataTable.Load(dataReader);

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        return dataTable;
    }
    private void exportExcel(DataTable dataTable, String excelFileName)
    {
        //string attachment = "attachment; filename=" + excelFileName; 
        //Response.ClearContent();
        //Response.AddHeader("content-disposition", attachment);
        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //Response.ContentEncoding = System.Text.Encoding.Default;
        string tab = "";
        StringBuilder sb = new StringBuilder();
        foreach (DataColumn dc in dataTable.Columns)
        {
            sb.Append(tab + dc.ColumnName);
            //Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        sb.Append("\n");
        //Response.Write("\n");

        int i;
        foreach (DataRow dr in dataTable.Rows)
        {
            tab = "";
            for (i = 0; i < dataTable.Columns.Count; i++)
            {
                sb.Append(tab + dr[i].ToString().TrimEnd());
                //Response.Write(tab + dr[i].ToString().TrimEnd());
                tab = "\t";
            }
            sb.Append("\n");
            //Response.Write("\n");
        }
        File.WriteAllBytes(Server.MapPath("~/DATA/" + excelFileName), Encoding.Default.GetBytes(sb.ToString()));
        //Response.Write(sb.ToString());
        //Response.End();
    }
    private void saveExcel(DataTable dataTable, String excelFileName)
    {

        string pathData = HttpContext.Current.Server.MapPath("~/DATA/");
        StreamWriter wr = new StreamWriter(pathData + excelFileName);
                try
                {

                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        wr.Write(dataTable.Columns[i].ToString().ToUpper() + "\t");
                    }

                    wr.WriteLine();

                    //write rows to excel file
                    for (int i = 0; i < (dataTable.Rows.Count); i++)
                    {
                        for (int j = 0; j < dataTable.Columns.Count; j++)
                        {
                            if (dataTable.Rows[i][j] != null)
                            {
                                wr.Write(Convert.ToString(dataTable.Rows[i][j]) + "\t");
                            }
                            else
                            {
                                wr.Write("\t");
                            }
                        }
                        //go to next line
                        wr.WriteLine();
                    }
                    //close file
                    wr.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
    }
}
