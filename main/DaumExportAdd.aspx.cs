﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Net;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.IO.Compression;

public partial class main_TemplateAdd : System.Web.UI.Page
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //

    private const int INITIAL_RECTYPE_ID = 1;
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;
    private DataTable dtRecType = new DataTable();
    private DataTable dtTemplate = new DataTable();
    private string strQuery = string.Empty;
    public string strTaskIdx = string.Empty;
    public string exportIdx = string.Empty;
    // Set a variable to the /Data File path. 
    private string pathData = HttpContext.Current.Server.MapPath("~/DATA/");
    private string preRecType = string.Empty;
    private int newEntryID = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }   
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝  
        if (!IsPostBack)
        {
            // 1. exportIDX 받기
            if (Request.Params["exportIdx"] != null)
            {
                exportIdx = Request.Params["exportIdx"].ToString();
                txtExportIdx.Value = exportIdx;
            }

            // 1.1 IDX 없으면 ExportIdx생성
            if (exportIdx.Length == 0)
            {
                // ExportIdx 생성
                string strTimestamp = System.DateTime.Now.ToString("yyyyMMddhhmmss");
                DBFileInfo();
                strQuery = "INSERT INTO TDaumExport (TempKey) Values ('" + strTimestamp + "')";
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                //Con.Close();

                strQuery = "SELECT TOP 1 Idx FROM TDaumExport WHERE TempKey ='" + strTimestamp + "' ORDER BY Idx DESC";
                //Con = new SqlConnection(connectionString);
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Cmd.Connection = Con;
                //Con.Open();
                SqlDataReader reader;
                reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    exportIdx = reader["Idx"].ToString();
                    txtExportIdx.Value = exportIdx;
                }
                Con.Close();

                Cmd = null;
                Con = null;
            }
            else  // 1.2 exportIdx를 param으로 받았으면 DB에서 값 가져오기
            {
                // 정보를 가져온다.
                GetExportInfo();
                // 목록을 가져온다.
                Listing();
            }

        }
    }
    protected void Listing() 
    {
        DBFileInfo();
        exportIdx = txtExportIdx.Value;
        strQuery = "Declare @sumCnt int; SELECT @sumCnt=SUM(EntryCount) FROM TDaumExportData WHERE ExportIdx =" + exportIdx + " ;SELECT *, @sumCnt as totalCount FROM TDaumExportData WHERE ExportIdx =" + exportIdx + " ORDER BY Idx DESC";
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Cmd.Connection = Con;
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        EntryList.DataSource = ds;
        EntryList.DataBind();
        if (ds.Tables["data_list"].Rows.Count > 0)
        {
            txtSumEntry.Text = ds.Tables["data_list"].Rows[0]["totalCount"].ToString();
        }
        Con.Close();

        Cmd = null;
        Con = null;
    }
    private void GetExportInfo()
    {
        DBFileInfo();
        exportIdx = txtExportIdx.Value; 
        
        strQuery = "SELECT * FROM TDaumExport WHERE Idx =" + exportIdx + "";
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Cmd.Connection = Con;
        Con.Open();
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        if (reader.Read())
        {
            txtTitle.Text = reader["ExportTitle"].ToString();
            txtBookID.Text = reader["BookID"].ToString();
            txtCategoryID.Text = reader["CategoryID"].ToString();
            txtEntryID.Text = reader["NewEntryID"].ToString();
        }
        reader.Close();
        Con.Close();

        Cmd = null;
        Con = null;
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT * FROM TDaumExportData WHERE ExportIdx =" + exportIdx + "";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        //Con.Close();

        string strNewEntryID = txtEntryID.Text;
        
        if (strNewEntryID.Length > 0)
            newEntryID = Int32.Parse(strNewEntryID);

        if (dt.Rows.Count == 0)
        {
        }
        else
        {
            strQuery = "DELETE FROM TDaumCategory WHERE ExportIdx="+ exportIdx +"";
            Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            //Con.Open();
            Cmd.ExecuteNonQuery();
            
            StringBuilder sb = new StringBuilder();
            string strXML = string.Empty;
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.AppendLine("<encyclopedia xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://100.daum.net/resource/xsd/entry.xsd\">");
            
            foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다 시작
            {
                switch (row["Type"].ToString())
                {
                    case "TASK":
                        //sb.Append(sbXMLByTask(row["Content"].ToString()));
                        continue;
                    case "Category":
                        sb.Append(sbXMLByCategory(row["Content"].ToString()));
                        continue;
                    case "Binder":
                        //sb.Append(sbXMLByBinder(row["Content"].ToString()));
                        continue;
                }
            }
            sb.AppendLine("</encyclopedia>");

            // XML Document에 담는다.
            //XmlDocument xd = new XmlDocument();
            //xd.LoadXml(sb.ToString());

            // Write the stream contents to a new file named 
            DateTime dtTime = DateTime.Now;
            string FileNewName = "DAUM_XML_Export_" + dtTime.ToString("yyyyMMddHHmmss") + ".xml";
            using (StreamWriter outfile = new StreamWriter(pathData + @FileNewName))
            {
                //outfile.Write(xd.OuterXml);
                outfile.Write(sb.ToString());
            }

            try
            {
                lblXMLFile.Text = "<a target='_blank' href=\"DownloadFile.aspx?dfn=" + @FileNewName + "&Type=X\">"+ @FileNewName + "</a>";

                //lblImageFile.Text = strImageLink;
                lblFirstDate.Text = dtTime.ToString("yyyy-MM-dd");
                int sumCnt = 0;
                sumCnt = Convert.ToInt32(txtSumEntry.Text);//Convert.ToInt32(Request.Form[txtSumEntry.UniqueID].ToString());
                //Con = new SqlConnection(connectionString);
                strQuery = "UPDATE TDaumExport SET ExportTitle='" + txtTitle.Text + "', NewEntryID="+ txtEntryID.Text +", "
                    + " BookID ='" + txtBookID.Text + "', CategoryID='" + txtCategoryID.Text +"', EntryCount ="+sumCnt +", "
                    + " XMLFile ='" + FileNewName + "' , FirstDate=getdate(), "
                    + " SaveFlag=1  WHERE Idx=" + exportIdx + "";

                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                //Con.Open();
                Cmd.ExecuteNonQuery();
                //Con.Close();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : " + ex.ToString();
            }
            finally
            {

            }
        }
        makeImageFile();
        makeMultimediaXML();
        makeCategoryXML();
        btnExport.Visible = false;
        btnImageChk.Visible = false;
        sectionBtn.Visible = false;
        txtBookID.Enabled = false;
        txtCategoryID.Enabled = false;
        txtTitle.Enabled = false;
        //EntryList도 Enabled = false 해야 함 안그러면 삭제됨.
        Con.Close();
    }

    protected void makeCategoryXML()
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT ExportIdx, C.LUnitIdx, C.MUnitIdx, L.LUnitName, M.MUnitName, C.EntryTitle, NewEntryID FROM TDaumCategory C join TLUnit L on C.LUnitIdx=L.Idx "
            + "	join TMUnit M on C.MUnitIdx=M.Idx join TEntry E on C.EntryIdx=E.Idx WHERE C.ExportIdx="+ exportIdx + " "
            + " Order By C.LUnitIdx asc, C.MUnitIdx asc, E.SortNo asc";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();
        if (dt.Rows.Count == 0)
        {
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            string strXML = string.Empty;
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.AppendLine("<encyclopedia>"); 
            sb.AppendLine("<book id=\""+ txtBookID.Text +"\">"); 
            sb.AppendLine("<toc>"); 

            string preLUnitIdx = string.Empty;
            string preMUnitIdx = string.Empty;
            string itemDepth1 = "<item depth=\"1\">";
            string itemDepth2 = "<item depth=\"2\">";
            foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다 시작
            {
                if(preLUnitIdx==row["LUnitIdx"].ToString())                
                {
                }
                else
                {
                    sb.AppendLine(itemDepth1);
                    sb.AppendLine("<title><![CDATA["+row["LUnitName"].ToString()+"]]></title>");
                    sb.AppendLine("</item>");
                }
                if (preMUnitIdx == row["MUnitIdx"].ToString())
                {
                }
                else
                {
                    sb.AppendLine(itemDepth2);
                    sb.AppendLine("<title><![CDATA[" + row["MUnitName"].ToString() + "]]></title>");
                    sb.AppendLine("</item>");
                }

                sb.AppendLine("<item entryid=\"" + row["NewEntryID"].ToString() + "\" depth=\"3\">");
                sb.AppendLine("<title><![CDATA["+row["EntryTitle"].ToString()+"]]></title>");
                sb.AppendLine("</item>");
      
                preLUnitIdx= row["LUnitIdx"].ToString();
                preMUnitIdx= row["MUnitIdx"].ToString();
            }
            sb.AppendLine("</toc>");
            sb.AppendLine("</book>");
            sb.AppendLine("</encyclopedia>"); 
            // Write the stream contents to a new file named 
            DateTime dtTime = DateTime.Now;
            string FileNewName = "DAUM_CategoryList_Export_" + dtTime.ToString("yyyyMMddHHmmss") + ".xml";
            using (StreamWriter outfile = new StreamWriter(pathData + @FileNewName))
            {
                //outfile.Write(xd.OuterXml);
                outfile.Write(sb.ToString());
            }
            try
            {
                lblXMLFile.Text += "<br /><a target='_blank' href=\"DownloadFile.aspx?dfn=" + @FileNewName + "&Type=X\">" + @FileNewName + "</a>";

                //lblImageFile.Text = strImageLink;
                lblFirstDate.Text = dtTime.ToString("yyyy-MM-dd");
                int sumCnt = 0;
                sumCnt = Convert.ToInt32(txtSumEntry.Text);//Convert.ToInt32(Request.Form[txtSumEntry.UniqueID].ToString());
                Con = new SqlConnection(connectionString);
                strQuery = "UPDATE TDaumExport SET CategoryXML ='" + FileNewName + "' WHERE Idx=" + exportIdx + "";

                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : " + ex.ToString();
            }
        }
    }

    protected void makeMultimediaXML()
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT * FROM TDaumExportData WHERE ExportIdx =" + exportIdx + "";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        //Con.Close();
        if (dt.Rows.Count == 0)
        {
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            string strXML = string.Empty;
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.AppendLine("<encyclopedia>");

            foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다 시작
            {
                switch (row["Type"].ToString())
                {
                    case "TASK":
                        //sb.Append(sbXMLByTask(row["Content"].ToString()));
                        continue;
                    case "Category":
                        sb.Append(sbMultimediaListByCategory(row["Content"].ToString()));
                        continue;
                    case "Binder":
                        //sb.Append(sbXMLByBinder(row["Content"].ToString()));
                        continue;
                }
            }
            sb.AppendLine("</encyclopedia>");

            // XML Document에 담는다.
            //XmlDocument xd = new XmlDocument();
            //xd.LoadXml(sb.ToString());

            // Write the stream contents to a new file named 
            DateTime dtTime = DateTime.Now;
            string FileNewName = "DAUM_MultimediaList_Export_" + dtTime.ToString("yyyyMMddHHmmss") + ".xml";
            using (StreamWriter outfile = new StreamWriter(pathData + @FileNewName))
            {
                //outfile.Write(xd.OuterXml);
                outfile.Write(sb.ToString());
            }
            try
            {
                lblXMLFile.Text += "<br /><a target='_blank' href=\"DownloadFile.aspx?dfn=" + @FileNewName + "&Type=X\">" + @FileNewName + "</a>";

                //lblImageFile.Text = strImageLink;
                lblFirstDate.Text = dtTime.ToString("yyyy-MM-dd");
                int sumCnt = 0;
                sumCnt = Convert.ToInt32(txtSumEntry.Text);//Convert.ToInt32(Request.Form[txtSumEntry.UniqueID].ToString());
                Con = new SqlConnection(connectionString);
                strQuery = "UPDATE TDaumExport SET MultimediaXML ='" + FileNewName + "' WHERE Idx=" + exportIdx + "";

                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : " + ex.ToString();
            }
        }
    }

    protected StringBuilder sbMultimediaListByCategory(string tmpEntryID)
    {

        StringBuilder sbCategory = new StringBuilder();
        string strEntryIdx = string.Empty;
        string[] arrEntryID = tmpEntryID.Split(',');

        foreach (string strID in arrEntryID.Reverse<string>())
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            strQuery = "SELECT * FROM TFileData WHERE EntryIdx=" + strID + " ORDER BY Idx ASC";
            Cmd.CommandType = CommandType.Text;
            Cmd.CommandText = strQuery;
            Con.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
            adapter.Fill(dt);
            Con.Close();
            //totalEntCount += dt.Rows.Count;
            foreach (DataRow row in dt.Rows)
            {
                strEntryIdx = row["Idx"].ToString();
                string fileType = row["FileType"].ToString();

                sbCategory.AppendLine();
                if (fileType == "IMG")
                {
                    sbCategory.AppendLine("<multimedia type=\"image\" id=\"" + row["FileName"].ToString() + "\">");
                    sbCategory.AppendLine("<url><![CDATA[" + row["FileName"].ToString() + "]]></url>");
                    if (row["Caption"].ToString().Length > 1)
                        sbCategory.AppendLine("<title><![CDATA[" + row["Caption"].ToString() + "]]></title>");
                    if (row["Description"].ToString().Length > 1)
                        sbCategory.AppendLine("<description><![CDATA[" + row["Description"].ToString() + "]]></description>");
                    if (row["FilePath"].ToString().Length > 1)
                        sbCategory.AppendLine("<original_url><![CDATA[" + row["FilePath"].ToString() + "]]></original_url>");
                    sbCategory.AppendLine("<credit><![CDATA[(주)천재교육]]></credit>");
                    sbCategory.AppendLine("<license><![CDATA[BY-NC-ND]]></license>");
                    sbCategory.AppendLine("</multimedia>");

                }
                else if (fileType == "SOU")
                {
                    sbCategory.AppendLine("<multimedia type=\"audio\" id=\"" + row["FileName"].ToString() + "\">");
                    sbCategory.AppendLine("<url><![CDATA[" + row["FileName"].ToString() + "]]></url>");
                    sbCategory.AppendLine("<credit><![CDATA[(주)천재교육]]></credit>");
                    sbCategory.AppendLine("<license><![CDATA[BY-NC-ND]]></license>");
                    sbCategory.AppendLine("</multimedia>");
                }
                else if (fileType == "MOV")
                {
                    sbCategory.AppendLine("<multimedia type=\"video\" id=\"" + row["FileName"].ToString() + "\">");
                    sbCategory.AppendLine("<url><![CDATA[" + row["FileName"].ToString() + "]]></url>");
                    sbCategory.AppendLine("<credit><![CDATA[(주)천재교육]]></credit>");
                    sbCategory.AppendLine("<license><![CDATA[BY-NC-ND]]></license>");
                    sbCategory.AppendLine("</multimedia>");
                }
            }
        }
        return sbCategory;
       
    }
    protected StringBuilder sbXMLByCategory(string tmpEntryID)
    {

        StringBuilder sbCategory = new StringBuilder();
        string strEntryIdx = string.Empty;
        string[] arrEntryID = tmpEntryID.Split(',');

        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();

        foreach (string strID in arrEntryID)
        {
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            strQuery = "SELECT * FROM TEntry WHERE Idx=" + strID + " ORDER BY SortNo ASC, Idx ASC";
            Cmd.CommandType = CommandType.Text;
            Cmd.CommandText = strQuery;

            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
            adapter.Fill(dt);
            //Con.Close();
            //Con.Open();
            //totalEntCount += dt.Rows.Count;
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();

            foreach (DataRow row in dt.Rows)
            {
                strEntryIdx = row["Idx"].ToString();

                sbCategory.AppendLine();
                //sbCategory.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
                sbCategory.AppendLine("<entry id=\"" + newEntryID + "\" timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" bookid=\"" + txtBookID.Text + "\" categoryid=\"" + txtCategoryID.Text + "\" thumbnailDisplayType=\"none\" serialImageDisplayType =\"vertical\" type=\"study\" >");

                SqlCommand Cmd_Insert = new SqlCommand();
                Cmd_Insert.Parameters.Add("@ExportIdx", SqlDbType.Int);
                Cmd_Insert.Parameters.Add("@EntryIdx", SqlDbType.Int);
                Cmd_Insert.Parameters.Add("@NewEntryID", SqlDbType.Int);

                Cmd_Insert.Parameters["@ExportIdx"].Value = txtExportIdx.Value;
                Cmd_Insert.Parameters["@EntryIdx"].Value = strEntryIdx;
                Cmd_Insert.Parameters["@NewEntryID"].Value = newEntryID;

                Cmd_Insert.CommandText = "USP_DaumCategory_INSERT";
                Cmd_Insert.CommandType = CommandType.StoredProcedure;
                Cmd_Insert.Connection = Con;
                Cmd_Insert.ExecuteNonQuery();

                newEntryID++;
                //sbCategory.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
                sbCategory.AppendLine("<headword><![CDATA[" + RemoveText(row["EntryTitle"].ToString()) + "]]></headword>");
                string strTitleK = string.Empty;
                if (row["EntryTitleK"].ToString().Length > 0)
                    //strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                    strTitleK = "<koheadword><![CDATA[" + RemoveText(row["EntryTitleK"].ToString()) + "]]></koheadword>";
                else
                    strTitleK = "<koheadword></koheadword>";
                string strTitleE = string.Empty;
                if (row["EntryTitleE"].ToString().Length > 0)
                    //strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                    strTitleE = "<enheadword><![CDATA[" + RemoveText(row["EntryTitleE"].ToString()) + "]]></enheadword>";
                else
                    strTitleE = "<enheadword></enheadword>";
                string strTitleC = string.Empty;
                if (row["EntryTitleC"].ToString().Length > 0)
                    //strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
                    strTitleC = "<hanheadword><![CDATA[" + RemoveText(row["EntryTitleC"].ToString()) + "]]></hanheadword>";
                else
                    strTitleC = "<hanheadword></hanheadword>";
                string strSummary = string.Empty;
                if (row["Summary"].ToString().Length > 0)
                    strSummary = "<summary><![CDATA[" + RemoveText(row["Summary"].ToString()) + "]]></summary>";
                else
                    strSummary = "<summary></summary>";
                //##### 2015.09-15 추가
                string strTitleSub1 = string.Empty;
                if (row["EntryTitleSub1"].ToString().Length > 0)
                    strTitleSub1 = "<title1><![CDATA[" + RemoveText(row["EntryTitleSub1"].ToString()) + "]]></title1>";
                else
                    strTitleSub1 = "<title1></title1>";
                string strTitleSub2 = string.Empty;
                if (row["EntryTitleSub2"].ToString().Length > 0)
                    strTitleSub2 = "<title2><![CDATA[" + RemoveText(row["EntryTitleSub2"].ToString()) + "]]></title2>";
                else
                    strTitleSub2 = "<title2></title2>";
                string strSynonym = string.Empty;
                if (row["Synonym"].ToString().Length > 0)
                    strSynonym = ConvertSynonym(RemoveText(row["Synonym"].ToString()));
                else
                    strSynonym = "";
                sbCategory.AppendLine(strTitleK);
                sbCategory.AppendLine(strTitleE);
                sbCategory.AppendLine(strTitleC);
                sbCategory.AppendLine(strSummary);
                sbCategory.AppendLine(strTitleSub1);
                sbCategory.AppendLine(strTitleSub2);
                sbCategory.AppendLine(strSynonym);

                sbCategory.AppendLine("<contents>");
                SqlCommand Cmd2 = new SqlCommand();
                Cmd2.Connection = Con;

                Cmd2.CommandText = "SELECT * FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + strEntryIdx + "";
                Cmd2.CommandType = CommandType.Text;
                //Con2.Open();
                SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                string strINFORMATION = string.Empty;
                if (reader2.HasRows)
                {
                    sbCategory.AppendLine("<table><![CDATA[<table><thead></thead><tbody>");
                    while (reader2.Read())
                    {
                        strINFORMATION = " <tr><td>" + reader2["OutlineName"] + "</td><td>" + reader2["OutlineData"] + "</td></tr>";
                        sbCategory.AppendLine(strINFORMATION);
                    }
                    sbCategory.AppendLine("</tbody></table>]]></table>");
                }
                reader2.Close();
                //Con2.Close();
                // INDEXCONTENT 처리 
                //Cmd2.Connection = Con2;
                Cmd2.CommandText = "USP_DaumExport_CONTENT_SELECT";
                Cmd2.CommandType = CommandType.StoredProcedure;
                Cmd2.Parameters.Add("@entryIdx", SqlDbType.Int);
                Cmd2.Parameters.Add("@type", SqlDbType.Char);
                Cmd2.Parameters["@entryIdx"].Value = Convert.ToInt32(strEntryIdx);
                Cmd2.Parameters["@type"].Value = "I"; //INDEXCONTENT
                //Con2.Open();
                DataTable dt2 = new DataTable();
                SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                adapter2.Fill(dt2);
                foreach (DataRow row2 in dt2.Rows)
                {
                    string content = RemoveText(row2["Content"].ToString());
                    string rectype = row2["RecType"].ToString();
                    string convertContent = string.Empty;
                    convertContent = ConvertIndexContent(rectype, content); 
                    //convertContent = convertContent.Replace("><", ">" + Environment.NewLine + "<"); 
                    sbCategory.AppendLine(convertContent);
                }
                //Con2.Close();
                // QUIZ 처리 

                //Con2 = new SqlConnection(connectionString);
                Cmd2 = new SqlCommand();
                Cmd2.Connection = Con;
                Cmd2.CommandText = "USP_DaumExport_CONTENT_SELECT";
                Cmd2.CommandType = CommandType.StoredProcedure;
                Cmd2.Parameters.Add("@entryIdx", SqlDbType.Int);
                Cmd2.Parameters.Add("@type", SqlDbType.Char);
                Cmd2.Parameters["@entryIdx"].Value = Convert.ToInt32(strEntryIdx);
                Cmd2.Parameters["@type"].Value = "Q"; //INDEXCONTENT


                //Cmd2.Connection = Con2;
                //Cmd2.CommandText = "SELECT Idx, RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx!=0 AND RecType!='TAG' AND DelFlag=0 "
                //    + " AND (Idx>=(SELECT Top 1 Idx FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND RecType='QUESTION' ORDER BY SortNo ASC)) ORDER BY SortNo ASC";
                //Cmd2.CommandType = CommandType.Text;
                //Con2.Open();
                dt2 = new DataTable();
                adapter2 = new SqlDataAdapter(Cmd2);
                adapter2.Fill(dt2);
                if (dt2.Rows.Count > 0)
                {
                    sbCategory.AppendLine("<study>");
                }

                int questionCount = 0;
                
                foreach (DataRow row2 in dt2.Rows)
                {
                    string content = RemoveText(row2["Content"].ToString());
                    string rectype = row2["RecType"].ToString();
                    string entryDataIdx = row2["Idx"].ToString();
                    string level = string.Empty;

                    if (rectype=="QUESTION")
                    {
                        SqlConnection Con3 = new SqlConnection(connectionString);
                        SqlCommand Cmd3 = new SqlCommand();
                        Cmd3.Connection = Con3;

                            //strQuery = "SELECT Top 1 Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND RecType='LEVEL' AND DelFlag=0 AND (Idx > " + entryDataIdx +") Order BY EntryIdx ASC, SortNo ASC";
                            strQuery = "SELECT Top 1 Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND RecType='LEVEL' AND DelFlag=0 AND ParentIdx=(SELECT ParentIdx FROM TEntryData WHERE Idx=" + entryDataIdx + ") Order By SortNo ASC";
                        //Response.Write (strQuery);
                        Cmd3.CommandText = strQuery;
                        Cmd3.CommandType = CommandType.Text;
                        Con3.Open();
                        SqlDataReader reader3 = Cmd3.ExecuteReader(CommandBehavior.CloseConnection);
                        if (reader3.Read())
                        {
                            level = reader3["Content"].ToString();
                        }
                        else
                        {
                            level = "";
                        }
                        reader3.Close();
                        Con3.Close();

                        if ( questionCount > 0)
                        {
                                sbCategory.AppendLine("</question>");
                        }
                        if (level.Length > 0)
                        {
                            if (level == "1")
                                sbCategory.AppendLine("<question level=\"상\">");
                            else if (level == "2")
                                sbCategory.AppendLine("<question level=\"중\">");
                            else if (level == "3")
                                sbCategory.AppendLine("<question level=\"하\">");
                            else 
                                sbCategory.AppendLine("<question>");
                        }
                        else
                        {
                            sbCategory.AppendLine("<question>");
                        }
                        questionCount++;
                    
                    }
                    
                    string convertContent = string.Empty;
                    if(preRecType=="DISTRACTOR"&&rectype!="DISTRACTOR")
                    {
                        convertContent = "]]></example>";
                    }
                    convertContent += ConvertQuiz(rectype, content);
                    //선택지 작업을 위해서 기존 RecType 저장
                    preRecType = rectype;
                    //convertContent = convertContent.Replace("><", ">" + Environment.NewLine + "<");
                    sbCategory.AppendLine(convertContent);
                }
                //Con2.Close();
                if(dt2.Rows.Count>0)
                {
                    sbCategory.AppendLine("</question>");
                    sbCategory.AppendLine("</study>");
                }
                sbCategory.AppendLine("</contents>");
                // TAG 처리 
                Cmd2.Connection = Con;
                Cmd2.CommandText = "SELECT Tag FROM TEntry WHERE Idx=" + strEntryIdx + " AND DelFlag=0 ";
                Cmd2.CommandType = CommandType.Text;
                //Con2.Open();
                dt2 = new DataTable();
                adapter2 = new SqlDataAdapter(Cmd2);
                adapter2.Fill(dt2);
                foreach (DataRow row2 in dt2.Rows)
                {
                    string content = row2["Tag"].ToString();
                    string convertContent = string.Empty;
                    convertContent = ConvertTag(content);
                    convertContent = convertContent.Replace("><", ">" + Environment.NewLine + "<");
                    sbCategory.AppendLine(convertContent);
                }
                sbCategory.AppendLine("</entry>");
                //Con2.Close();
            }
            Con.Close();
        }
        Conn.Close();
        return sbCategory;
    }
    private string ConvertTag(string content)
    {
        string rtnString = string.Empty;

        rtnString = "<tags>";

        content = content.Replace("<p>", "").Replace("</p>", "");
        string[] Tags = content.Split(',');

        foreach (string tag in Tags)
        {
            if(SearchTagID(tag.Trim()).Length>0)
                rtnString += "<tag id=\"" + SearchTagID(tag.Trim()) + "\"/>";

        }
        rtnString += "</tags>";
        return rtnString;

    }
    private string SearchTagID(string tagData)
    {
        string rtnString = string.Empty;
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        string strQuery = "SELECT Idx FROM TDaumTag WHERE Tag='" + tagData + "'";
        SqlCommand Cmd = new SqlCommand(strQuery, Conn);
        Cmd.CommandType = CommandType.Text;
        Cmd.Connection = Conn;
        Conn.Open();
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        if (reader.Read())
        {
            rtnString = reader["Idx"].ToString();
        }
        reader.Close();
        Conn.Close();
        return rtnString;
    }
    private string ConvertQuiz(string rectype, string content)
    {
        string rtnString = string.Empty;

        switch (rectype)
        {
            case ("QUESTION"):
                rtnString = "<title>" + content + "</title>";
                break;
            case ("EXAMPLE"):
                rtnString = "<description>" + content + "</description>";
                break;
            case ("PAIRSENTENCE"):
                rtnString = ConvertDescriptionPairSentence(content);
                break;
            case ("DISTRACTOR"):
                if(preRecType!="DISTRACTOR")
                {
                    rtnString = "<example><![CDATA[";
                }
                //content = RemoveText(content);
                content = content.Replace("<![CDATA[", "");
                content = content.Replace("]]>", "");
                rtnString += content +"<br />";
                break;
            case ("ANSWER"):
                rtnString = "<answer>" + content + "</answer>";
                break;
            case ("EXPLANATION"):
                rtnString = "<explanation>" + content + "</explanation>";
                break;
            //case ("LEVEL"):
            //    rtnString = "<question level=\""+ content + "\">";
            //    break;
        }

        return rtnString;

    }
    private string ConvertDescriptionPairSentence(string content)
    {
        string rtnString = string.Empty;
        //<SENTENCE>추출
        int contentStart = content.ToString().IndexOf("<SENTENCE>");
        int contentEnd = content.ToString().IndexOf("</SENTENCE>");
        string strContents = string.Empty;
        if ((contentEnd - (contentStart + 10)) > 0)
            strContents = content.ToString().Substring(contentStart + 10, contentEnd - (contentStart + 10));
        //<TRANSLATION>추출
        int transStart = content.ToString().IndexOf("<TRANSLATION>");
        int transEnd = content.ToString().IndexOf("</TRANSLATION>");
        string strTrans = string.Empty;
        if ((transEnd - (transStart + 13)) > 0)
            strTrans = content.ToString().Substring(transStart + 13, transEnd - (transStart + 13));
        //<FILENAME>추출
        int fileStart = content.ToString().IndexOf("<FILENAME>");
        int fileEnd = content.ToString().IndexOf("</FILENAME>");
        string strFile = string.Empty;
        if ((fileEnd - (fileStart + 10)) > 0)
            strFile = content.ToString().Substring(fileStart + 10, fileEnd - (fileStart + 10));

        rtnString = "<description_pair_sentences><description_pair_sentence><top><![CDATA[<audio id=\"" + strFile + "\"/><br />" + strContents.Replace("<![CDATA[", "") + "</top><bottom>" + strTrans + "</bottom></description_pair_sentence></description_pair_sentences>";

        return rtnString;
    }
    private string ConvertIndexContent(string rectype, string content)
    {
        string rtnString = string.Empty;
        switch(rectype)
        {
            case("MAINTITLE"):
                rtnString = "<title>" + content + "</title>";
                break;
            case("SUBTITLE"):
                rtnString = "<subtitle>" + content + "</subtitle>";
                break;
            case ("BASIC"):
                rtnString = "<paragraph>" + content + "</paragraph>";
                break;
            case ("BASIC2"):
                rtnString = "<paragraph>" + content + "</paragraph>";
                break;
            case ("CHDIVIDE"):
                rtnString = "<paragraph>" + content + "</paragraph>";
                break;
            case ("BOX"):
                rtnString = "<panel>" + content + "</panel>";
                break;
            case ("IMAGE"):
                rtnString = ConvertImageRecType(content);
                break;
            case ("MATRIX_TABLE"):
                rtnString = ConvertMatrixTable(content);
                break;
            case ("SOUND"):
                rtnString = ConvertSound(content);
                break;
            case ("PAIRSENTENCE"):
                rtnString = ConvertPairSentence(content);
                break;

        }

        return rtnString;

    }
    private string ConvertPairSentence(string content)
    {
        string rtnString = string.Empty;
        //<SENTENCE>추출
        int contentStart = content.ToString().IndexOf("<SENTENCE>");
        int contentEnd = content.ToString().IndexOf("</SENTENCE>");
        string strContents = string.Empty;
        if ((contentEnd - (contentStart + 10)) > 0)
            strContents = content.ToString().Substring(contentStart + 10, contentEnd - (contentStart + 10));
        //<TRANSLATION>추출
        int transStart = content.ToString().IndexOf("<TRANSLATION>");
        int transEnd = content.ToString().IndexOf("</TRANSLATION>");
        string strTrans = string.Empty;
        if ((transEnd - (transStart + 13)) > 0)
            strTrans = content.ToString().Substring(transStart + 13, transEnd - (transStart + 13));
        //<FILENAME>추출
        int fileStart = content.ToString().IndexOf("<FILENAME>");
        int fileEnd = content.ToString().IndexOf("</FILENAME>");
        string strFile = string.Empty;
        if ((fileEnd - (fileStart + 10)) > 0)
            strFile = content.ToString().Substring(fileStart + 10, fileEnd - (fileStart + 10));

        rtnString = "<pair_sentence><top><![CDATA[<audio id=\"" + strFile + "\"/><br />" + strContents.Replace("<![CDATA[", "") + "</top><bottom>" + strTrans + "</bottom></pair_sentence>";
        
        return rtnString;
    }

    private string ConvertSound(string content)
    {
        string rtnString = string.Empty;
        //<SENTENCE>추출
        int contentStart = content.ToString().IndexOf("<SENTENCE>");
        int contentEnd = content.ToString().IndexOf("</SENTENCE>");
        string strContents = string.Empty;
        if ((contentEnd - (contentStart + 10)) > 0)
            strContents = content.ToString().Substring(contentStart + 10, contentEnd - (contentStart + 10));
        //<FILENAME>추출
        int fileStart = content.ToString().IndexOf("<FILENAME>");
        int fileEnd = content.ToString().IndexOf("</FILENAME>");
        string strFile = string.Empty;
        if ((fileEnd - (fileStart + 10)) > 0)
            strFile = content.ToString().Substring(fileStart + 10, fileEnd - (fileStart + 10));

        rtnString = "<paragraph><![CDATA[<audio id=\"" + strFile + "\"/><br />" + strContents.Replace("<![CDATA[", "") + "</paragraph>";
        return rtnString;
    }

    
    private string ConvertMatrixTable(string content)
    {
        string rtnString = string.Empty;
        //<CONTENTS>추출
        int contentStart = content.ToString().IndexOf("<CONTENTS>");
        int contentEnd = content.ToString().IndexOf("</CONTENTS>");
        string strContents = string.Empty;
        if ((contentEnd - (contentStart + 10)) > 0)
            strContents = content.ToString().Substring(contentStart + 10, contentEnd - (contentStart + 10));

        rtnString = "<table>" + strContents + "</table>";

        return rtnString;
    }
    private string ConvertImageRecType(string content)
    {
        string rtnString = string.Empty;
        string[] arrImg = content.Split(new string[] { "<FILENAME>" }, StringSplitOptions.None);
        foreach (string imgEach in arrImg)
        {
            int imgEnd = imgEach.ToString().IndexOf("</FILENAME>");
            string strImageName = string.Empty;
            if (imgEnd > 0)
            {
                try
                {
                    //이미지명 추출
                    strImageName = imgEach.ToString().Substring(0, imgEnd);
                    rtnString += "<multimedia id=\"" + strImageName + "\" /><anchor/>";
                }
                catch (Exception ex)
                {
                    rtnString += "이미지명 추출 중다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : " + ex.ToString();
                    return rtnString;
                }
            }

        }
        return rtnString;
    }
    private string ConvertSynonym(string content)
    {
        string rtnString = string.Empty;

        rtnString = "<synonyms>";

        string[] Synonyms = content.Split(',');
        
        foreach (string synonym in Synonyms)
        {
            rtnString += "<synonym><![CDATA[" + synonym.Trim() + "]]></synonym>";

        }
        rtnString += "</synonyms>";
        rtnString = rtnString.Replace("><", ">" + Environment.NewLine + "<"); 
        return rtnString;
    }
    private string RemoveText(string content)
    {
        content = content.Replace("<TEXT>", "").Replace("</TEXT>", "");
        content = content.Replace("<ALIGN>left</ALIGN>","").Replace("<ALIGN>right</ALIGN>","").Replace("<ALIGN>center</ALIGN>","");

        //<a href="추출
        string[] arrImg = content.Split(new string[] { "<a href=" }, StringSplitOptions.None);
        foreach (string imgEach in arrImg)
        {
            int j = 0;
            if (j == 0) //배열 중 첫번째 사항은 처리하지 않기 위함
            { j++; }
            else
            {
                int imgEnd = imgEach.ToString().IndexOf(">");
                int linkNameEnd = imgEach.ToString().IndexOf("</a>");
                string strLinkAddr = string.Empty;
                string strLinkName = string.Empty;
                if (imgEnd > 0 && linkNameEnd > 0)
                {
                    try
                    {
                        //링크주소 추출
                        strLinkAddr = imgEach.ToString().Substring(1, imgEnd - 2);
                        //링크이름 추출
                        strLinkName = imgEach.ToString().Substring(imgEnd + 1, linkNameEnd - (imgEnd + 1));
                        content = content.Replace("<a href=\"" + strLinkAddr + "\">" + strLinkName + "</a>", strLinkName + "(" + strLinkAddr + ")");
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }
        // <$ 추출
        arrImg = content.Split(new string[] { "&lt;$" }, StringSplitOptions.None);
        foreach (string imgEach in arrImg)
        {
            int imgEnd = imgEach.ToString().IndexOf("/");
            int footnoteEnd = imgEach.ToString().IndexOf("$&gt;");
            string strFootWord = string.Empty;
            string strFootMemo = string.Empty;
            if (imgEnd > 0 && footnoteEnd > 0)
            {
                try
                {
                    //각주단어 추출
                    strFootWord = imgEach.ToString().Substring(0, imgEnd);
                    //각주내용 추출
                    strFootMemo = imgEach.ToString().Substring(imgEnd + 1, footnoteEnd-(imgEnd+1));
                    content = content.Replace("&lt;$" + strFootWord + "/" + strFootMemo + "$&gt;", "<FOOTNOTE TXT=\"" + strFootMemo + "\">" + strFootWord + "</FOOTNOTE>");
                }
                catch (Exception ex)
                {

                }
            }
        }
        // <img src=" 추출
        arrImg = content.Split(new string[] { "<img src=\"" }, StringSplitOptions.None);
        int i = 0;
        foreach (string imgEach in arrImg)
        {
            if (i == 0) //배열 중 첫번째 사항은 처리하지 않기 위함
            { i++; }
            else
            {
                int imgEnd = imgEach.ToString().IndexOf("\"");
                int imgTypeStart = imgEach.ToString().IndexOf("data-type=\"");
                int imgTypeEnd = imgEach.ToString().IndexOf("\" s");
                int imgFullTextEnd = imgEach.ToString().IndexOf("/>");
                string strFullText = string.Empty;
                string strImgName = string.Empty;
                string strImgType = string.Empty;
                if (imgEnd > 0 && imgFullTextEnd > 0)
                {
                    try
                    {
                        //이미지파일명 추출
                        strImgName = imgEach.ToString().Substring(0, imgEnd);
                        //이미지타입 추출
                        if (imgTypeStart > 0 && imgTypeEnd > 0)
                            strImgType = imgEach.ToString().Substring(imgTypeStart + 11, imgTypeEnd - (imgTypeStart + 11));
                        //풀 링크 추출
                        strFullText = "<img src=\"" + imgEach.ToString().Substring(0, imgFullTextEnd + 2);
                        if (strImgType.Length > 0)
                            content = content.Replace(strFullText, "<IMAGE ID=\"" + strImgName + "\" TYPE=\"" + strImgType + "\" />");
                        else
                            content = content.Replace(strFullText, "<IMAGE ID=\"" + strImgName + "\" />");
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        return content;
    }
    
    private void Serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
    {
        throw new NotImplementedException();
    }

    private void Serializer_UnknownNode(object sender, XmlNodeEventArgs e)
    {
        throw new NotImplementedException();
    }

    protected void btnImageChk_Click(object sender, EventArgs e)
    {
        exportIdx = txtExportIdx.Value;
        GetImageData();
        CheckImage();

    }
    protected void CheckImage()
    {
        exportIdx = txtExportIdx.Value;
        int totalImgCount = 0;
        int missImgCount = 0; 
        DBFileInfo();
        //########## 이미지 추출한 정보를 기반으로 파일이 존재하는지 체크한다 시작 ######
        string pathFolder = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/");
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT Idx, ImageName, ExTaskIdx FROM TDaumExportImage WHERE ExportIdx =" + exportIdx + " ";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        totalImgCount += dt.Rows.Count;
        Con.Close();
        foreach (DataRow row2 in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다
        {
            string fileURI = pathFolder + row2["ExTaskIdx"].ToString() + "/" + row2["ImageName"].ToString();
            if (File.Exists(fileURI))
            {
                Con = new SqlConnection(connectionString);
                strQuery = "UPDATE TDaumExportImage SET ExistFlag=1 WHERE Idx=" + row2["Idx"].ToString() + "";
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
        }
        Con = new SqlConnection(connectionString);
        Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT ImageName FROM TDaumExportImage WHERE ExportIdx =" + exportIdx + " AND ExistFlag=0 Group By ImageName";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        dt = new DataTable();
        adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();
        MissImageList.DataSource = dt;
        MissImageList.DataBind();
        missImgCount += dt.Rows.Count;

        txtSumImage.Text = "전체:" + totalImgCount + " | 누락:" + missImgCount;
    }
    protected void GetImageData()
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT * FROM TDaumExportData WHERE ExportIdx =" + exportIdx + "";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        //Con.Close();

        foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다 시작
        {
            switch (row["Type"].ToString() )
            {
                case "Category":
                    string[] arrEntryIdx = row["Content"].ToString().Split(',');
                    //SqlConnection Con2 = new SqlConnection(connectionString);
                    //Con2.Open();

                    foreach (string entryIdx in arrEntryIdx.Reverse<string>())
                    {
                        SqlCommand Cmd2 = new SqlCommand();
                        Cmd2.Connection = Con;// Con2;
                        Cmd2.CommandText = "SELECT Content, B.TaskIdx FROM TEntryData A, TEntry B WHERE A.EntryIdx =" + entryIdx + " AND A.EntryIdx=B.Idx AND A.DelFlag=0";
                        Cmd2.CommandType = CommandType.Text;
                        DataTable dt2 = new DataTable();
                        SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                        adapter2.Fill(dt2);
                        foreach (DataRow row2 in dt2.Rows)
                        {
                            string[] arrImgFile = row2["Content"].ToString().Split(new string[] { "<FILENAME>" }, StringSplitOptions.None);
                            foreach (string imgFile in arrImgFile)
                            {
                                //int preVal = unitKKK.ToString().IndexOf("<FILENAME>");
                                int nextVal = imgFile.ToString().IndexOf("</FILENAME>");
                                string imgName = string.Empty;
                                if (nextVal > 0)
                                {
                                    imgName = imgFile.ToString().Substring(0, nextVal);
                                    //Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand();
                                    Cmd.Parameters.Add("@ExportIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ExDataIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ImageName", SqlDbType.VarChar, 255);
                                    int intExDataIdx = 0;
                                    if (row["Idx"].ToString().Length > 0)
                                        intExDataIdx = Convert.ToInt32(row["Idx"].ToString());
                                    Cmd.Parameters.Add("@ExTaskIdx", SqlDbType.Int);
                                    Cmd.Parameters["@ExportIdx"].Value = exportIdx;
                                    Cmd.Parameters["@ExDataIdx"].Value = intExDataIdx;
                                    Cmd.Parameters["@ImageName"].Value = imgName;
                                    Cmd.Parameters["@ExTaskIdx"].Value = row2["TaskIdx"].ToString();

                                    Cmd.CommandText = "USP_DaumExportImage_INSERT";
                                    Cmd.CommandType = CommandType.StoredProcedure;
                                    Cmd.Connection = Con;
                                    //Con.Open();
                                    Cmd.ExecuteNonQuery();
                                    //Con.Close();
                                }
                            }
                            arrImgFile = row2["Content"].ToString().Split(new string[] { "<img src=\"" }, StringSplitOptions.None);
                            int i = 0;
                            foreach (string imgFile in arrImgFile)
                            {
                                if (i == 0) //배열 중 첫번째 사항은 처리하지 않기 위함
                                { i++; }
                                else
                                {
                                    //int preVal = unitKKK.ToString().IndexOf("<FILENAME>");
                                    int nextVal = imgFile.ToString().IndexOf("\"");
                                    string imgName = string.Empty;
                                    if (nextVal > 0)
                                    {
                                        imgName = imgFile.ToString().Substring(0, nextVal);
                                        //Con = new SqlConnection(connectionString);
                                        Cmd = new SqlCommand();
                                        Cmd.Parameters.Add("@ExportIdx", SqlDbType.Int);
                                        Cmd.Parameters.Add("@ExDataIdx", SqlDbType.Int);
                                        Cmd.Parameters.Add("@ImageName", SqlDbType.VarChar, 255);
                                        int intExDataIdx = 0;
                                        if (row["Idx"].ToString().Length > 0)
                                            intExDataIdx = Convert.ToInt32(row["Idx"].ToString());
                                        Cmd.Parameters.Add("@ExTaskIdx", SqlDbType.Int);

                                        Cmd.Parameters["@ExportIdx"].Value = exportIdx;
                                        Cmd.Parameters["@ExDataIdx"].Value = intExDataIdx;
                                        Cmd.Parameters["@ImageName"].Value = imgName;
                                        Cmd.Parameters["@ExTaskIdx"].Value = row2["TaskIdx"].ToString();

                                        Cmd.CommandText = "USP_DaumExportImage_INSERT";
                                        Cmd.CommandType = CommandType.StoredProcedure;
                                        Cmd.Connection = Con;
                                        //Con.Open();
                                        Cmd.ExecuteNonQuery();
                                        //Con.Close();
                                    }
                                }
                            }
                        }
                    }
                    continue;
            } //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다 끝
        }
    }
    protected void makeImageFile()
    {
        string pathFolder = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/");
        string pathNewFolder = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/Temp/");
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT ImageName, ExTaskIdx FROM TDaumExportImage WHERE ExportIdx =" + exportIdx + " Group By ImageName, ExTaskIdx";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();
        foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다
        {
            string fileURI = pathFolder + row["ExTaskIdx"].ToString() +"/"+ row["ImageName"].ToString();
            string newFileURI = pathNewFolder + row["ImageName"].ToString();
            if (File.Exists(fileURI))
            {
                try
                {
                    File.Copy(fileURI, newFileURI);
                }
                catch(Exception ex)
                {
                    lblImageFile.Text = "이미지 파일 복사 중 오류가 발생했습니다.";
                    continue;
                }
            }
        }
        if (dt.Rows.Count > 0)
        {
            string ExportImageFile = "IMG_Export_" + System.DateTime.Now.ToString("yyyyMMddhhmmss") + ".zip";
            string startPath = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/Temp/"); ;
            string zipPath = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/") + ExportImageFile;

            ZipFile.CreateFromDirectory(startPath, zipPath);
            lblImageFile.Text = "<a target='_blank' href='/cms100data/EntryData/" + ExportImageFile + "'>" + ExportImageFile + "</a><br />";
            // TExport Export테이블에 이미지 파일명 업데이트
            Con = new SqlConnection(connectionString);
            strQuery = "UPDATE TDaumExport SET ImgFile='" + ExportImageFile + "', ImageCount="+ dt.Rows.Count +" WHERE Idx =" + exportIdx + "";
            Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();

            foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다
            {
                string fileURI = pathFolder + row["ImageName"].ToString();
                string newFileURI = pathNewFolder + row["ImageName"].ToString();
                if (File.Exists(newFileURI))
                {
                    // ...or by using FileInfo instance method.
                    System.IO.FileInfo fi = new System.IO.FileInfo(@newFileURI);
                    try
                    {
                        fi.Delete();
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
    }
    protected void EntryList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string delID;
        RepeaterItem repeaterItem = EntryList.Items[e.Item.ItemIndex];
        HiddenField txtIDX = repeaterItem.FindControl("txtIDX") as HiddenField;
        delID = txtIDX.Value;

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand("DELETE FROM TDaumExportData WHERE Idx = " + delID +"; DELETE FROM TDaumExportImage WHERE ExDataIdx="+ delID  , Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

        Listing();
    }
    protected void btnCategory_Click(object sender, EventArgs e)
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        strQuery = "UPDATE TDaumExport SET ExportTitle='" + txtTitle.Text + "',"
            + " BookID =" + txtBookID.Text + ", CategoryID=" + txtCategoryID.Text + ", "
            + " NewEntryID="+ txtEntryID.Text +" WHERE Idx=" + exportIdx + "";

        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();
        Cmd = null;
        Con = null;
        Response.Write("<script>window.open('DaumExportCategory.aspx?exportIdx=" + exportIdx + "','popup','width=960,height=700, scrollbars=yes');</script>");
    }
    //protected void btnTask_Click(object sender, EventArgs e)
    //{
    //    exportIdx = txtExportIdx.Value;
    //    DBFileInfo();
    //    SqlConnection Con = new SqlConnection(connectionString);
    //    strQuery = "UPDATE TExport SET ExportTitle='" + txtTitle.Text + "',"
    //        + " TemplateIdx =" + ddlTemplate.SelectedValue + ", Target="+ ddlType.SelectedValue +" "
    //        + " WHERE Idx=" + exportIdx + "";

    //    SqlCommand Cmd = new SqlCommand(strQuery, Con);
    //    Cmd.CommandType = CommandType.Text;
    //    Con.Open();
    //    Cmd.ExecuteNonQuery();
    //    Con.Close();
    //    Cmd = null;
    //    Con = null;
    //    Response.Write("<script>window.open('ExportTask.aspx?exportIdx="+ exportIdx +"','popup','width=960,height=700, scrollbars=yes');</script>");
    //}

    //protected void btnBinder_Click(object sender, EventArgs e)
    //{
    //    exportIdx = txtExportIdx.Value;
    //    DBFileInfo();
    //    SqlConnection Con = new SqlConnection(connectionString);
    //    strQuery = "UPDATE TExport SET ExportTitle='" + txtTitle.Text + "',"
    //        + " TemplateIdx =" + ddlTemplate.SelectedValue + ", Target=" + ddlType.SelectedValue + " "
    //        + " WHERE Idx=" + exportIdx + "";

    //    SqlCommand Cmd = new SqlCommand(strQuery, Con);
    //    Cmd.CommandType = CommandType.Text;
    //    Con.Open();
    //    Cmd.ExecuteNonQuery();
    //    Con.Close();
    //    Cmd = null;
    //    Con = null;
    //    Response.Write("<script>window.open('ExportBinder.aspx?exportIdx=" + exportIdx + "','popup','width=960,height=700, scrollbars=yes');</script>");        
    //}

    //protected StringBuilder sbXMLByBinder(string tmpBinderID)
    //{
    //    string strQuery = string.Empty;
    //    string strEntryIdx = string.Empty;
    //    string strImageLink = string.Empty;
    //    string strImgFileName = string.Empty;
    //    string strXML = string.Empty;
    //    //strTaskIdx = "60";
    //    exportIdx = txtExportIdx.Value;

    //    StringBuilder sbTask = new StringBuilder();
    //    string[] arrBinderID = tmpBinderID.Split(',');

    //    foreach (string strID in arrBinderID)   //---Task 목록에서 TaskID를 추출한다. 시작
    //    {
    //        DBFileInfo();
    //        SqlConnection Con = new SqlConnection(connectionString);
    //        SqlCommand Cmd = new SqlCommand();
    //        Cmd.Connection = Con;
    //        strQuery = "SELECT A.EntryIdx, B.EntryTitle, B.EntryTitleK, B.EntryTitleE, B.EntryTitleC, B.Summary FROM TBinderEntry A, TEntry B WHERE A.BinderIdx=" + strID + " AND A.EntryIdx=B.Idx AND B.DelFlag=0 ORDER BY A.EntryIdx ASC";
    //        Cmd.CommandType = CommandType.Text;
    //        Cmd.CommandText = strQuery;
    //        Con.Open();

    //        DataTable dt = new DataTable();
    //        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
    //        adapter.Fill(dt);
    //        Con.Close();
    //        //totalEntCount += dt.Rows.Count;
    //        foreach (DataRow row in dt.Rows)
    //        {
    //            //strXML = "";
    //            //strXML += "<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=" + row["Idx"].ToString() + ">";
    //            //strXML += "<ENTRYTITLE><![CDATA["+ row["EntryTitle"].ToString() +"]]></ENTRYTITLE>";
    //            //strXML += "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
    //            //strXML += "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
    //            //strXML += "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";

    //            strEntryIdx = row["EntryIdx"].ToString();

    //            //sbTask.AppendLine();
    //            sbTask.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
    //            sbTask.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
    //            string strTitleK = string.Empty;
    //            if (row["EntryTitleK"].ToString().Length > 0)
    //                strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
    //            else
    //                strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
    //            string strTitleE = string.Empty;
    //            if (row["EntryTitleE"].ToString().Length > 0)
    //                strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
    //            else
    //                strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
    //            string strTitleC = string.Empty;
    //            if (row["EntryTitleC"].ToString().Length > 0)
    //                strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
    //            else
    //                strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";
    //            string strSummary = string.Empty;
    //            if (row["Summary"].ToString().Length > 0)
    //                strSummary = "<SUMMARY><![CDATA[" + row["Summary"].ToString() + "]]></SUMMARY>";
    //            else
    //                strSummary = "<SUMMARY></SUMMARY>";
    //            //##### 2015.09-15 추가
    //            string strTitleSub1 = string.Empty;
    //            if (row["EntryTitleSub1"].ToString().Length > 0)
    //                strTitleSub1 = "<ENTRYTITLE_SUB1><![CDATA[" + row["EntryTitleSub1"].ToString() + "]]></ENTRYTITLE_SUB1>";
    //            else
    //                strTitleSub1 = "<ENTRYTITLE_SUB1></ENTRYTITLE_SUB1>";
    //            string strTitleSub2 = string.Empty;
    //            if (row["EntryTitleSub2"].ToString().Length > 0)
    //                strTitleSub2 = "<ENTRYTITLE_SUB2><![CDATA[" + row["EntryTitleSub2"].ToString() + "]]></ENTRYTITLE_SUB2>";
    //            else
    //                strTitleSub2 = "<ENTRYTITLE_SUB2></ENTRYTITLE_SUB2>";
    //            string strSynonym = string.Empty;
    //            if (row["Synonym"].ToString().Length > 0)
    //                strSynonym = "<SYNONYM><![CDATA[" + row["Synonym"].ToString() + "]]></SYNONYM>";
    //            else
    //                strSynonym = "<SYNONYM></SYNONYM>";
    //            sbTask.AppendLine(strTitleK);
    //            sbTask.AppendLine(strTitleE);
    //            sbTask.AppendLine(strTitleC);
    //            sbTask.AppendLine(strSummary);
    //            sbTask.AppendLine(strTitleSub1);
    //            sbTask.AppendLine(strTitleSub2);
    //            sbTask.AppendLine(strSynonym);                //reader.Close();
    //            Con.Close();

    //            SqlConnection Con2 = new SqlConnection(connectionString);
    //            SqlCommand Cmd2 = new SqlCommand();
    //            Cmd2.Connection = Con2;

    //            //Cmd2.Parameters.Add("@Idx", SqlDbType.Int);
    //            //Cmd2.Parameters["@Idx"].Value = Convert.ToInt32(strEntryIdx);
    //            strQuery = "SELECT A.CodeName + '>'+ C.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
    //                + " FROM TEntry as Y "
    //                + " join TCode as A on A.Idx=Y.RevisionIdx "
    //                + "	join TCode as C on C.Idx=Y.SubjectIdx "
    //                + "	join TCode as E on E.Idx=Y.BrandIdx  "
    //                + "	join TCode as F on F.Idx=Y.GradeIdx  "
    //                + "	join TCode as G on G.Idx=Y.SemesterIdx "
    //                + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
    //                + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
    //                + " WHERE Y.Idx=" + strEntryIdx + "";
    //            //Response.Write (strQuery);
    //            Cmd2.CommandText = strQuery;
    //            Cmd2.CommandType = CommandType.Text;
    //            Con2.Open();
    //            SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
    //            if (reader2.Read())
    //            {
    //                string[] strCategory = reader2["category"].ToString().Split('>');
    //                int i = 1;
    //                foreach (string strUnit in strCategory)
    //                {
    //                    sbTask.AppendLine("<DIGIT" + i + ">" + strUnit + "</DIGIT" + i + ">");
    //                    i++;
    //                }
    //            }
    //            reader2.Close();
    //            Con2.Close();

    //            Cmd2.CommandText = "SELECT * FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + strEntryIdx + "";
    //            Cmd2.CommandType = CommandType.Text;
    //            Con2.Open();
    //            reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
    //            string strINFORMATION = string.Empty;
    //            if (reader2.HasRows)
    //            {
    //                sbTask.AppendLine("<INFOMATION>");
    //                while (reader2.Read())
    //                {
    //                    strINFORMATION = " <ITEM TITLE=\"" + reader2["OutlineName"] + "\"><![CDATA[" + reader2["OutlineData"] + "]]></ITEM>";
    //                    sbTask.AppendLine(strINFORMATION);
    //                }
    //                sbTask.AppendLine("</INFOMATION>");
    //            }
    //            reader2.Close();
    //            Con2.Close();

    //            Con2 = new SqlConnection(connectionString);
    //            Cmd2 = new SqlCommand();
    //            Cmd2.Connection = Con2;
    //            Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 AND DelFlag=0 ORDER BY SortNo ASC";
    //            Cmd2.CommandType = CommandType.Text;
    //            Con2.Open();
    //            DataTable dt2 = new DataTable();
    //            SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
    //            adapter2.Fill(dt2);
    //            foreach (DataRow row2 in dt2.Rows)
    //            {
    //                string content = row2["Content"].ToString();
    //                content = content.Replace("<CAPTION><TEXT>", "<CAPTION>");
    //                content = content.Replace("</TEXT></CAPTION>", "</CAPTION>");
    //                content = content.Replace("<DESCRIPTION><TEXT>", "<DESCRIPTION>");
    //                content = content.Replace("</TEXT></DESCRIPTION>", "</DESCRIPTION>");
    //                content = content.Replace("><", ">" + Environment.NewLine + "<");
    //                sbTask.AppendLine("<" + row2["RecType"].ToString() + ">");
    //                sbTask.AppendLine(content);
    //                sbTask.AppendLine("</" + row2["RecType"].ToString() + ">");

    //            }
    //            Con2.Close();
    //            sbTask.AppendLine("</ENTRY>");
    //        }
    //    } //---Task 목록에서 TaskID를 추출한다. 끝
    //    return sbTask;
    //}
    //protected StringBuilder sbXMLByTask(string tmpTaskID)
    //{
    //    string strQuery = string.Empty;
    //    string strEntryIdx = string.Empty;
    //    string strImageLink = string.Empty;
    //    string strImgFileName = string.Empty;
    //    string strXML = string.Empty;
    //    //strTaskIdx = "60";
    //    exportIdx = txtExportIdx.Value;

    //    StringBuilder sbTask = new StringBuilder();
    //    string[] arrTaskID = tmpTaskID.Split(',');

    //    foreach (string strID in arrTaskID)   //---Task 목록에서 TaskID를 추출한다. 시작
    //    {
    //        strTaskIdx = strID;

    //        DBFileInfo();
    //        SqlConnection Con = new SqlConnection(connectionString);
    //        SqlCommand Cmd = new SqlCommand();
    //        Cmd.Connection = Con;
    //        strQuery = "SELECT * FROM TEntry WHERE TaskIdx=" + strTaskIdx + " AND DelFlag=0 ORDER BY SortNo ASC, Idx ASC";
    //        Cmd.CommandType = CommandType.Text;
    //        Cmd.CommandText = strQuery;
    //        Con.Open();

    //        DataTable dt = new DataTable();
    //        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
    //        adapter.Fill(dt);
    //        Con.Close();
    //        //totalEntCount += dt.Rows.Count;
    //        foreach (DataRow row in dt.Rows)
    //        {
    //            //strXML = "";
    //            //strXML += "<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=" + row["Idx"].ToString() + ">";
    //            //strXML += "<ENTRYTITLE><![CDATA["+ row["EntryTitle"].ToString() +"]]></ENTRYTITLE>";
    //            //strXML += "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
    //            //strXML += "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
    //            //strXML += "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";

    //            strEntryIdx = row["Idx"].ToString();

    //            //sbTask.AppendLine();
    //            sbTask.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
    //            sbTask.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
    //            string strTitleK = string.Empty;
    //            if (row["EntryTitleK"].ToString().Length > 0)
    //                strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
    //            else
    //                strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
    //            string strTitleE = string.Empty;
    //            if (row["EntryTitleE"].ToString().Length > 0)
    //                strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
    //            else
    //                strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
    //            string strTitleC = string.Empty;
    //            if (row["EntryTitleC"].ToString().Length > 0)
    //                strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
    //            else
    //                strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";
    //            string strSummary = string.Empty;
    //            if (row["Summary"].ToString().Length > 0)
    //                strSummary = "<SUMMARY><![CDATA[" + row["Summary"].ToString() + "]]></SUMMARY>";
    //            else
    //                strSummary = "<SUMMARY></SUMMARY>";
    //            //##### 2015.09-15 추가
    //            string strTitleSub1 = string.Empty;
    //            if (row["EntryTitleSub1"].ToString().Length > 0)
    //                strTitleSub1 = "<ENTRYTITLE_SUB1><![CDATA[" + row["EntryTitleSub1"].ToString() + "]]></ENTRYTITLE_SUB1>";
    //            else
    //                strTitleSub1 = "<ENTRYTITLE_SUB1></ENTRYTITLE_SUB1>";
    //            string strTitleSub2 = string.Empty;
    //            if (row["EntryTitleSub2"].ToString().Length > 0)
    //                strTitleSub2 = "<ENTRYTITLE_SUB2><![CDATA[" + row["EntryTitleSub2"].ToString() + "]]></ENTRYTITLE_SUB2>";
    //            else
    //                strTitleSub2 = "<ENTRYTITLE_SUB2></ENTRYTITLE_SUB2>";
    //            string strSynonym = string.Empty;
    //            if (row["Synonym"].ToString().Length > 0)
    //                strSynonym = "<SYNONYM><![CDATA[" + row["Synonym"].ToString() + "]]></SYNONYM>";
    //            else
    //                strSynonym = "<SYNONYM></SYNONYM>";
    //            sbTask.AppendLine(strTitleK);
    //            sbTask.AppendLine(strTitleE);
    //            sbTask.AppendLine(strTitleC);
    //            sbTask.AppendLine(strSummary);
    //            sbTask.AppendLine(strTitleSub1);
    //            sbTask.AppendLine(strTitleSub2);
    //            sbTask.AppendLine(strSynonym);
    //            //reader.Close();
    //            Con.Close();

    //            SqlConnection Con2 = new SqlConnection(connectionString);
    //            SqlCommand Cmd2 = new SqlCommand();
    //            Cmd2.Connection = Con2;

    //            //Cmd2.Parameters.Add("@Idx", SqlDbType.Int);
    //            //Cmd2.Parameters["@Idx"].Value = Convert.ToInt32(strEntryIdx);
    //            strQuery = "SELECT A.CodeName + '>'+ C.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
    //                + " FROM TEntry as Y "
    //                + " join TCode as A on A.Idx=Y.RevisionIdx "
    //                + "	join TCode as C on C.Idx=Y.SubjectIdx "
    //                + "	join TCode as E on E.Idx=Y.BrandIdx  "
    //                + "	join TCode as F on F.Idx=Y.GradeIdx  "
    //                + "	join TCode as G on G.Idx=Y.SemesterIdx "
    //                + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
    //                + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
    //                + " WHERE Y.Idx=" + strEntryIdx + "";
    //            //Response.Write (strQuery);
    //            Cmd2.CommandText = strQuery;
    //            Cmd2.CommandType = CommandType.Text;
    //            Con2.Open();
    //            SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
    //            if (reader2.Read())
    //            {
    //                string[] strCategory = reader2["category"].ToString().Split('>');
    //                int i = 1;
    //                foreach (string strUnit in strCategory)
    //                {
    //                    sbTask.AppendLine("<DIGIT" + i + ">" + strUnit + "</DIGIT" + i + ">");
    //                    i++;
    //                }
    //            }
    //            reader2.Close();
    //            Con2.Close();

    //            Cmd2.CommandText = "SELECT * FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + strEntryIdx + "";
    //            Cmd2.CommandType = CommandType.Text;
    //            Con2.Open();
    //            reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
    //            string strINFORMATION = string.Empty;
    //            if (reader2.HasRows)
    //            {
    //                sbTask.AppendLine("<INFOMATION>");
    //                while (reader2.Read())
    //                {
    //                    strINFORMATION = " <ITEM TITLE=\"" + reader2["OutlineName"] + "\"><![CDATA[" + reader2["OutlineData"] + "]]></ITEM>";
    //                    sbTask.AppendLine(strINFORMATION);
    //                }
    //                sbTask.AppendLine("</INFOMATION>");
    //            }
    //            reader2.Close();
    //            Con2.Close();


    //            Con2 = new SqlConnection(connectionString);
    //            Cmd2 = new SqlCommand();
    //            Cmd2.Connection = Con2;
    //            Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 AND DelFlag=0 ORDER BY SortNo ASC";
    //            Cmd2.CommandType = CommandType.Text;
    //            Con2.Open();
    //            DataTable dt2 = new DataTable();
    //            SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
    //            adapter2.Fill(dt2);
    //            foreach (DataRow row2 in dt2.Rows)
    //            {
    //                string content = row2["Content"].ToString();
    //                content = content.Replace("<CAPTION><TEXT>", "<CAPTION>");
    //                content = content.Replace("</TEXT></CAPTION>", "</CAPTION>");
    //                content = content.Replace("<DESCRIPTION><TEXT>", "<DESCRIPTION>");
    //                content = content.Replace("</TEXT></DESCRIPTION>", "</DESCRIPTION>");
    //                content = content.Replace("><", ">" + Environment.NewLine + "<");
    //                sbTask.AppendLine("<" + row2["RecType"].ToString() + ">");
    //                sbTask.AppendLine(content);
    //                sbTask.AppendLine("</" + row2["RecType"].ToString() + ">");

    //            }
    //            Con2.Close();
    //            sbTask.AppendLine("</ENTRY>");
    //        }
    //    } //---Task 목록에서 TaskID를 추출한다. 끝
    //    return sbTask;
    //}

}
