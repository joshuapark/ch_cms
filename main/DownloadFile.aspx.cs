﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class main_NewQuery : System.Web.UI.Page
{

    private string connectionString = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        string fileName = string.Empty;
        string path = string.Empty;
        
        //int Idx = Convert.ToInt32(Request.Params["Idx"]);
        string Type = Request.Params["Type"].ToString();
        //string strQuery = "SELECT XMLFile, ImgFile FROM TExport WHERE Idx=" + Idx + "";
        //DBFileInfo();

        //SqlConnection Con = new SqlConnection(connectionString);
        //SqlCommand Cmd = new SqlCommand(strQuery, Con);
        //Con.Open();

        //SqlDataReader dataReader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //DataTable dt = new DataTable();
        //dt.Load(dataReader);

        //if (Type == "X")
        //{
        //    fileName = dt.Rows[0]["XMLFile"].ToString();
        //    path = HttpContext.Current.Server.MapPath("~/Data/" + fileName);
        //}
        //else if(Type == "I")
        //{
        //    fileName = dt.Rows[0]["ImgFile"].ToString();
        //    path = HttpContext.Current.Server.MapPath("~/UploadFile/" + fileName);
        //}
        //if (Con.State == ConnectionState.Open)
        //    Con.Close();

        //Cmd = null;
        //Con = null;

        fileName = Request.Params["dfn"].ToString();

        if (Type == "X")
        {
        //    fileName = dt.Rows[0]["XMLFile"].ToString();
            path = HttpContext.Current.Server.MapPath("~/Data/") + fileName;
        }
        else if(Type == "I")
        {
        //    fileName = dt.Rows[0]["ImgFile"].ToString();
            path = HttpContext.Current.Server.MapPath("~/UploadFile/") + fileName;
        }
        

        byte[] bts=System.IO.File.ReadAllBytes(path);
        Response.Clear();
        Response.AddHeader("Content-Type", "Application/otect-stream");
        Response.AddHeader("Content-Length", bts.Length.ToString());
        Response.AddHeader("Content-Disposition", "attachment; filename="+fileName);
        Response.BinaryWrite(bts); Response.Flush(); Response.End();


    }


    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];

        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    private void showMessage(String message){

        string script = "alert(\""+ message +"\");";
        ScriptManager.RegisterStartupScript(this, GetType() , "ServerControlScript", script, true);

    }
}