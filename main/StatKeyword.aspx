﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="StatKeyword.aspx.cs" Inherits="notice_List" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>


<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">
  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
                        <li><a href="DaumExportList.aspx">다음Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li class="active"><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
                        <li><a href="OutlineList.aspx">개요부관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->
    <form id="searchForm" runat="server">


	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">콘텐츠 주제별 현황</h2>
		</div><!-- // title -->	
        
        <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: 110px;">
			    <col style="width: auto;">
			    <col style="width: auto;">
		    </colgroup>
		    <tbody>
			    <tr>
				    <th>과목</th>
				    <td>
    <script type="text/javascript">
        function CheckSubjectSelect(cbControl)
        {   
            var chkBoxList3 = document.getElementById(cbControl);
            var chkAll3 =  document.getElementById('chkAllSubject');
            var chkBoxCount= chkBoxList3.getElementsByTagName("input");
            if ($(chkAll3).prop("checked")) {
                for(var i=0;i<chkBoxCount.length;i++)
                {
                    chkBoxCount[i].checked = true;
                }
                return false; 
            } else {
                for(var i=0;i<chkBoxCount.length;i++)
                {
                    chkBoxCount[i].checked = false;
                }
                return false; 
            }
        }
    </script>
                        <table class="de-check"><tr><td><input type="checkbox"id="chkAllSubject" onclick="javascript:CheckSubjectSelect('<%= cblSubject.ClientID %>');" /><label for="chkAllSubject">전체</label></td></tr></table>
                        <asp:CheckBoxList ID="cblSubject" CssClass="de-check" runat="server" RepeatColumns="7" RepeatDirection="Horizontal" RepeatLayout="Table">
                            <asp:ListItem value="0">전체</asp:ListItem>
                        </asp:CheckBoxList>
				    </td>
			    </tr>
                <tr>
				    <th>키워드</th>
                    <td>
					    <asp:TextBox runat="server" ID="txtKeyword" class="large" /><br />
                        여러개의 키워드를 입력하시는 경우는 키워드를 ,(컴마)으로 구분해 주시기 바랍니다.
                    </td>
                    <td>
					    <asp:Button runat="server" ID="btnSearch" Text="조회" CssClass="btn btn-success btn-lg" OnClick="btnSearch_Click" />
                    </td>
                </tr>
    		</tbody>
		</table><!-- // table-a -->
        <asp:Label ID="strTotalCnt" runat="server"></asp:Label>
		<table border="0" class="table table-list" style="padding:0; border-spacing:0">
            <asp:Repeater ID="EntryList" runat="server" >
                <HeaderTemplate>
                    <colgroup>
                        <col/>
                        <col/>
                        <col/>
                        <col/>
                        <col/>                            
                        <col/>
                        <col/>
                        <col/>
                        <col/>
                        <col/>                            
                        <col/>
                        <col/>
                        <col/>
                        <col/>
                        <col/>                            
                        <col/>
                        <col/>
                        <col/>
                        <col/>
                    </colgroup>
                    <tr>
				        <th rowspan="2">No</th>
				        <th rowspan="2">검색어</th>
				        <th rowspan="2">합계</th>
				        <th colspan="7">초등</th>
				        <th colspan="4">중등</th>
                        <th colspan="4">고등</th>
                        <th rowspan="2">공통</th>
                    </tr>
                    <tr>
				        <th>1학년</th>
				        <th>2학년</th>
				        <th>3학년</th>
				        <th>4학년</th>
				        <th>5학년</th>
				        <th>6학년</th>
				        <th>초공통</th>
				        <th>1학년</th>
				        <th>2학년</th>
				        <th>3학년</th>
				        <th>중공통</th>
				        <th>1학년</th>
				        <th>2학년</th>
				        <th>3학년</th>
				        <th>고공통</th>
                    </tr>
<%
    if (EntryList.Items.Count == 0)
    {
%>
                    <tr>
                        <td colspan="19" style="text-align:center">키워드 검색걸과가 없습니다.</td>
                    </tr>                    
<%
    }
%>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="text-align:center"><asp:Label ID="lblNo" Text=<%#DataBinder.Eval(Container.DataItem , "Row")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblKeyword" Text=<%#DataBinder.Eval(Container.DataItem , "Keyword")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblSumCnt" Text=<%#DataBinder.Eval(Container.DataItem , "totalCnt")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblE1" Text=<%#DataBinder.Eval(Container.DataItem , "E1")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblE2" Text=<%#DataBinder.Eval(Container.DataItem , "E2")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblE3" Text=<%#DataBinder.Eval(Container.DataItem , "E3")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblE4" Text=<%#DataBinder.Eval(Container.DataItem , "E4")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblE5" Text=<%#DataBinder.Eval(Container.DataItem , "E5")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblE6" Text=<%#DataBinder.Eval(Container.DataItem , "E6")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblE0" Text=<%#DataBinder.Eval(Container.DataItem , "E0")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblM1" Text=<%#DataBinder.Eval(Container.DataItem , "M1")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblM2" Text=<%#DataBinder.Eval(Container.DataItem , "M2")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblM3" Text=<%#DataBinder.Eval(Container.DataItem , "M3")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblM0" Text=<%#DataBinder.Eval(Container.DataItem , "M0")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblH1" Text=<%#DataBinder.Eval(Container.DataItem , "H1")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblH2" Text=<%#DataBinder.Eval(Container.DataItem , "H2")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblH3" Text=<%#DataBinder.Eval(Container.DataItem , "H3")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblH0" Text=<%#DataBinder.Eval(Container.DataItem , "H0")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblCommon" Text=<%#DataBinder.Eval(Container.DataItem , "Common")%> runat="server" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
                
                    <tr id="footerTemplate" runat="server">
				        <th colspan="2">합계</th>
				        <th><asp:Label ID="lblTotal" runat="server" /></th>
				        <th><asp:Label ID="lblSumE1" runat="server" /></th>
				        <th><asp:Label ID="lblSumE2" runat="server" /></th>
				        <th><asp:Label ID="lblSumE3" runat="server" /></th>
				        <th><asp:Label ID="lblSumE4" runat="server" /></th>
				        <th><asp:Label ID="lblSumE5" runat="server" /></th>
				        <th><asp:Label ID="lblSumE6" runat="server" /></th>
				        <th><asp:Label ID="lblSumE0" runat="server" /></th>
				        <th><asp:Label ID="lblSumM1" runat="server" /></th>
				        <th><asp:Label ID="lblSumM2" runat="server" /></th>
				        <th><asp:Label ID="lblSumM3" runat="server" /></th>
				        <th><asp:Label ID="lblSumM0" runat="server" /></th>
				        <th><asp:Label ID="lblSumH1" runat="server" /></th>
				        <th><asp:Label ID="lblSumH2" runat="server" /></th>
				        <th><asp:Label ID="lblSumH3" runat="server" /></th>
				        <th><asp:Label ID="lblSumH0" runat="server" /></th>
				        <th><asp:Label ID="lblSumCommon" runat="server" /></th>
                    </tr>
                
        </table>

	</div><!-- // contents -->
    
    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>