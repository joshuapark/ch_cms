﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using System.Text.RegularExpressions;

public partial class main_EntryEdit2 : System.Web.UI.Page
{

    private string connectionString = string.Empty;
    private string rootPath = string.Empty;
    
    public DataTable entryTable = null;
    public string strTaskIdx = string.Empty;
    public string EntryIdx = string.Empty;
    public string entryTag = string.Empty;
    public string preIdx = string.Empty;
    public string preTitle = string.Empty;
    public string nextIdx = string.Empty;
    public string nextTitle = string.Empty;
    
    public bool flagEdit = true;//엔트리 상태에 따라 편집 가능여부 판단. 작업완료(검수요청)의 경우 false로 변경
    public string usrAuth = string.Empty;
    public int EntryStatus = 1;
    public int TaskStatus = 1;
    private static string rootUrl = string.Empty;
    private static string imgURL = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"]+ ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }
            
        //##### 권한 처리는 GetTaskInfo에서 처리
        
        // 1. TaskIDX 받기
        if (Request.Params["idx"] != null)
        {
            strTaskIdx = Request.Params["idx"].ToString();
            if (strTaskIdx.Length == 0)
                Response.Redirect("EntryAddList.aspx");
        }
        else
        {
            Response.Redirect("EntryAddList.aspx");
        }

        //엔트리 삭제
        string strDeleteEntry = deleteEntryString.Value;
        if (strDeleteEntry.Length != 0) 
        {
            removeEntry(strDeleteEntry);
        }

        //엔트리  렉타입 삭제
        string deleteIDStrings = deleteIDString.Value;
        if (deleteIDStrings.Length != 0)
        {
            removeEntryData(deleteIDStrings);
        }

        //엔트리 저장
        string jsonSaveString = saveString.Value;
        if (jsonSaveString.Length != 0)
        {
            deleteEntryFile();
            saveEntry(jsonSaveString);
        }

        if (!Page.IsPostBack)
        {
            //DBFileInfo();
            //SqlConnection Con = new SqlConnection(connectionString);
            //Con.Open();

            GetTaskInfo();
        }
        
        GetEntryList();

    }
    private void GetMoveEntry()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Con.Open();
        Cmd.Parameters.Add("@Idx", SqlDbType.Int);
        Cmd.Parameters["@Idx"].Value = EntryIdx;
        Cmd.CommandText = "USP_Entry_Move_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        if (reader.Read())
        {
            preIdx = reader["preIdx"].ToString()+"";
            preTitle = reader["preTitle"].ToString()+"";
            nextIdx = reader["nextIdx"].ToString()+"";
            nextTitle = reader["nextTitle"].ToString()+"";

            if (preIdx.Length == 0)
            {
                divPreEntry.Visible = false;
                preEntry.Visible = false;
            }
            else
            {
                if (preTitle.Length > 9)
                    preTitle = "이전엔트리:" + preTitle.Substring(0, 10) + "...";
                else
                    preTitle = "이전엔트리:" + preTitle;
            }
            if (nextIdx.Length == 0)
            {
                divNextEntry.Visible = false;
                nextEntry.Visible = false;
            }
            else
            {
                if (nextTitle.Length > 9)
                    nextTitle = "다음엔트리:" + nextTitle.Substring(0, 10) + "...";
                else
                    nextTitle = "다음엔트리:" + nextTitle;
            }
        }
        Con.Close();
    }
    private void GetTaskInfo()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Con.Open();
        //Cmd.CommandText = "SELECT * FROM TTaskID WHERE Idx=" + strTaskIdx + " AND DelFlag=0";
        Cmd.Parameters.Add("@Idx", SqlDbType.Int);
        Cmd.Parameters["@Idx"].Value = strTaskIdx;
        Cmd.CommandText = "USP_Task_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        if (reader.Read())
        {
            //##### 권한처리 시작 
            //작업자이면 자신에게 배정된 작업만 볼 수 있음. 검수자, 관리자는 모두 조회 가능
            int uAuth = 1;
            if (Session["uauth"].ToString().Length > 0)
            {
                uAuth = Convert.ToInt32(Session["uauth"].ToString());
                usrAuth = Session["uauth"].ToString();
            }
            if (uAuth == 1)   // 작업자이면
            {
                if (reader["UserIdx"].ToString() == Session["uidx"].ToString() )  // 자신의 작업이면
                {
                    Session.Timeout = 120;
                }
                else
                {
                    string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                    if (Con.State == ConnectionState.Open)
                        Con.Close();
                    return;
                }
            }
            else              // 관리자 검수자이면
                Session.Timeout = 120;
            //##### 권한처리 끝 
            
            txtTaskTitle.Text = reader["TaskTitle"].ToString();
            txtTaskContent.Text = reader["TaskContent"].ToString();
            txtEntryCount.Text = reader["EntryCount"].ToString();
            txtManager.Text = reader["ManagerName"].ToString();
            txtUser.Text = reader["UserName"].ToString();
            txtPublishDate.Text = reader["PublishDate"].ToString();
            txtFinishDate.Text = reader["CheckDate"].ToString();
            TaskStatus = Convert.ToInt32(reader["status"].ToString());
            if (Convert.ToInt32(reader["status"].ToString()) > 2)
            {
                btnFinish.Visible = false;
                btnImport.Visible = false;
                sectionBtn.Visible = false;
                flagEdit = false;
            }
        }
        Con.Close();
    }
    private void GetEntryList()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlDataAdapter sda = new SqlDataAdapter("USP_EntryAdd_Left_SELECT " + strTaskIdx + "", Con);
        DataSet ds = new DataSet();
        sda.Fill(ds, "data_list");

        EntryList.DataSource = ds;
        EntryList.DataBind();

        Con.Close();

        EntryIdx = string.Empty;
        if (Request.Params["EntryIdx"] != null)
        {
            if (Request.Params["EntryIdx"].ToString().Length > 0)
            {
                EntryIdx = Request.Params["EntryIdx"].ToString();
                GetEntryData(EntryIdx);
            }
        }
        else
        {
            EntryIdx = ds.Tables["data_list"].Rows[0]["Idx"].ToString(); ;
            GetEntryData(EntryIdx);
        }
        GetMoveEntry();
    }
    private void GetEntryData(string EntryIdx)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM TEntryData WHERE EntryIdx = " + EntryIdx + " AND DelFlag=0", Con);

        this.entryTable = new DataTable();
        sda.Fill(entryTable);

        Con.Close();

        Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        //Cmd.CommandText = "SELECT E.*, U.UserName FROM TEntry E join TUser U on E.UserIdx=U.Idx WHERE E.Idx=" + idx + "";
        Cmd.Parameters.Add("@Idx", SqlDbType.Int);
        Cmd.Parameters["@Idx"].Value = EntryIdx;
        Cmd.CommandText = "USP_Entry_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;
        Con.Open();
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        txtEntryNo.Text = EntryIdx;
        if (reader.Read())
        {
            txtTitle.Text = Server.HtmlDecode(reader["EntryTitle"].ToString());
            txtTitle_K.Text = Server.HtmlDecode(reader["EntryTitleK"].ToString());
            txtTitle_E.Text = Server.HtmlDecode(reader["EntryTitleE"].ToString());
            txtTitle_C.Text = Server.HtmlDecode(reader["EntryTitleC"].ToString());
            txtTitle_Sub1.Text = Server.HtmlDecode(reader["EntryTitleSub1"].ToString());
            txtTitle_Sub2.Text = Server.HtmlDecode(reader["EntryTitleSub2"].ToString());
            txtSynonym.Text = Server.HtmlDecode(reader["Synonym"].ToString());
            txtEntryUser.Text = Server.HtmlDecode(reader["UserName"].ToString());
            txtCategory.Text = Server.HtmlDecode(reader["Category"].ToString());
            txtEditDate.Text = Server.HtmlDecode(reader["EditDate"].ToString());
            txtSummary.Text = Server.HtmlDecode(reader["Summary"].ToString());
            //EntryStatus = Convert.ToInt32(reader["status"].ToString());
            //if (EntryStatus > 2)
            //{
            //    sectionBtn.Visible = false;
            //    flagEdit = false;
            //}
            entryTag = reader["Tag"].ToString();
            txtTag.Text = entryTag;

            HttpCookie myCookie = new HttpCookie("UserSettings");
            myCookie["EntryIdx"] = EntryIdx;
            myCookie["TaskIdx"] = reader["TaskIdx"].ToString();
            myCookie.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(myCookie);
            

            string rootUrl = "http://" + Request.ServerVariables["HTTP_HOST"];
            imgURL = rootUrl + "/CMS100Data/EntryData/" + strTaskIdx + "/";
            fileUrl.Value = imgURL;

            GetOutline();
        }
        if(Con.State==ConnectionState.Open)
            Con.Close();
    }
    private void GetOutline()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        string strQuery = "SELECT OutlineIdx as Idx, OutlineName, OutlineData, OutlineType FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + txtEntryNo.Text + " ORDER BY OutlineIdx ASC";
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Con.Open();

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");
        if (ds.Tables["data_list"].Rows.Count > 0)
        {
            StringBuilder strOutline = new StringBuilder();
            strOutline.Append("<table class=\"table\">");
            try
            {

                foreach (DataRow dr in ds.Tables["data_list"].Rows)
                {

                    strOutline.Append("<tr><th>" + dr["OutlineName"].ToString() + "</th><td>" + dr["OutlineData"].ToString() + "</td></tr>");

                }
            }
            catch (Exception ex)
            {
                Response.Write("개요부 저장 중 오류가 발생했습니다.<br />오류내용:" + ex.ToString());
            }


            strOutline.Append("</table>");
            lblOutline.Text = strOutline.ToString();
        }

        //Close the connection.
        Con.Close();
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버  
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    public string getRecTypeName(string recType)
    {
        string result = String.Empty;
        switch (recType)
        {

            case "INDEXCONTENT":
                result = "대분류";
                break;
            case "MAINTITLE":
                result = "대제목";
                break;
            case "SUBTITLE":
                result = "중제목";
                break;
            case "BOX":
                result = "블럭";
                break;
            case "BASIC":
                result = "기본";
                break;
            case "BASIC2":
                result = "기본2";
                break;
            case "RELATEDSEARCH":
                result = "연관검색어";
                break;
            case "MATRIX_TABLE":
                result = "표";
                break;
            case "ANNOTATION":
                result = "주석";
                break;
            case "IMAGE":
                result = "이미지";
                break;
            case "TAG":
                result = "태그";
                break;
            case "QUIZ":
                result = "문제";
                break;
            case "INFOTABLE":
                result = "정보";
                break;
            case "QUESTION":
                result = "문제";
                break;
            case "EXAMPLE":
                result = "보기";
                break;
            case "DISTRACTOR":
                result = "선택지";
                break;
            case "EXPLANATION":
                result = "설명";
                break;
            case "ANSWER":
                result = "정답";
                break;
            case "LEVEL":
                result = "난이도";
                break;
            case "CHDIVIDE":
                result = "한자분해";
                break;
            case "SOUND":
                result = "사운드";
                break;
            case "PAIRSENTENCE":
                result = "Pair Sentence";
                break;
            case "VIDEO":
                result = "동영상";
                break;

        }

        return result;

    }

    public static string convertXmlToHtml(string strXml, string recType)
    {

        //align이 있는지 확인한다.
        bool isAlign = false;
        string strAlign = String.Empty;
        int startAlignIndex = strXml.IndexOf("<ALIGN>");
        string alignEndTag = "</ALIGN>";
        int endAlignIndex = strXml.LastIndexOf(alignEndTag);
        if (startAlignIndex > -1)
        {
            isAlign = true;
            string alignTag = strXml.Substring(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
            strAlign = alignTag.Replace("<ALIGN>", "").Replace("</ALIGN>", "");
            strXml = strXml.Remove(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
        }

        
        if (recType.Equals("IMAGE"))
        {
            if (strXml.Contains("<IMGS>"))
            {
                if (!strXml.Contains("</IMGS>"))
                {
                    strXml += "</IMGS>";
                }
            }

            return parseImageXmlToHtml(strXml, isAlign, strAlign);
        }
        else if (recType.Equals("MATRIX_TABLE"))
        {
            return parseTableXmlToHtml(strXml, isAlign, strAlign);
        }
        else if (recType.Equals("SOUND") || recType.Equals("PAIRSENTENCE") || recType.Equals("VIDEO"))
        {
            strXml = strXml.Replace("<FILENAME>", string.Empty);
            strXml = strXml.Replace("</FILENAME>", string.Empty);
            return strXml;
        }
        else if (recType.Equals("LEVEL")) {
            return strXml;
        }
        else
        {

            strXml = strXml.Replace("<img src=\"", "<img src=\"" + imgURL);
            strXml = strXml.Replace("<br>", "<br/>");

            string result = string.Empty;
            strXml = "<edu>" + strXml + "</edu>";
            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
                {
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.CDATA:
                                if (isAlign)
                                {
                                    StringWriter stringWriter = new StringWriter();
                                    HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);
                                    htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                                    htmlWriter.Write(reader.Value);
                                    htmlWriter.RenderEndTag();
                                    result += stringWriter.ToString();
                                }
                                else
                                {
                                    result += reader.Value;
                                }
                                break;
                            case XmlNodeType.Text:
                                result += reader.Value;
                                break;

                        }
                    }
                }
            }
            catch (Exception e) {
                Logger.Log("에러 발생" + e.Message + " xml : " +  strXml);
            }

            return result;

        }


    }

    private static string pasteImgURL(string strSrc)
    {

        ////이미지 태그가 있다면 src를 전체 경로로 만들어준다.
        XmlDocument xml = new XmlDocument();
        xml.LoadXml("<edu>" + strSrc + "</edu>");
        XmlNodeList imgNodeList = xml.GetElementsByTagName("img");
        if (imgNodeList.Count > 0)
        {
            foreach (XmlNode imgNode in imgNodeList)
            {
                XmlAttributeCollection attributes = imgNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Name.Contains("src"))
                    {
                        string imgSrc = imgURL + attribute.Value;
                        attribute.Value = imgSrc;
                        break;
                    }
                }
            }

            //문자열로 출력시킨다.
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
            {
                xml.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                strSrc = stringWriter.GetStringBuilder().ToString();
            }

            strSrc = strSrc.Replace("<edu>", string.Empty);
            strSrc = strSrc.Replace("</edu>", string.Empty);

        }

        return strSrc;
    }


    private static string parseImageXmlToHtml(string strXml, bool isAlign, string strAlign)
    {
        //루트태그를 붙여줌
        strXml = "<edu>" + strXml + "</edu>";

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

        using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
        {

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.CDATA)
                {
                    htmlWriter.Write(reader.Value);

                }
                else if (reader.NodeType == XmlNodeType.Element)
                {

                    switch (reader.Name)
                    {
                        case "TEXT":

                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "IMGS":
                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "IMG":
                            break;
                        case "WIDTH":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Width, reader.Value);
                            break;
                        case "HEIGHT":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Height, reader.Value);
                            break;
                        case "FILENAME":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Src, imgURL + reader.Value);

                            break;
                    }

                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    switch (reader.Name)
                    {
                        case "TEXT":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMGS":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMG":
                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Img);
                            htmlWriter.RenderEndTag();
                            break;
                        case "WIDTH":
                            break;
                        case "HEIGHT":
                            break;
                    }
                }
            }
        }

        return stringWriter.ToString();
    }

    private static string parseTableXmlToHtml(string strXml, bool isAlign, string strAlign)
    {
        //루트태그를 붙여줌
        strXml = "<edu>" + strXml + "</edu>";

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

        using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
        {

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.CDATA)
                {

                    string valueString = reader.Value;
                    valueString = valueString.Replace("src=\"", "src=\"" + imgURL);

                    htmlWriter.Write(valueString);
                }
                else if (reader.NodeType == XmlNodeType.Element)
                {

                    switch (reader.Name)
                    {
                        case "TEXT":

                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "WIDTH":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Width, reader.Value);
                            break;
                        case "HEIGHT":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Height, reader.Value);
                            break;
                        case "FILENAME":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Src, imgURL + reader.Value);
                            break;
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    switch (reader.Name)
                    {
                        case "TEXT":
                            htmlWriter.RenderEndTag();
                            break;
                        case "WIDTH":
                            break;
                        case "HEIGHT":
                            break;
                    }
                }
            }
        }

        return stringWriter.ToString();
    }


    private string convertHtmlToXml(string strHtml, string recType)
    {
        //공백을 없앤다.
        strHtml = strHtml.Replace("&nbsp;", " ");

        //hr태그에 닫는 태그를 넣는다.
        strHtml = strHtml.Replace("<hr>", "<hr/>");

        //img 태그와 br 태그의 닫는 태그를 넣는다.
        strHtml = strHtml.Replace("<br>", "<br/>");
        
        int imgStartIndex = 0;
        int imgEndIndex = 0;

        imgStartIndex = strHtml.IndexOf("<img", 0);
        while (imgStartIndex > -1)
        {

            imgEndIndex = strHtml.IndexOf(">", imgStartIndex);
            strHtml = strHtml.Insert(imgEndIndex, "/");
            imgStartIndex = strHtml.IndexOf("<img", imgEndIndex);

        }

        //이미지의 이상한 문자를 지운다.
        StringBuilder sb = new StringBuilder(strHtml);
        sb.Replace(char.ConvertFromUtf32(8203), string.Empty);
        strHtml = sb.ToString();

        //div 태그가 있는지 확인한다.
        //div 태그가 있다면 정렬을 가져온다.
        string strAlign = string.Empty;
        XmlDocument divXml = new XmlDocument();
        divXml.LoadXml("<edu>" + strHtml + "</edu>");

        XmlNodeList divNodeList = divXml.GetElementsByTagName("div");
        if (divNodeList.Count > 0)
        {
            foreach (XmlNode divNode in divNodeList)
            {
                XmlAttributeCollection attributes = divNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Value.Contains("text-align"))
                    {
                        strAlign = "<ALIGN>" + attribute.Value.Replace("text-align:", "").Replace(";", string.Empty).Trim() + "</ALIGN>";
                    }
                }
            }


            int divStartIndex = strHtml.IndexOf("<div", 0);
            while (divStartIndex > -1)
            {

                int divEndIndex = strHtml.IndexOf(">", divStartIndex) + 1;
                strHtml = strHtml.Remove(divStartIndex, divEndIndex - divStartIndex);
                divStartIndex = strHtml.IndexOf("<div", divStartIndex);
            }
            //div 태그를 지운다.
            strHtml = strHtml.Replace("</div>", string.Empty);
        }

        //링크 태그의 쓰레기 속성을 지운다.
        XmlDocument xml = new XmlDocument();
        xml.PreserveWhitespace = true;
        xml.LoadXml("<edu>" + strHtml + "</edu>");

        XmlNodeList aNodeList = xml.GetElementsByTagName("a");
        if (aNodeList.Count > 0)
        {
            foreach (XmlNode aNode in aNodeList)
            {
                XmlAttributeCollection attributes = aNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Name.Contains("data-cke-saved-href"))
                    {
                        attributes.Remove(attribute);
                        break;
                    }
                }
            }
        }

        //이미지 태그의 쓰레기 속성을 지운다.
        if (!recType.Equals("IMAGE"))
        {
            XmlNodeList imgNodeList = xml.GetElementsByTagName("img");
            if (imgNodeList.Count > 0)
            {
                foreach (XmlNode imgNode in imgNodeList)
                {
                    XmlAttributeCollection attributes = imgNode.Attributes;
                    foreach (XmlAttribute attribute in attributes)
                    {
                        if (attribute.Name.Contains("data-cke-saved-src"))
                        {
                            attributes.Remove(attribute);
                            break;
                        }
                    }

                    attributes = imgNode.Attributes;
                    foreach (XmlAttribute attribute in attributes)
                    {
                        if (attribute.Name.Contains("alt"))
                        {
                            attributes.Remove(attribute);
                            break;
                        }
                    }

                    attributes = imgNode.Attributes;
                    if (attributes.GetNamedItem("src").Value != null)
                    {

                        string dataType = attributes.GetNamedItem("data-type").Value;
                        string imgSrc = attributes.GetNamedItem("src").Value;
                        if (!imgSrc.Equals(string.Empty))
                        {
                            string[] splitedFileName = imgSrc.Split(new char[] { '/' });
                            string fileName = splitedFileName[splitedFileName.Length - 1];

                            string newFileName = saveImageFileWithDataType(fileName, dataType);
                            attributes.GetNamedItem("src").Value = newFileName;
                            if (newFileName != null && !newFileName.Equals(string.Empty))
                                this.saveEntryFile(newFileName, "IMG");
                        }
                    }

                    attributes = imgNode.Attributes;
                    if (attributes.GetNamedItem("style") == null)
                    {
                        XmlAttribute styleAttribute = xml.CreateAttribute("style");
                        styleAttribute.Value = "";
                        attributes.SetNamedItem(styleAttribute);
                    }

                }
            }
        }

        //문자열로 출력시킨다.
        
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.OmitXmlDeclaration = true;
        using (var stringWriter = new StringWriter())
        using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
        {
            xml.WriteTo(xmlTextWriter);
            xmlTextWriter.Flush();
            strHtml = stringWriter.GetStringBuilder().ToString();
        }
        

        strHtml = strHtml.Replace("<edu>", string.Empty);
        strHtml = strHtml.Replace("</edu>", string.Empty);

        //태그를 파싱한다.
        string strConvert = string.Empty;
        if (recType.Equals("IMAGE"))
        {
            if (strAlign.Equals(string.Empty))
            {
                strAlign = "<ALIGN></ALIGN>";
            }

            strConvert = this.parseTagContent(strHtml) + strAlign;
        }
        else if (recType.Equals("MATRIX_TABLE"))
        {

            if (strAlign.Equals(string.Empty))
            {
                strAlign = "<ALIGN>left</ALIGN>";
            }

            strConvert = this.parseTagContent(strHtml) + strAlign;
        }
        else if (recType.Equals("SOUND") || recType.Equals("PAIRSENTENCE") || recType.Equals("VIDEO"))
        {
            strHtml = strHtml.Replace("<br />", string.Empty);
            strConvert = "<FILENAME>" + strHtml + "</FILENAME>";

            string fileName = strHtml;
            if (fileName != null && !fileName.Equals(string.Empty))
            {
                string type = string.Empty;
                //사운드와 비디오를 저장한다.
                if (recType.Equals("VIDEO"))
                {
                    saveEntryFile(strHtml, "MOV");
                }
                else
                {
                    saveEntryFile(strHtml, "SOU");
                }
            }

        }
        else if (recType.Equals("LEVEL")) {
            strConvert = strHtml;
        }
        else
        {

            StringWriter strWriter = new StringWriter();
            XmlTextWriter xmlWriter = new XmlTextWriter(strWriter);

            xmlWriter.WriteStartElement("TEXT");
            xmlWriter.WriteCData(strHtml);
            xmlWriter.WriteEndElement();

            if (recType.Equals("BASIC") || recType.Equals("BASIC2") || recType.Equals("BOX"))
            {
                if (strAlign.Equals(string.Empty))
                {
                    strAlign = "<ALIGN>left</ALIGN>";
                }
            }

            strConvert = strWriter.ToString() + strAlign;
        }

        return strConvert;
    }
    private string parseTagContent(string strPTagContent)
    {

        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);

        strPTagContent = "<edu>" + strPTagContent + "</edu>";

        string strText = string.Empty;
        Boolean startedImageTage = false;

        using (XmlReader reader = XmlReader.Create(new StringReader(strPTagContent)))
        {
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Text:
                        if (startedImageTage)
                        {
                            xmlWriter.WriteEndElement();
                            startedImageTage = false;
                        }

                        strText += reader.Value;

                        break;
                    case XmlNodeType.Element:

                        switch (reader.Name)
                        {
                            case "table":
                                if (startedImageTage)
                                {
                                    xmlWriter.WriteEndElement();
                                    startedImageTage = false;
                                }

                                if (strText != string.Empty)
                                {
                                    xmlWriter.WriteStartElement("TEXT");
                                    xmlWriter.WriteCData(strText);
                                    xmlWriter.WriteEndElement();
                                    strText = string.Empty;
                                }


                                string tableStyle = reader.GetAttribute("style");
                                string tableHeight = string.Empty;
                                string tableWidth = string.Empty;
                                if (tableStyle != null)
                                {
                                    string[] splitedImgStyle = tableStyle.Split(new char[] { ';' });

                                    foreach (string attribute in splitedImgStyle)
                                    {
                                        if (attribute.Contains("width"))
                                        {
                                            tableWidth = attribute.Replace("width:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                        else if (attribute.Contains("height"))
                                        {
                                            tableHeight = attribute.Replace("height:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                    }
                                }

                                xmlWriter.WriteStartElement("CONTENTS");
                                xmlWriter.WriteCData(reader.ReadOuterXml());
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("HEIGHT");
                                xmlWriter.WriteValue(tableHeight);
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("WIDTH");
                                xmlWriter.WriteValue(tableWidth);
                                xmlWriter.WriteEndElement();
                                break;
                            case "img":
                                if (strText != string.Empty)
                                {
                                    xmlWriter.WriteStartElement("TEXT");
                                    xmlWriter.WriteCData(strText);
                                    xmlWriter.WriteEndElement();
                                    strText = string.Empty;
                                }

                                if (!startedImageTage)
                                {
                                    xmlWriter.WriteStartElement("IMGS");
                                    startedImageTage = true;
                                }

                                xmlWriter.WriteStartElement("IMG");

                                string imgHeight = string.Empty;
                                string imgWidth = string.Empty;
                                string imgStyle = reader.GetAttribute("style");
                                string fileSrc = reader.GetAttribute("src");

                                if (imgStyle != null)
                                {
                                    string[] splitedImgStyle = imgStyle.Split(new char[] { ';' });

                                    foreach (string attribute in splitedImgStyle)
                                    {
                                        if (attribute.Contains("width"))
                                        {
                                            imgWidth = attribute.Replace("width:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                        else if (attribute.Contains("height"))
                                        {
                                            imgHeight = attribute.Replace("height:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                    }
                                }

                                xmlWriter.WriteStartElement("FILENAME");
                                string[] splitedFileName = fileSrc.Split(new char[] { '/' });
                                string fileName = splitedFileName[splitedFileName.Length - 1];

                                if (fileName != null && !fileName.Equals(string.Empty))
                                {
                                    this.saveEntryFile(fileName, "IMG");
                                }
                                xmlWriter.WriteValue(fileName);
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteRaw("<WIDTH>" + imgWidth + "</WIDTH>");

                                xmlWriter.WriteRaw("<HEIGHT>" + imgHeight + "</HEIGHT>");
                                xmlWriter.WriteEndElement();
                                break;
                        }

                        break;
                    case XmlNodeType.EndElement:

                        switch (reader.Name)
                        {
                            case "table":
                                break;
                            case "img":
                                xmlWriter.WriteEndElement();
                                break;
                        }
                        break;
                }
            }
        }

        StringBuilder sb = new StringBuilder(strText);
        sb.Replace(char.ConvertFromUtf32(8203), string.Empty);
        strText = sb.ToString();

        if (strText != string.Empty)
        {
            xmlWriter.WriteStartElement("TEXT");
            xmlWriter.WriteCData(strText);
            xmlWriter.WriteEndElement();
        }
        if (startedImageTage) xmlWriter.WriteEndElement();

        return stringWriter.ToString();
    }

    [System.Web.Services.WebMethod]
    public static string getContents(string idxList)
    {
        string[] arrIdxList = idxList.Split(new char[] { ',' });
        DataTable entryTable = new DataTable();

        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        SqlConnection Con = new SqlConnection(connectionString);

        foreach (string unitIdx in arrIdxList)
        {
            if (unitIdx.Trim().Length > 0)
            {
                Con.Open();

                string strQuery = "SELECT * FROM TEntryData Where idx=" + unitIdx;
                SqlCommand cmd = new SqlCommand(strQuery, Con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);

                sda.Fill(entryTable);

                DataRow[] entryRowList = entryTable.Select("idx=" + unitIdx);
                int childCount = Convert.ToInt32(entryRowList[0]["childcount"]);

                if (childCount > 0)
                {
                    strQuery = "SELECT * FROM TEntryData Where parentidx=" + unitIdx;
                    cmd = new SqlCommand(strQuery, Con);
                    sda = new SqlDataAdapter(cmd);
                    sda.Fill(entryTable);
                }

                Con.Close();
            }
        }

        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

        Dictionary<string, object> row;
        foreach (DataRow dr in entryTable.Rows)
        {
            Boolean isParse = true;
            String recType = Convert.ToString(dr["RecType"]);

            if (recType.Equals("INDEXCONTENT") || recType.Equals("QUIZ") || recType.Equals("EXAMPLE"))
            {
                isParse = false;
            }


            row = new Dictionary<string, object>();
            foreach (DataColumn col in entryTable.Columns)
            {
                if (col.ColumnName.Equals("Content"))
                {
                    if (isParse)
                    {
                        string parseContent = convertXmlToHtml(Convert.ToString(dr[col]) , recType);
                        row.Add(col.ColumnName, parseContent);
                    }
                }
                else
                {
                    row.Add(col.ColumnName, dr[col]);
                }

            }
            rows.Add(row);
        }

        return serializer.Serialize(rows);
    }




    private void saveCategory(string entryIdx)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        //Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        //Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@MUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@EntryIdx", SqlDbType.Int);
        string[] arrCateIdx = hfCategoryIdx.Value.Split(new Char[] { '>' });

        Cmd.Parameters["@RevisionIdx"].Value = arrCateIdx[0];
        //Cmd.Parameters["@SubjectGroupIdx"].Value = ddlSubjectGroup.SelectedValue;
        Cmd.Parameters["@SubjectIdx"].Value = arrCateIdx[1];
        //Cmd.Parameters["@SchoolIdx"].Value = ddlSchool.SelectedValue;
        Cmd.Parameters["@BrandIdx"].Value = arrCateIdx[2];
        Cmd.Parameters["@GradeIdx"].Value = arrCateIdx[3];
        Cmd.Parameters["@SemesterIdx"].Value = arrCateIdx[4];
        Cmd.Parameters["@LUnitIdx"].Value = arrCateIdx[5];
        Cmd.Parameters["@MUnitIdx"].Value = arrCateIdx[6];
        Cmd.Parameters["@EntryIdx"].Value = entryIdx;
        Cmd.CommandText = "USP_Category_UPDATE";
        Cmd.CommandType = CommandType.StoredProcedure;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

    }
    private void saveEntry(string jsonSaveString)
    {
        if (hfCategoryIdx.Value.Length > 0)
        {
            this.saveCategory(txtEntryNo.Text);
        }
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();

        //엔트리 정보를 업데이트 합니다.
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.Parameters.Add("@Idx", SqlDbType.Int);
        Cmd.Parameters.Add("@title", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@title_k", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@title_e", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@title_c", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@summary", SqlDbType.NText);
        Cmd.Parameters.Add("@subtitle1", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@subtitle2", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@synonym", SqlDbType.NVarChar, 500);
        
        Cmd.Parameters["@Idx"].Value = txtEntryNo.Text;
        Cmd.Parameters["@title"].Value = Server.HtmlEncode(txtTitle.Text);
        Cmd.Parameters["@title_k"].Value = Server.HtmlEncode(txtTitle_K.Text);
        Cmd.Parameters["@title_e"].Value = Server.HtmlEncode(txtTitle_E.Text);
        Cmd.Parameters["@title_c"].Value = Server.HtmlEncode(txtTitle_C.Text);
        Cmd.Parameters["@summary"].Value = Server.HtmlEncode(txtSummary.Text);
        Cmd.Parameters["@subtitle1"].Value = Server.HtmlEncode(txtTitle_Sub1.Text);
        Cmd.Parameters["@subtitle2"].Value = Server.HtmlEncode(txtTitle_Sub2.Text);
        Cmd.Parameters["@synonym"].Value = Server.HtmlEncode(txtSynonym.Text);

        Cmd.CommandText = "USP_Entry_UPDATE";
        Cmd.CommandType = CommandType.StoredProcedure;
        Cmd.ExecuteNonQuery();

        int sortNo = 0;

        Edu edu = JsonConvert.DeserializeObject<Edu>(jsonSaveString);

        List<LUnit> lunitList = edu.LUnit;

        foreach (LUnit lunit in lunitList)
        {
            //대분류 DB에 저장하기
            SqlCommand lunitCommand = null;
            if (Convert.ToInt32(lunit.idx) == -1)
            {
                lunitCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount , TaskIdx) VALUES(" + Convert.ToInt32(txtEntryNo.Text) + ", 0 , " + sortNo + " , '" + lunit.rectype + "' , '' , " + lunit.list.Count + "," + Convert.ToInt32(strTaskIdx) + ")", Con);
                lunitCommand.ExecuteNonQuery();
                sortNo++;

                //제일 마지막에 넣은 것을 가져와서 idx로 설정을 해야 한다.
                lunitCommand = new SqlCommand("SELECT TOP 1 * FROM TEntryData Where EntryIdx=" + Convert.ToInt32(txtEntryNo.Text) + " AND TaskIdx=" + Convert.ToInt32(strTaskIdx) + " ORDER BY idx DESC", Con);
                SqlDataReader sdr = lunitCommand.ExecuteReader(CommandBehavior.SingleResult);
                sdr.Read();
                lunit.idx = Convert.ToString(sdr["idx"]);
                sdr.Close();

            }
            else
            {
                lunitCommand = new SqlCommand("UPDATE TEntryData SET ChildCount=" + lunit.list.Count + " , SortNo=" + sortNo + ",TaskIdx=" + Convert.ToInt32(strTaskIdx) + " WHERE idx =" + lunit.idx, Con);
                lunitCommand.ExecuteNonQuery();
                sortNo++;
            }

            string lunitContent = string.Empty;
            List<Child> lunitChildList = lunit.list;
            foreach (Child secondItem in lunitChildList)
            {

                if (secondItem.content != null)
                {
                    string secondContent = string.Empty;
                    if (Convert.ToString(secondItem.rectype).Equals("MATRIX_TABLE") || 
                        Convert.ToString(secondItem.rectype).Equals("SOUND") ||
                        Convert.ToString(secondItem.rectype).Equals("PAIRSENTENCE") ||
                        Convert.ToString(secondItem.rectype).Equals("VIDEO")

                        )
                    {

                        List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                        string childString = string.Empty;
                        foreach (Child child in childList)
                        {
                            
                            if (child.rectype.Equals(string.Empty))
                            {
                                string childContent = this.convertHtmlToXml(child.content, secondItem.rectype);
                                childString += childContent;
                            }
                            else
                            {
                                string childContent = this.convertHtmlToXml(child.content, child.rectype);
                                childString += "<" + child.rectype + ">" + childContent + "</" + child.rectype + ">";
                            }
                        }

                        secondContent = childString;
                    }
                    else if (Convert.ToString(secondItem.rectype).Equals("IMAGE"))
                    {

                        string caption = string.Empty;
                        string description = string.Empty;
                        string link = string.Empty;

                        List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                        string childString = string.Empty;
                        foreach (Child child in childList)
                        {
                            if (child.rectype.Equals(string.Empty))
                            {
                                string childContent = this.parseImageHtmlToXml(child.content, child.rectype, caption, description, link);
                                childString += childContent;
                            }
                            else
                            {
                                string childContent = this.convertHtmlToXml(child.content, child.rectype);
                                childString += "<" + child.rectype + ">" + childContent + "</" + child.rectype + ">";

                                if (child.rectype.Equals("CAPTION"))
                                {
                                    caption = child.content.Replace("<br>", "<br/>");
                                }
                                else if (child.rectype.Equals("DESCRIPTION"))
                                {
                                    description = child.content.Replace("<br>", "<br/>");
                                }
                                else if (child.rectype.Equals("LINK"))
                                {
                                    link = child.content.Replace("<br/>", string.Empty).Replace("<br>", string.Empty);
                                    link = link.Replace("<p>", string.Empty).Replace("</p>", string.Empty);
                                }
                            }
                        }

                        secondContent = childString;
                    }
                    else
                    {
                        secondContent = convertHtmlToXml(secondItem.content , secondItem.rectype);
                    }


                    lunitContent += "<" + secondItem.rectype + ">" + secondContent + "</" + secondItem.rectype + ">";
                    SqlCommand secondCommand = null;
                    if (Convert.ToInt32(secondItem.idx) == -1)
                    {
                        string insertSql = "INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount , TaskIdx) VALUES(" + Convert.ToInt32(txtEntryNo.Text) + "," + lunit.idx + " , " + sortNo + " , '" + secondItem.rectype + "' , '" + secondContent.Replace("'", "''") + "' , 0," + Convert.ToInt32(strTaskIdx) + ")";
                        secondCommand = new SqlCommand( insertSql , Con);
                        sortNo++;
                    }
                    else
                    {
                        secondCommand = new SqlCommand("UPDATE TEntryData SET SortNo=" + sortNo + ", Content='" + secondContent.Replace("'", "''") + "', TaskIdx=" + Convert.ToInt32(strTaskIdx) + ", ParentIdx=" + lunit.idx + " WHERE idx=" + secondItem.idx, Con);
                        sortNo++;
                    }

                    secondCommand.ExecuteNonQuery();
                }
            }

            lunitCommand = new SqlCommand("UPDATE TEntryData SET Content='" + lunitContent.Replace("'", "''") + "' WHERE idx =" + lunit.idx, Con);
            lunitCommand.ExecuteNonQuery();
        }

        //퀴즈 DB에 넣기
        List<Quiz> quizList = edu.Quiz;
        if (quizList != null)
        {
            foreach (Quiz quiz in quizList)
            {
                SqlCommand quizCommand = null;

                if (Convert.ToInt32(quiz.idx) == -1)
                {
                    quizCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount , TaskIdx) VALUES(" + Convert.ToInt32( txtEntryNo.Text ) + ", 0 , " + sortNo + " , '" + quiz.rectype + "' , '' , " + quiz.list.Count + "," + Convert.ToInt32(strTaskIdx) + ")" , Con);
                    quizCommand.ExecuteNonQuery();
                    sortNo++;

                    //제일 마지막에 넣은 것을 가져와서 idx로 설정을 해야 한다.
                    quizCommand = new SqlCommand("SELECT TOP 1 * FROM TEntryData Where EntryIdx=" + Convert.ToInt32(txtEntryNo.Text) + " AND TaskIdx=" + Convert.ToInt32(strTaskIdx) + " ORDER BY idx DESC", Con);
                    SqlDataReader sdr = quizCommand.ExecuteReader(CommandBehavior.SingleResult);
                    sdr.Read();
                    quiz.idx = Convert.ToString(sdr["idx"]);
                    sdr.Close();

                }
                else
                {
                    quizCommand = new SqlCommand("UPDATE TEntryData SET ChildCount=" + quiz.list.Count + " , SortNo=" + sortNo +",TaskIdx=" + Convert.ToInt32(strTaskIdx) +" WHERE idx =" + quiz.idx, Con);
                    quizCommand.ExecuteNonQuery();
                    sortNo++;
                }

                string quizContent = string.Empty;
                List<Child> quizChildList = quiz.list;
                foreach (Child secondItem in quizChildList)
                {

                    if (secondItem.content != null)
                    {
                        string secondContent = String.Empty;
                        string strRectype = Convert.ToString(secondItem.rectype);
                        if (strRectype.Equals("SOUND") || strRectype.Equals("PAIRSENTENCE") || strRectype.Equals("VIDEO"))
                        {
                            List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                            string childString = string.Empty;
                            foreach (Child child in childList)
                            {

                                if (child.rectype.Equals(string.Empty))
                                {
                                    string childContent = this.convertHtmlToXml(child.content, secondItem.rectype);
                                    childString += childContent;
                                }
                                else
                                {
                                    string childContent = this.convertHtmlToXml(child.content, child.rectype);
                                    childString += "<" + child.rectype + ">" + childContent + "</" + child.rectype + ">";
                                }
                            }

                            secondContent = childString;
                        }
                        else
                        {
                            secondContent = convertHtmlToXml(secondItem.content, secondItem.rectype);
                        }


                        quizContent += "<" + secondItem.rectype + ">" + secondContent + "</" + secondItem.rectype + ">";
                        SqlCommand secondCommand = null;
                        if (Convert.ToInt32(secondItem.idx) == -1)
                        {
                            string insertSql = "INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount , TaskIdx) VALUES(" + Convert.ToInt32(txtEntryNo.Text) + "," + quiz.idx + " , " + sortNo + " , '" + secondItem.rectype + "' , '" + secondContent.Replace("'", "''") + "',0," + Convert.ToInt32(strTaskIdx) + ")";
                            secondCommand = new SqlCommand(insertSql , Con);
                            sortNo++;
                        }
                        else
                        {
                            secondCommand = new SqlCommand("UPDATE TEntryData SET SortNo=" + sortNo + ", Content='" + secondContent.Replace("'", "''") + "', TaskIdx=" + Convert.ToInt32(strTaskIdx) + ", ParentIdx=" + quiz.idx + " WHERE idx=" + secondItem.idx, Con);
                            sortNo++;
                        }

                        secondCommand.ExecuteNonQuery();
                    }
                }

                quizCommand = new SqlCommand("UPDATE TEntryData SET Content='" + quizContent.Replace("'", "''") + "' WHERE idx =" + quiz.idx, Con);
                quizCommand.ExecuteNonQuery();
            }
        }

        //태그, 수정한사람 DB에 넣기
        Tag tag = edu.Tag;
        if (tag != null)
        {
            SqlCommand tagCommand = new SqlCommand("Update TEntry SET Tag='" + tag.content.Replace("'", "''") + "', EditDate=getdate(), EditorIdx="+ Session["uidx"].ToString() +" Where idx =" + tag.idx, Con);
            tagCommand.ExecuteNonQuery();
        }

        Con.Close();

        string script = "alert(\"저장되었습니다.\");";
        ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
    }

    private void removeEntryData(string deleteIDStrings) {
        string[] splitedDeleteIDString = deleteIDStrings.Split(',');
        foreach (string deleteID in splitedDeleteIDString)
        {
            if (deleteID.Trim().Length != 0) 
            {
                this.deleteEntry(Convert.ToInt32(deleteID));
            }
        }
    }

    private void removeEntry(string strDeleteEntry)
    {
        try
        {
            int delID = Convert.ToInt32(strDeleteEntry);

            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);

            SqlCommand Cmd = new SqlCommand("UPDATE TEntry SET DelFlag=1 WHERE Idx = " + delID + "; UPDATE TTaskID SET EntryCount=EntryCount-1 WHERE Idx=" + strTaskIdx + "", Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();

            Cmd = null;
            Con = null;
            Response.Write("<script>alert('삭제되었습니다.');</script>");
        }
        catch (Exception ex)
        {
            Response.Write("엔트리 삭제 중에 오류가 발생했습니다.<br/> 오류내용:" + ex.ToString());
        }
    }



    private void deleteEntry(int deleteID) {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlCommand deleteCommand = new SqlCommand("UPDATE TEntryData SET DelFlag=1 WHERE Idx=" + deleteID, Con);
        deleteCommand.ExecuteNonQuery();

        if (deleteID > 0 && deleteID != null)
        {
            SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx FROM TEntryData WHERE ParentIdx=" + deleteID, Con);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow entryDatarow in dt.Rows)
                {
                    deleteEntry(Convert.ToInt32(entryDatarow["Idx"]));
                }
            }
        }
        if (Con.State == ConnectionState.Open)
            Con.Close();
    }

    private void deleteEntryFile()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        int entryIdx = Convert.ToInt32(txtEntryNo.Text);
        SqlCommand deleteCommand = new SqlCommand("DELETE FROM TFileData WHERE EntryIdx=" + entryIdx , Con);
        deleteCommand.ExecuteNonQuery();
        Con.Close();
    }

    private void saveEntryFile(String fileName , string type)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();

        int entryIdx = Convert.ToInt32(txtEntryNo.Text);
        int tIdx = Convert.ToInt32(strTaskIdx);
        string strQuery = "INSERT INTO TFileData(FileName , TaskIdx , EntryIdx , FileType) VALUES('" + fileName + "'," + tIdx + "," + entryIdx + ",'" + type + "')";
        SqlCommand deleteCommand = new SqlCommand(strQuery, Con);
        deleteCommand.ExecuteNonQuery();
        Con.Close();
    }

    private void saveEntryFile(string fileName, string type, string caption, string description, string link)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();

        int entryIdx = Convert.ToInt32(txtEntryNo.Text);
        int tIdx = Convert.ToInt32(strTaskIdx);
        string strQuery = "INSERT INTO TFileData(FileName , TaskIdx , EntryIdx, FileType , FilePath , Description , Caption ) VALUES('" + fileName + "'," + tIdx + "," + entryIdx + ",'" + type + "' , '" + link.Replace("'", "''") + "','" + description.Replace("'", "''") + "','" + caption.Replace("'", "''") + "')";

        SqlCommand saveCommand = new SqlCommand(strQuery, Con);
        saveCommand.ExecuteNonQuery();
        Con.Close();
    }

    private string saveImageFileWithDataType(string fileName, string dataType)
    {

        string result = string.Empty;

        string newFileName = fileName.Replace("_sym1", string.Empty).Replace("_sym2", string.Empty).Replace("_sym3", string.Empty).Replace("_han", string.Empty);

        if (!dataType.Equals(string.Empty))
        {

            int dotIndex = newFileName.LastIndexOf(".");
            newFileName = newFileName.Insert(dotIndex, "_" + dataType);
        }

        if (!fileName.Equals(newFileName))
        {
            string originalFilePath = Server.MapPath("../") + "\\CMS100Data\\EntryData\\" + strTaskIdx + "\\" + fileName;
            string newFilePath = Server.MapPath("../") + "\\CMS100Data\\EntryData\\" + strTaskIdx + "\\" + newFileName;

            File.Copy(originalFilePath, newFilePath, true);
            //File.Delete(originalFilePath);

            result = newFileName;
        }
        else
        {
            result = fileName;
        }

        return result;
    }


    private string parseImageHtmlToXml(string strHtml, string recType, string caption, string description, string link)
    {
        //공백을 없앤다.
        strHtml = strHtml.Replace("&nbsp;", " ");

        //img 태그와 br 태그의 닫는 태그를 넣는다.
        strHtml = strHtml.Replace("<br>", "<br/>");
        int imgStartIndex = 0;
        int imgEndIndex = 0;

        imgStartIndex = strHtml.IndexOf("<img", 0);
        while (imgStartIndex > -1)
        {

            imgEndIndex = strHtml.IndexOf(">", imgStartIndex);
            strHtml = strHtml.Insert(imgEndIndex, "/");
            imgStartIndex = strHtml.IndexOf("<img", imgEndIndex);

        }

        //이미지의 이상한 문자를 지운다.
        StringBuilder sb = new StringBuilder(strHtml);
        sb.Replace(char.ConvertFromUtf32(8203), string.Empty);
        strHtml = sb.ToString();

        //div 태그가 있는지 확인한다.
        //div 태그가 있다면 정렬을 가져온다.
        string strAlign = string.Empty;
        XmlDocument divXml = new XmlDocument();
        divXml.LoadXml("<edu>" + strHtml + "</edu>");

        XmlNodeList divNodeList = divXml.GetElementsByTagName("div");
        if (divNodeList.Count > 0)
        {
            foreach (XmlNode divNode in divNodeList)
            {
                XmlAttributeCollection attributes = divNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Value.Contains("text-align"))
                    {
                        strAlign = "<ALIGN>" + attribute.Value.Replace("text-align:", "").Replace(";", string.Empty).Trim() + "</ALIGN>";
                    }
                }
            }


            int divStartIndex = strHtml.IndexOf("<div", 0);
            while (divStartIndex > -1)
            {
                int divEndIndex = strHtml.IndexOf(">", divStartIndex) + 1;
                strHtml = strHtml.Remove(divStartIndex, divEndIndex - divStartIndex);
                divStartIndex = strHtml.IndexOf("<div", divStartIndex);
            }
            //div 태그를 지운다.
            strHtml = strHtml.Replace("</div>", string.Empty);
        }


        //링크 태그의 쓰레기 속성을 지운다.
        XmlDocument xml = new XmlDocument();
        xml.PreserveWhitespace = true;
        xml.LoadXml("<edu>" + strHtml + "</edu>");


        //이미지 태그의 쓰레기 속성을 지운다.
        XmlNodeList imgNodeList = xml.GetElementsByTagName("img");

        if (imgNodeList.Count > 0)
        {

            foreach (XmlNode imgNode in imgNodeList)
            {
                XmlAttributeCollection attributes = imgNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Name.Contains("data-cke-saved-src"))
                    {
                        attributes.Remove(attribute);
                        break;
                    }
                }

                attributes = imgNode.Attributes;
                foreach (XmlAttribute attribute in attributes)
                {
                    if (attribute.Name.Contains("alt"))
                    {
                        attributes.Remove(attribute);
                        break;
                    }
                }
            }
        }


        //문자열로 출력시킨다.
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.OmitXmlDeclaration = true;
        using (var stringWriter = new StringWriter())
        using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
        {
            xml.WriteTo(xmlTextWriter);
            xmlTextWriter.Flush();
            strHtml = stringWriter.GetStringBuilder().ToString();
        }

        strHtml = strHtml.Replace("<edu>", string.Empty);
        strHtml = strHtml.Replace("</edu>", string.Empty);

        //태그를 파싱한다.
        string strConvert = string.Empty;
        if (strAlign.Equals(string.Empty))
        {
            strAlign = "<ALIGN></ALIGN>";
        }

        StringWriter imgStringWriter = new StringWriter();
        XmlTextWriter xmlWriter = new XmlTextWriter(imgStringWriter);
        string imgResult = strHtml;
        imgResult = "<edu>" + imgResult + "</edu>";

        string strText = string.Empty;
        Boolean startedImageTage = false;

        using (XmlReader reader = XmlReader.Create(new StringReader(imgResult)))
        {
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Text:
                        if (startedImageTage)
                        {
                            xmlWriter.WriteEndElement();
                            startedImageTage = false;
                        }

                        strText += reader.Value;

                        break;
                    case XmlNodeType.Element:

                        switch (reader.Name)
                        {
                            case "img":
                                if (strText != string.Empty)
                                {
                                    xmlWriter.WriteStartElement("TEXT");
                                    xmlWriter.WriteCData(strText);
                                    xmlWriter.WriteEndElement();
                                    strText = string.Empty;
                                }

                                if (!startedImageTage)
                                {
                                    xmlWriter.WriteStartElement("IMGS");
                                    startedImageTage = true;
                                }

                                xmlWriter.WriteStartElement("IMG");

                                string imgHeight = string.Empty;
                                string imgWidth = string.Empty;
                                string imgStyle = reader.GetAttribute("style");
                                string fileSrc = reader.GetAttribute("src");

                                if (imgStyle != null)
                                {
                                    string[] splitedImgStyle = imgStyle.Split(new char[] { ';' });

                                    foreach (string attribute in splitedImgStyle)
                                    {
                                        if (attribute.Contains("width"))
                                        {
                                            imgWidth = attribute.Replace("width:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                        else if (attribute.Contains("height"))
                                        {
                                            imgHeight = attribute.Replace("height:", string.Empty).Replace("px", string.Empty).Trim();
                                        }
                                    }
                                }

                                xmlWriter.WriteStartElement("FILENAME");
                                string[] splitedFileName = fileSrc.Split(new char[] { '/' });
                                string fileName = splitedFileName[splitedFileName.Length - 1];

                                if (fileName != null && !fileName.Equals(string.Empty))
                                {
                                    this.saveEntryFile(fileName, "IMG", caption, description, link);
                                }
                                xmlWriter.WriteValue(fileName);
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteRaw("<WIDTH>" + imgWidth + "</WIDTH>");

                                xmlWriter.WriteRaw("<HEIGHT>" + imgHeight + "</HEIGHT>");
                                xmlWriter.WriteEndElement();
                                break;
                        }

                        break;
                    case XmlNodeType.EndElement:

                        switch (reader.Name)
                        {
                            case "img":
                                xmlWriter.WriteEndElement();
                                break;
                        }
                        break;
                }
            }
        }

        StringBuilder stringBuilder = new StringBuilder(strText);
        stringBuilder.Replace(char.ConvertFromUtf32(8203), string.Empty);
        strText = stringBuilder.ToString();

        if (strText != string.Empty)
        {
            xmlWriter.WriteStartElement("TEXT");
            xmlWriter.WriteCData(strText);
            xmlWriter.WriteEndElement();
        }
        if (startedImageTage) xmlWriter.WriteEndElement();

        return imgStringWriter.ToString() + strAlign;
    }

    public class Edu
    {
        [JsonProperty("lunit")]
        public List<LUnit> LUnit { get; set; }
        [JsonProperty("quiz")]
        public List<Quiz> Quiz { get; set; }
        [JsonProperty("tag")]
        public Tag Tag { get; set; }
    }


    public class Tag
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }
    }

    public class LUnit
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("list")]
        public List<Child> list { get; set; }
    }

    public class Quiz
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("list")]
        public List<Child> list { get; set; }
    }



    public class Child
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }
    }


    protected void btnCheck_Click(object sender, EventArgs e)
    {
        try
        {
            if (EntryIdx.Length == 0)
                EntryIdx = txtEntryNo.Text;

            string strQuery = "UPDATE TEntry SET Status=3, CheckDate=getdate(), EditDate=getdate() WHERE Idx=" + EntryIdx + "; UPDATE TEntryData SET Status=3, CheckDate=getdate() WHERE EntryIdx=" + EntryIdx + "";

            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
            //sectionBtn.Visible = false;
            GetEntryList();
            
        }
        catch(Exception ex)
        {
            Response.Write("작업완료 처리 중에 오류가 발생했습니다.<br/> 오류내용:"+ ex.ToString());
        }
    }

    protected void btnFinish_Click(object sender, EventArgs e)
    {
        try
        {
            string strQuery = "UPDATE TTaskID SET Status=3, CheckDate=getdate() WHERE Idx=" + strTaskIdx + "; UPDATE TEntry SET Status=3, CheckDate=getdate(), EditDate=getdate() WHERE TaskIdx=" + strTaskIdx + "; UPDATE TEntryData SET Status=3, CheckDate=getdate(), EditDate=getdate() WHERE TaskIdx=" + strTaskIdx + "";

            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
            sectionBtn.Visible = false;
            btnFinish.Visible = false;
            btnImport.Visible = false;
            GetTaskInfo();
            GetEntryList();
        }
        catch (Exception ex)
        {
            Response.Write("최종작업완료 처리 중에 오류가 발생했습니다.<br/> 오류내용:" + ex.ToString());
        }

        Response.Write("<script>alert('검수가 요청되었습니다.');</script>");
    }
    protected void EntryList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            string delID;
            //RepeaterItem repeaterItem = EntryList.Items[e.Item.ItemIndex];
            HiddenField txtIDX = e.Item.FindControl("txtIDX") as HiddenField;
            delID = txtIDX.Value;

            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);

            SqlCommand Cmd = new SqlCommand("UPDATE TEntry SET DelFlag=1 WHERE Idx = " + delID + "; UPDATE TTaskID SET EntryCount=EntryCount-1 WHERE Idx=" + strTaskIdx + "", Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();

            Cmd = null;
            Con = null;
            Response.Write("<script>alert('삭제되었습니다.');</script>");
            GetEntryList();
            GetTaskInfo();
        }
        catch (Exception ex)
        {
            Response.Write("엔트리 삭제 중에 오류가 발생했습니다.<br/> 오류내용:" + ex.ToString());
        }
    }
    protected void btnImport_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.open('ImportXML.aspx?taskIdx=" + strTaskIdx + "','popup','width=960,height=620, scrollbars=yes');</script>");                
    }
    protected void btnImport2_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.open('ImportXML2.aspx?taskIdx=" + strTaskIdx + "','popup','width=960,height=620, scrollbars=yes');</script>");                

    }

    public string str2hex(string strData)
    {
        string resultHex = string.Empty;
        byte[] arr_byteStr = Encoding.Default.GetBytes(strData);

        foreach (byte byteStr in arr_byteStr)
            resultHex += string.Format("{0:X2}", byteStr);

        return resultHex;
    }

}