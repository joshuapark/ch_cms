﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="EntryList.aspx.cs" Inherits="notice_List" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>
<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">

    
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li class="active"><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
                        <li><a href="DaumExportList.aspx">다음Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
                        <li><a href="OutlineList.aspx">개요부관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->
    <form id="searchForm" runat="server">


	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">엔트리조회</h2>
		</div><!-- // title -->	

		<table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
		</colgroup>
  
		<tbody>
			<tr>
				<th>개정</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlRevision" runat="server"></asp:DropDownList>
					</div>
				</td>
				<th>과목</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlSubject" runat="server"></asp:DropDownList>					
					</div>
				</td>
                <th>브랜드</th>
				<td>
					<div class="de-select" id="divBrand">
                        <asp:DropDownList class="de-select" ID="ddlBrand" runat="server"></asp:DropDownList>					
					</div>
				</td>
                <th >학년</th>
				<td>
					<div class="de-select" id="divGrade">
                        <asp:DropDownList class="de-select" ID="ddlGrade" runat="server"></asp:DropDownList>					
					</div>			
				</td>
				<th >학기</th>
				<td>
					<div class="de-select" id="divSemester">
                        <asp:DropDownList class="de-select" ID="ddlSemester" runat="server"></asp:DropDownList>					
					</div>		
				</td>
            </tr>
            <tr>
				<th onclick="$('#divLUnit').toggle();">대단원</th>
                <td colspan="2">
					<asp:Button runat="server" ID="LUnitSearch" class="btn btn-sm btn-success" text="조회" OnClick="LUnitSearch_Click" />
                    <div class="de-select" id="divLUnit">
                        <asp:DropDownList class="de-select" ID="ddlLUnit" runat="server"></asp:DropDownList>					
					</div>	
                </td>
				<th onclick="$('#divMUnit').toggle();">중단원</th>
                <td colspan="2">
					<asp:Button runat="server" ID="MUnitSearch" class="btn btn-sm btn-success" text="조회" OnClick="MUnitSearch_Click" />
                    <div class="de-select" id="divMUnit">
                        <asp:DropDownList class="de-select" ID="ddlMUnit" runat="server"></asp:DropDownList>					
					</div>	
                </td>
				<th>키워드</th>
                <td colspan="3">
					<asp:TextBox runat="server" ID="txtKeyWord" class="large" />
                </td>
            </tr>
            <tr>
				<th onclick="$('#divUser').toggle();">작업자</th>
                <td colspan="2">
                    <div class="de-select" id="divUser">
                        <asp:DropDownList class="de-select" ID="ddlUser" runat="server"></asp:DropDownList>					
					</div>	
                </td>
				<th onclick="$('#divTask').toggle();">작업명</th>
                <td colspan="6">
                    <asp:Button runat="server" ID="TaskSearch" class="btn btn-sm btn-success" text="조회" OnClick="TaskSearch_Click" />
                    <div class="de-select" id="divTask">
                        <asp:DropDownList class="de-select" ID="ddlTask" runat="server"></asp:DropDownList>					
					</div>	
                </td>
            </tr>
		</tbody>
		</table><!-- // table-a -->

		<div class="section-button"><!-- section-button -->
			<asp:Button runat="server" ID="SearchButton" class="btn btn-lg btn-success" text="조회" OnClick="SearchButton_Click"/>
		</div><!-- // section-button -->
		<div class="section-button"><!-- section-button -->
			<div class="pull-right">
                <asp:Button runat="server" ID="DownButton" class="btn btn-sm btn-danger" text="다운로드" OnClick="DownButton_Click" />
			</div>
		</div><!-- // section-button -->
        
        <asp:Label ID="lblTotalCnt" runat="server" />
		<table border="0" class="table table-list" style="padding:0; border-spacing:0">
            <asp:ListView ID="LV_Notice" runat="server" ItemPlaceholderID="phItemList" 
                    OnItemDataBound="ListView_ItemDataBound" >
                    <LayoutTemplate>
                        <colgroup>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>                            
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                        </colgroup>
                        <tr>
				            <th>No</th>
				            <th>개정</th>
				            <th>과목</th>
				            <th>브랜드</th>
				            <th>학년</th>
				            <th>학기</th>
				            <th>대단원</th>
				            <th>중단원</th>
				            <th>타이틀</th>
				            <th>작업자</th>
				            <th>검수자</th>
                        </tr>
                        <asp:PlaceHolder ID="phItemList" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblNo" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblRevision" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblSubject" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblBrand" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblGrade" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblSemester" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblLUnit" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblMUnit" runat="server" />
                            </td>
                            <td>
                                <asp:HyperLink ID="lblTitle" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblUser" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblChecker" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <colgroup>
                            <col/>
                            <col/>
                            <col/>                            
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                        </colgroup>
                        <tr>
				            <th>No</th>
				            <th>개정</th>
				            <th>과목</th>
				            <th>브랜드</th>
				            <th>학년</th>
				            <th>학기</th>
				            <th>대단원</th>
				            <th>중단원</th>
				            <th>타이틀</th>
				            <th>작업자</th>
				            <th>검수자</th>
                        </tr>
                        <tr>
                            <td colspan="11">등록된 엔트리가 없습니다.</td>
                        </tr>
                    </EmptyDataTemplate>
            </asp:ListView>
        </table>
        <div style="width:100%;text-align:center;">
		<div class="list-inline" id="pagepart" runat="server">


<%--            <ul class="pagination">
                <asp:HyperLink ID="hlPagePrev" runat="server" />
                    
                <%=lblPage %>
                    
                <asp:HyperLink ID="hlPageNext" runat="server" />
            </ul>--%>
        </div> 
        </div>
            <input type="hidden" id="txtPageNo" name="txtPageNo" runat="server"  />
            <input type="hidden" id="txtTotalCount" runat="server" />
            <input type="hidden" id="txtTotalPage" runat="server" />
	</div><!-- // contents -->
    
    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script type="text/javascript">
    function ChangeTxtPageNo(value)
    {
        $('#txtPageNo').val(value);
        return true;
    }
</script>
</body>
</html>