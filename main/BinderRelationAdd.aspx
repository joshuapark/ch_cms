﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BinderRelationAdd.aspx.cs" Inherits="main_BinderRelationAdd" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <script src="../js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript">
        function checkClick() {
            if ($("#chkAll").prop("checked")) {
                $("input[type=checkbox]").each(function () {
                    $(this).attr("checked", true);
                });
            } else {
                $("input[type=checkbox]").each(function () {
                    $(this).attr("checked", false);
                });
            }
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="title"><!-- title -->
		    <h3 class="title title-primary"><asp:Label ID="lblTypeTitle" runat="server"></asp:Label> 추가</h3>
	    </div><!-- // title -->
        <form id="form1" runat="server">
            <asp:HiddenField ID="txtBinderIdx" runat="server" />
            <asp:HiddenField ID="txtRelation" runat="server" />
        <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<tbody>
			<tr>
                <td><asp:TextBox ID="txtKeyword" runat="server" CssClass="large"></asp:TextBox></td>
                <td><asp:Button runat="server" ID="SearchButton" class="btn btn-sm btn-success" text="조회" OnClick="SearchButton_Click"/></td>
            </tr>
		</tbody>
		</table><!-- // table-a -->
            <asp:Label ID="lblTotalCount" runat="server"></asp:Label>
            <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
                <asp:Repeater ID="rpRelationList" runat="server">
                <HeaderTemplate>
                    <colgroup>
                        <col style="width: 5%;">
                        <col style="width: 5%;">
			            <col style="width: 25%;">
			            <col style="width: 35%;">
			            <col style="width: 10%;">
                        <col style="width: 15%;">
		            </colgroup>
                    <tr>
                        <th><input type="checkbox" id="chkAll" onclick="javascript:checkClick();" /></th>
				        <th>No</th>
				        <th>바인더명</th>
				        <th>바인더설명</th>
				        <th>엔트리수</th>
                        <th>최종수정일</th>
                    </tr>
<%
    if (rpRelationList.Items.Count == 0)
    {
%>
                    <tr>
                        <td colspan="6" style="text-align:center">조회된 바인더가 없습니다.</td>
                    </tr>                    
<%
    }
%>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="singleCheckbox">
                            <asp:CheckBox ID="chkID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem , "BinderIdx")%>'  />
                        </td>
                        <td>
                            <asp:Label ID="lblNo" Text='<%#DataBinder.Eval(Container.DataItem , "BinderIdx")%>' runat="server"/>
                        </td>
                        <td>
                            <asp:Label ID="lblBinderTitle" Text=<%#DataBinder.Eval(Container.DataItem , "BinderTitle")%> runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblBinderMemo" Text=<%#DataBinder.Eval(Container.DataItem , "BinderMemo")%> runat="server" />
                        </td>
                        <td style="text-align:right">
                            <asp:Label ID="lblEntryCount" Text=<%#DataBinder.Eval(Container.DataItem , "EntryCount")%> runat="server" />
                        </td>
                        <td style="text-align:center">
                            <asp:Label ID="lblEditDate" Text=<%#DataBinder.Eval(Container.DataItem , "EditDate")%> runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </table><!-- // table-a -->
            <div class="section-button"><!-- section-button -->
			<asp:Button runat="server" ID="SaveButton" class="btn btn-lg btn-success" text="저장" OnClick="SaveButton_Click"/>
		    </div><!-- // section-button -->
        </form>
    </div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/jquery-1.4.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/jquery.js"></script>
</body>

</html>
