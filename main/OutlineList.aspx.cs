﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Drawing;

public partial class main_List : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private bool _refreshState;
    private bool _isRefresh;

    public bool IsRefresh
    {
        get { return _isRefresh; }
    }
    protected override void LoadViewState(object savedState)
    {
        object[] allStates = (object[])savedState;
        base.LoadViewState(allStates[0]);
        _refreshState = (bool)allStates[1];
        _isRefresh = _refreshState == (bool)Session["__ISREFRESH"];
    }
    protected override object SaveViewState()
    {
        Session["__ISREFRESH"] = _refreshState;
        object[] allStates = new object[2];
        allStates[0] = base.SaveViewState();
        allStates[1] = !_refreshState;
        return allStates;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  


        if (!IsPostBack)
        {
            OutlineTypeListing();

            OutlineTypeBinding();

        }

    }

    protected void OutlineTypeBinding()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand("SELECT OutlineType, OutlineTypeName FROM TOutlineType WHERE DelFlag=0 ORDER BY OutlineTypeName ASC", Conn);
        Conn.Open();
        SqlDataReader Reader = Cmd.ExecuteReader();

        //Set up the data binding.
        ddlOutlineType.DataSource = Reader;
        ddlOutlineType.DataTextField = "OutlineTypeName";
        ddlOutlineType.DataValueField = "OutlineType";
        ddlOutlineType.DataBind();

        //Close the connection.
        Conn.Close();
        Reader.Close();


    }
    protected void OutlineTypeListing()
    {
        // DB Connection String, File Path 설정
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.CommandText = "USP_OutlineType_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        OutlineTypeListRepeater.DataSource = ds;
        OutlineTypeListRepeater.DataBind();

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

    }
    protected void OutlineListing(string outlineTypeID)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.CommandText = "USP_Outline_SELECT";
        Cmd.Parameters.Add("@OutlineType", SqlDbType.Int);
        Cmd.Parameters["@OutlineType"].Value = outlineTypeID;
        Cmd.CommandType = CommandType.StoredProcedure;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "status_list");

        OutlineTableRepeater.DataSource = ds;
        OutlineTableRepeater.DataBind();
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        if (txtOutlineType.Text.Trim().Length > 1)  //코드타입명을 입력했는지 체크한다.
        {
            DBFileInfo();
            SqlConnection Con0 = new SqlConnection(connectionString);
            SqlCommand Cmd0 = new SqlCommand("SELECT OutlineType FROM TOutlineType WHERE OutlineTypeName='" + txtOutlineType.Text.Trim() + "' AND DelFlag=0 ", Con0);
            Cmd0.CommandType = CommandType.Text;
            Con0.Open();

            SqlDataReader reader = Cmd0.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                //msgIDCheck.InnerText = "* " + txtOutlineType.Text.Trim() + " 코드타입은 이미 등록되어 있습니다. 다른 코드타입을 입력하세요.";
                string script = "<script>alert('" + txtOutlineType.Text.Trim() + " 코드타입은 이미 등록되어 있습니다.');</script>";
                txtOutlineType.Text = "";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                Con0.Close();
                Con0 = null;
                Cmd0 = null;
                Response.Write(script);
            }
            else
            {
                SqlConnection Con = new SqlConnection(connectionString);

                SqlCommand Cmd = new SqlCommand("INSERT INTO TOutlineType (OutlineTypeName) VALUES ( '" + txtOutlineType.Text.Trim() + "')", Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                Cmd = null;
                Con = null;
                txtOutlineType.Text = "";
                selectedOutlineTypeName.Text = "";
                selectedOutlineType.Value = "";

                OutlineTypeListing();
                //Response.Redirect("OutlineList.aspx");
                //string script = "<script>alert('저장되었습니다');location.href='OutlineList.aspx'</script>";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            }
        }
    }

    protected void OutlineTypeListRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //string delID;
        //Button clickedButton = (Button)sender;
        //delID = clickedButton.ID;
        Control ctl = source as Control;
        var argument = e.CommandArgument;
        
        //CommandEventArgs cea = e as CommandEventArgs;
        if (ctl != null)
        {
            if(argument.ToString()=="D")
            {
                RepeaterItem repeaterItem = OutlineTypeListRepeater.Items[e.Item.ItemIndex];
                HiddenField hfID = repeaterItem.FindControl("OutlineID") as HiddenField;
                string delID = hfID.Value;

                DBFileInfo();

                SqlConnection Con = new SqlConnection(connectionString);

                SqlCommand Cmd = new SqlCommand("UPDATE TOutlineType SET DelFlag=1	WHERE OutlineType = " + delID, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                Cmd = null;
                Con = null;

                string script = "<script>alert('삭제되었습니다');</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);

                OutlineTypeListing();
            }
            else if (argument.ToString()=="G")
            {
                RepeaterItem repeaterItem = OutlineTypeListRepeater.Items[e.Item.ItemIndex];
                HiddenField hfID = repeaterItem.FindControl("OutlineID") as HiddenField;
                TextBox txtOutlineTypeName = repeaterItem.FindControl("txtOutlineTypeName") as TextBox;
                string outlineTypeID = hfID.Value;

                selectedOutlineTypeName.Text = txtOutlineTypeName.Text;
                selectedOutlineType.Value = outlineTypeID;
                OutlineTypeBinding();
                ddlOutlineType.SelectedValue = outlineTypeID;

                OutlineListing(outlineTypeID);

            }
        }
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];

        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void btnSaveOutlineType_Click(object sender, EventArgs e)
    {
        try
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();
            SqlCommand Cmd = null;

            foreach (RepeaterItem item in OutlineTypeListRepeater.Items)
            {
                HiddenField txtIDX = item.FindControl("OutlineID") as HiddenField;
                TextBox txtOutlineTypeName = item.FindControl("txtOutlineTypeName") as TextBox;
                if (txtOutlineTypeName.Text.Length > 0)
                {
                    Cmd = new SqlCommand("UPDATE TOutlineType SET OutlineTypeName='" + txtOutlineTypeName.Text + "' WHERE OutlineType=" + txtIDX.Value + " ", Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.ExecuteNonQuery();
                }
            }

            Con.Close();

            Cmd = null;
            Con = null;

            Response.Write("<script>alert('저장되었습니다.');</script>");
        }
        catch (Exception ex)
        {
            Response.Write("코드타입 저장 중에 오류가 발생했습니다.<br />오류내용"+ ex.ToString());
        }

    }
    protected void btnSaveOutline_Click(object sender, EventArgs e)
    {
        try
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();
            SqlCommand Cmd = null;

            foreach (RepeaterItem item in OutlineTableRepeater.Items)
            {
                HiddenField txtIDX = item.FindControl("OutlineIdx") as HiddenField;
                TextBox txtOutlineName = item.FindControl("OutlineName") as TextBox;
                if (txtOutlineName.Text.Length > 0)
                {
                    Cmd = new SqlCommand("UPDATE TOutline SET OutlineName='" + txtOutlineName.Text + "' WHERE Idx=" + txtIDX.Value + " ", Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.ExecuteNonQuery();
                }
            }

            Con.Close();

            Cmd = null;
            Con = null;

            Response.Write("<script>alert('저장되었습니다.');</script>");
        }
        catch (Exception ex)
        {
            Response.Write("코드타입 저장 중에 오류가 발생했습니다.<br />오류내용" + ex.ToString());
        }
    }
    protected void btnOutlineAdd_Click(object sender, EventArgs e)
    {
        if (txtOutlineName.Text.Trim().Length > 0)  //코드명을 입력했는지 체크한다.
        {
            DBFileInfo();
            SqlConnection Con0 = new SqlConnection(connectionString);
            SqlCommand Cmd0 = new SqlCommand("SELECT OutlineName FROM TOutline WHERE OutlineType=" + ddlOutlineType.SelectedValue + " AND OutlineName='" + txtOutlineName.Text.Trim() + "' AND DelFlag=0 ", Con0);
            Cmd0.CommandType = CommandType.Text;
            Con0.Open();

            SqlDataReader reader = Cmd0.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                //msgIDCheck2.InnerText = "* " + txtOutline.Text.Trim() + " 코드는 이미 등록되어 있습니다. 다른 코드를 입력하세요.";
                string script = "<script>alert('" + txtOutlineName.Text.Trim() + " 코드명은 이미 등록되어 있습니다.');</script>";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                Con0.Close();
                Con0 = null;
                Cmd0 = null;
                txtOutline.Text = "";
                txtOutlineName.Text = "";
                Response.Write(script);
            }
            else
            {
                SqlConnection Con = new SqlConnection(connectionString);

                SqlCommand Cmd = new SqlCommand("USP_Outline_INSERT", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@OutlineType", SqlDbType.Int);
                Cmd.Parameters.Add("@OutlineName", SqlDbType.VarChar, 255);

                Cmd.Parameters["@OutlineType"].Value = ddlOutlineType.SelectedValue;
                Cmd.Parameters["@OutlineName"].Value = txtOutlineName.Text;

                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                Cmd = null;
                Con = null;

                txtOutlineName.Text = "";
                txtOutline.Text = "";

                //string script = "<script>alert('저장되었습니다');location.href='OutlineView.aspx?OutlineType="+paramOutlineType+"'</script>";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                //Response.Redirect("OutlineView.aspx?OutlineType=" + paramOutlineType + "");
                selectedOutlineTypeName.Text = ddlOutlineType.SelectedItem.Text;
                selectedOutlineType.Value = ddlOutlineType.SelectedValue;

                OutlineListing(ddlOutlineType.SelectedValue);
            }
        }
        txtOutline.Text = "";
        txtOutlineName.Text = "";
    }
    protected void OutlineTableRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string delID;
        RepeaterItem repeaterItem = OutlineTableRepeater.Items[e.Item.ItemIndex];
        HiddenField hfID = repeaterItem.FindControl("OutlineIdx") as HiddenField;
        delID = hfID.Value;


        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand("UPDATE TOutline SET DelFlag=1	WHERE Idx = '" + delID + "'", Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

        string script = "<script>alert('삭제되었습니다');</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);

        OutlineListing(selectedOutlineType.Value);
    }
}