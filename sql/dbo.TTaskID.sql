﻿CREATE TABLE [dbo].[TTaskID] (
    [Idx]         INT            IDENTITY (1, 1) NOT NULL,
    [TaskID]      VARCHAR (50)   NULL,
    [Category]    NCHAR (10)     NULL,
    [UserId]      VARCHAR (20)   NULL,
    [ManagerId]      VARCHAR (20)   NULL,
    [TaskTitle]   NVARCHAR (255) NULL,
    [TaskContent] NTEXT          NULL,
    [InsertDate]  DATETIME       DEFAULT (getdate()) NOT NULL,
    [Status]     INT            DEFAULT ((0)) NOT NULL, -- 0:임시, 1:할당, 2:진행중 3:완료 4:검수중 5:검수완료
    [DelFlag]     BIT            DEFAULT ((0)) NOT NULL
);

