﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignIn_Finish.aspx.cs" Inherits="main_MyInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>iCareD 관리자 사이트</title>
    <link rel="shortcut icon" href="images/favicon.png" /> 
    <link href="css/bootstrap.css" rel="stylesheet" />
</head>
<body>
	<nav class="navbar navbar-inverse">
	    <div class="container">
    	    <div class="navbar-header">
			    <a class="navbar-brand">iCareD 회원가입 </a> 
		    </div>
		    <div class="collapse navbar-collapse">
			    <ul class="nav navbar-nav">
				    <li><a>환영합니다! </a></li>		
			    </ul>	
		    </div>		
        </div>
	</nav>
	<div class="container">
		<div class="row" >
			<nav class="navbar navbar-default navbar-static-top" style="padding: 15px;">
				<ul class="nav nav-pills nav-justified">	
					<li><a>약관 및 정보이용 동의 </a></li>
					<li ><a>회원정보 입력 </a></li>
					<li class="active" ><a>가입 완료</a></li>
				</ul>				
			</nav>
		</div>

		<div class="row">
			<div class="alert alert-success" role="alert">
		    	감사합니다. <br />
			    회원 가입 요청이 완료 되었습니다. <br />
			    관리자의 확인 후 승인이 완료되면 등록하신 메일 주소로 확인 안내 메일이 발송됩니다.
			</div>
        </div>
		<div class="row">
			<center>	
			    <button type="button" class="btn btn-primary btn-lg" onclick="window.close();">가입 완료</button>	
			</center>
		</div>	
    </div>
</body>
</html>